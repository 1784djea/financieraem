CREATE TABLE `contelementos` (
  `id` int(11) NOT NULL,
  `elemento` char(255) NOT NULL,
  `padre` char(255) DEFAULT NULL,
  `hijo` char(255) DEFAULT NULL,
  `tipo` char(50) DEFAULT NULL,
  `rubroDesc` char(255) NOT NULL,
  `saldoInicial` double DEFAULT NULL,
  `debe` double DEFAULT NULL,
  `haber` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `estatus2` varchar(200) DEFAULT NULL,
  `fechaCierre` date DEFAULT NULL,
  `fechaUltCierre` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


INSERT INTO `contelementos` (`id`, `elemento`, `padre`, `hijo`, `tipo`, `rubroDesc`, `saldoInicial`, `debe`, `haber`, `saldo`, `estatus`, `estatus2`, `fechaCierre`, `fechaUltCierre`, `created_at`, `updated_at`) VALUES
(1, '1', NULL, NULL, NULL, 'Activo', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:45:35', '2023-10-19 07:56:48');


ALTER TABLE `contelementos`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `contelementos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;


CREATE TABLE `contrubros` (
  `id` int(11) NOT NULL,
  `rubro` char(255) DEFAULT NULL,
  `padre` char(255) DEFAULT NULL,
  `hijo` char(255) DEFAULT NULL,
  `tipo` char(50) DEFAULT NULL,
  `rubroDesc` char(255) NOT NULL,
  `saldoInicial` double DEFAULT NULL,
  `debe` double DEFAULT NULL,
  `haber` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `estatus2` varchar(200) DEFAULT NULL,
  `fechaCierre` date DEFAULT NULL,
  `fechaUltCierre` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


INSERT INTO `contrubros` (`id`, `rubro`, `padre`, `hijo`, `tipo`, `rubroDesc`, `saldoInicial`, `debe`, `haber`, `saldo`, `estatus`, `estatus2`, `fechaCierre`, `fechaUltCierre`, `created_at`, `updated_at`) VALUES
(1, '11', '1', NULL, NULL, 'Activo Corriente', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(2, '12', '1', NULL, NULL, 'Activos no corrientes', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48');

INSERT INTO `contrubros` (`id`, `rubro`, `padre`, `hijo`, `tipo`, `rubroDesc`, `saldoInicial`, `debe`, `haber`, `saldo`, `estatus`, `estatus2`, `fechaCierre`, `fechaUltCierre`, `created_at`, `updated_at`) VALUES
(3, '13', '1', NULL, NULL, 'Otros activos', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(4, '21', '2', NULL, NULL, 'Pasivo corriente', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(5, '22', '2', NULL, NULL, 'Pasivos no corrientes', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(6, '31', '3', NULL, NULL, 'Capital', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(7, '32', '3', NULL, NULL, 'Reservas', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(8, '33', '3', NULL, NULL, 'Resultados', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(9, '34', '3', NULL, NULL, 'Ajustes y efectos por revaluación y cambios de valor', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(10, '41', '4', NULL, NULL, 'Ingresos de actividades ordinarias', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(11, '42', '4', NULL, NULL, 'Ingresos por administración de créditos', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(12, '43', '4', NULL, NULL, 'Ingresos de productos por cartera de créditos', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(13, '44', '4', NULL, NULL, 'Participación en resultados', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(14, '51', '5', NULL, NULL, 'Costos de actividades ordinarias', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(15, '52', '5', NULL, NULL, 'Comisiones bancarias', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(16, '53', '5', NULL, NULL, 'Costos por retenciones', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(17, '61', '6', NULL, NULL, 'Gastos de actividades ordinarias', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(18, '71', '7', NULL, NULL, 'Otros ingresos de actividad no ordinaria', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(19, '72', '7', NULL, NULL, 'Otros productos', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(20, '81', '8', NULL, NULL, 'Pérdidas y ganancias', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48'),
(21, '91', '9', NULL, NULL, 'Cuentas de orden', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-18 11:52:05', '2023-10-19 07:56:48');


ALTER TABLE `contrubros`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `contrubros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;



CREATE TABLE `contcuentas` (
  `id` int(11) NOT NULL,
  `cuenta` char(255) NOT NULL,
  `padre` char(255) DEFAULT NULL,
  `hijo` char(255) DEFAULT NULL,
  `tipo` char(50) DEFAULT NULL,
  `rubroDesc` char(255) NOT NULL,
  `saldoInicial` double DEFAULT NULL,
  `debe` double DEFAULT NULL,
  `haber` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `estatus2` varchar(200) DEFAULT NULL,
  `fechaCierre` date DEFAULT NULL,
  `fechaUltCierre` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



INSERT INTO `contcuentas` (`id`, `cuenta`, `padre`, `hijo`, `tipo`, `rubroDesc`, `saldoInicial`, `debe`, `haber`, `saldo`, `estatus`, `estatus2`, `fechaCierre`, `fechaUltCierre`, `created_at`, `updated_at`) VALUES
(4, '1101', '1', NULL, NULL, 'Efectivo y Equivalentes de Efectivo', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-02 04:38:29', '2023-10-19 07:56:48'),
(8, '1102', '1', NULL, NULL, 'Cuentas y documentos por cobrar comerciales', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-16 09:32:56', '2023-10-16 09:32:56'),
(10, '1103', '1', NULL, NULL, 'Cuentas por cobrar a partes relacionadas, corto plazo', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:16:23', '2023-10-19 14:16:23'),
(11, '1104', '1', NULL, NULL, 'Inventarios', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:19:55', '2023-10-19 14:19:55'),
(12, '1105', '1', NULL, NULL, 'Pagos anticipados', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:25:36', '2023-10-19 14:25:36'),
(13, '1106', '1', NULL, NULL, 'Impuestos por acreditar', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:08', '2023-10-19 14:41:08'),
(14, '1107', '1', NULL, NULL, 'Bienes recibidos en pago o adjudicados', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:08', '2023-10-19 14:41:08'),
(15, '1201', '2', NULL, NULL, 'Cuentas y documentos por cobrar a largo plazo', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:08', '2023-10-19 14:41:08'),
(16, '1202', '2', NULL, NULL, 'Propiedad planta y equipo', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:08', '2023-10-19 14:41:08');



ALTER TABLE `contcuentas`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `contcuentas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;



CREATE TABLE `contsubcuentas` (
  `id` int(11) NOT NULL,
  `subcuenta` char(255) NOT NULL,
  `padre` char(255) DEFAULT NULL,
  `hijo` char(255) DEFAULT NULL,
  `tipo` char(50) DEFAULT NULL,
  `rubroDesc` char(255) NOT NULL,
  `saldoInicial` double DEFAULT NULL,
  `debe` double DEFAULT NULL,
  `haber` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `estatus2` varchar(200) DEFAULT NULL,
  `fechaCierre` date DEFAULT NULL,
  `fechaUltCierre` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `contsubcuentas` (`id`, `subcuenta`, `padre`, `hijo`, `tipo`, `rubroDesc`, `saldoInicial`, `debe`, `haber`, `saldo`, `estatus`, `estatus2`, `fechaCierre`, `fechaUltCierre`, `created_at`, `updated_at`) VALUES
(1, '110101', '4', 1, NULL, 'Caja', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-02 06:20:16', '2023-10-19 07:55:25'),
(2, '110102', '4', 1, NULL, 'Fondos de tránsito', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-02 06:54:04', '2023-10-19 07:56:48'),
(4, '110103', '4', NULL, NULL, 'Bancos', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-02 06:57:17', '2023-10-05 07:38:52'),
(5, '110104', '4', 1, NULL, 'Inversiones en depósitos a plazo', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-02 06:57:53', '2023-10-19 07:56:48'),
(6, '110201', '8', 1, NULL, 'Cuotas por devengar', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-02 10:07:27', '2023-10-02 10:07:27'),
(9, '110202', '8', 1, NULL, 'Cuotas devengadas', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 13:58:22', '2023-10-19 13:58:22'),
(10, '110203', '8', NULL, NULL, 'Cuentas por cobrar a Aseguradoras', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:03:31', '2023-10-19 14:03:31'),
(11, '110204', '8', NULL, NULL, 'Cuentas por cobrar capitales vencidos', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:03:45', '2023-10-19 14:03:45'),
(12, '110205', '8', NULL, NULL, 'Estimación de cuentas incobrables', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:03:56', '2023-10-19 14:03:56'),
(13, '110206', '8', 1, NULL, 'Ingresos financieros no devengados', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:04:10', '2023-10-19 14:04:10'),
(14, '110207', '8', 1, NULL, 'Intereses por cobrar', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:04:53', '2023-10-19 14:04:53'),
(15, '110208', '8', 1, NULL, 'Cuentas por cobrar a aseguradoras', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:05:37', '2023-10-19 14:05:37'),
(16, '110209', '8', 1, NULL, 'Otras cuentas por cobrar', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:06:38', '2023-10-19 14:06:38'),
(17, '110301', '10', NULL, NULL, 'Cuentas por cobrar compañías relacionadas', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:17:06', '2023-10-19 14:17:06'),
(18, '110302', '10', NULL, NULL, 'Préstamos por cobrar compañías relacionadas', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:17:25', '2023-10-19 14:17:25'),
(19, '110303', '10', NULL, NULL, 'Intereses por cobrar compañías relacionadas', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:18:48', '2023-10-19 14:18:48'),
(20, '110401', '11', NULL, NULL, 'Inventario de vehículos', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:20:25', '2023-10-19 14:20:25'),
(21, '110402', '11', NULL, NULL, 'Inventario de productos', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:20:39', '2023-10-19 14:20:39'),
(22, '110403', '11', NULL, NULL, 'Otros inventarios', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:20:56', '2023-10-19 14:20:56'),
(23, '110501', '12', NULL, NULL, 'Gastos pagados por anticipados', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:38:48', '2023-10-19 14:38:48'),
(24, '110502', '12', NULL, NULL, 'Anticipos sueldos', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:39:02', '2023-10-19 14:39:02'),
(25, '110503', '12', NULL, NULL, 'Seguros pagados por anticipado', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:39:14', '2023-10-19 14:39:14'),
(26, '110504', '12', NULL, NULL, 'Intereses pagados por anticipado', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:39:31', '2023-10-19 14:39:31'),
(27, '110505', '12', NULL, NULL, 'Intereses por títulos pagados por anticipado', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:39:47', '2023-10-19 14:39:47'),
(28, '110506', '12', NULL, NULL, 'Comisiones bancarias pagadas por anticipado', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:40:08', '2023-10-19 14:40:08'),
(29, '110507', '12', NULL, NULL, 'Prendas bancarias por prestamos', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:40:34', '2023-10-19 14:40:34'),
(30, '110508', '12', NULL, NULL, 'Comisiones por emisión de títulos', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:40:48', '2023-10-19 14:40:48'),
(31, '110601', '13', NULL, NULL, 'Anticipos de impuesto sobre renta anual', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:30', '2023-10-19 14:41:30'),
(32, '110602', '13', NULL, NULL, 'Impuestos retenidos por instituciones financieras', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(33, '110603', '13', NULL, NULL, 'Remanentes de renta anual', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(34, '110604', '13', NULL, NULL, 'Impuesto transferencia de bienes - compras local', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(35, '110605', '13', NULL, NULL, 'Impuesto transferencia de bienes - compras del exterior', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(36, '110606', '13', NULL, NULL, 'Impuesto transferencia de bienes - retenciones', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(37, '110607', '13', NULL, NULL, 'Impuesto transferencia de bienes - retenciones por cobrar', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(38, '110608', '13', NULL, NULL, 'Impuesto transferencia de bienes - percepciones', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(39, '110609', '13', NULL, NULL, 'Impuesto transferencia de bienes - remanentes a favor', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(40, '110610', '13', NULL, NULL, 'Impuestos localidades - remanentes a favor', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(41, '110611', '13', NULL, NULL, 'Otros impuestos', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(42, '110612', '13', NULL, NULL, 'Retención 2% entidades gubernamentales', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(43, '110613', '13', NULL, NULL, 'Impuesto transferencia de bienes - retención 13%', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(44, '110614', '13', NULL, NULL, 'IVA percibido', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(45, '110615', '13', NULL, NULL, 'Impuesto sobre renta - retenciones locales 1% (retención)', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(46, '110701', '14', NULL, NULL, 'Valor de adquisición activos fijos disponible para la venta', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(47, '110702', '14', NULL, NULL, 'Depreciación acumulada activos fijos disponibles para la venta', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(48, '120101', '15', NULL, NULL, 'Documentos por cobrar LP', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(49, '120102', '15', NULL, NULL, 'Estimación documentos LP', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49'),
(50, '120103', '15', NULL, NULL, 'Ingresos financieros no devengados', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:41:49', '2023-10-19 14:41:49');



ALTER TABLE `contsubcuentas`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `contsubcuentas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;



CREATE TABLE `contcuentadetalles` (
  `id` int(11) NOT NULL,
  `cuentaDetalle` char(255) NOT NULL,
  `padre` char(255) DEFAULT NULL,
  `hijo` char(255) DEFAULT NULL,
  `tipo` char(50) DEFAULT NULL,
  `rubroDesc` char(255) NOT NULL,
  `saldoInicial` double DEFAULT NULL,
  `debe` double DEFAULT NULL,
  `haber` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `estatus2` varchar(200) DEFAULT NULL,
  `fechaCierre` date DEFAULT NULL,
  `fechaUltCierre` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


INSERT INTO `contcuentadetalles` (`id`, `cuentaDetalle`, `padre`, `hijo`, `tipo`, `rubroDesc`, `saldoInicial`, `debe`, `haber`, `saldo`, `estatus`, `estatus2`, `fechaCierre`, `fechaUltCierre`, `created_at`, `updated_at`) VALUES
(1, '11010101', '1', NULL, NULL, 'Caja General', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-02 06:29:33', '2023-10-19 07:55:25'),
(2, '11010102', '1', NULL, NULL, 'Caja Chica', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-02 06:53:17', '2023-10-19 07:55:25'),
(3, '11010201', '2', NULL, NULL, 'Fondos en tránsito tarjetas de crédito', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-02 06:54:42', '2023-10-19 07:56:48'),
(4, '11010401', '5', NULL, NULL, 'Depósitos en cuenta corriente', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-02 06:58:25', '2023-10-19 07:56:48'),
(5, '11020101', '6', NULL, NULL, 'Cuotas por devengar de seguros', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-02 10:33:00', '2023-10-02 10:33:00'),
(8, '11020201', '9', NULL, NULL, 'Cuotas devengadas de seguros', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:01:25', '2023-10-19 14:01:25'),
(9, '11020202', '9', NULL, NULL, 'Honorarios de abogados por cobranza', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:02:13', '2023-10-19 14:02:13'),
(10, '11020203', '9', NULL, NULL, 'Cargo de cheques rechazados', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:02:29', '2023-10-19 14:02:29'),
(11, '11020204', '9', NULL, NULL, 'Gastos de estructuración', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:02:43', '2023-10-19 14:02:43'),
(12, '11020205', '9', NULL, NULL, 'Otros cargos por cobrar a clientes', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:02:56', '2023-10-19 14:02:56'),
(13, '11020601', '13', NULL, NULL, 'Ingresos financieros cuotas no devengadas', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:04:33', '2023-10-19 14:04:33'),
(14, '11020701', '14', NULL, NULL, 'Intereses por cobrar exigibles', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:05:11', '2023-10-19 14:05:11'),
(15, '11020702', '14', NULL, NULL, 'Intereses por cobrar vencidos', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:05:24', '2023-10-19 14:05:24'),
(16, '11020801', '15', NULL, NULL, 'Reclamos sobre seguros', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:05:55', '2023-10-19 14:05:55'),
(17, '11020802', '15', NULL, NULL, 'Reintegros de seguros', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:06:09', '2023-10-19 14:06:09'),
(18, '11020901', '16', NULL, NULL, 'Cuentas por cobrar a accionistas', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:06:51', '2023-10-19 14:06:51'),
(19, '11020902', '16', NULL, NULL, 'Cuentas por cobrar a empleados', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:07:07', '2023-10-19 14:07:07'),
(20, '11020903', '16', NULL, NULL, 'Otras cuentas por cobrar', 0, 0, 0, 0, '1', NULL, NULL, NULL, '2023-10-19 14:07:19', '2023-10-19 14:07:19');



ALTER TABLE `contcuentadetalles`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `contcuentadetalles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

