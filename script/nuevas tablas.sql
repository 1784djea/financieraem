CREATE TABLE `partidacierres` (
  `id` int(11) NOT NULL,
  `idcatalogo` varchar(255) DEFAULT NULL,
  `tipo` char(255) DEFAULT NULL,
  `tipo2` char(255) DEFAULT NULL,
  `correlativo` char(255) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `descripcion` char(255) DEFAULT NULL,
  `saldoInicial` double DEFAULT NULL,
  `debe` double DEFAULT NULL,
  `haber` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `estatus` char(10) DEFAULT NULL,
  `estatus2` char(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;