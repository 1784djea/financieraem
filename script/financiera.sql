-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/


reporte para antes del cierre que ya no deje ingresar nuevas partidas,  
reporte despues del cierre

al cerrar el mes ya no debe dejar ingresar mas partidas del mes en curso

al abrir nuevo mes, dos reportes, mes actual y mes anterior, 

cuenta de mayor y cuenta de detalle, las de detalle son las que sufren transacciones

anular partida anterior, editar solo modificar cuenta cuenta y monto y fecha
o eliminar y que ordene la nueva por correlativo



--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-08-2023 a las 00:30:39
-- Versión del servidor: 10.4.27-MariaDB
-- Versión de PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bioapp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `log_name` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `subject_type` varchar(255) DEFAULT NULL,
  `registroModificado` varchar(50) DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(255) DEFAULT NULL,
  `properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`properties`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `registroModificado`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(2, 'default', 'created', 2, 'App\\Clase', 'clasep', 1, 'App\\User', '[]', '2021-08-16 16:42:58', '2021-08-23 08:39:12'),
(3, 'default', 'deleted', 2, 'App\\Clase', 'clasep', 1, 'App\\User', '[]', '2021-08-16 16:45:12', '2021-08-23 08:39:13'),
(4, 'default', 'created', 3, 'App\\Dominio', 'Dominios1', 1, 'App\\User', '[]', '2021-08-16 16:50:18', '2021-08-23 08:05:23'),
(5, 'default', 'updated', 2, 'App\\Dominio', 'prueba', 1, 'App\\User', '[]', '2021-08-16 16:51:06', '2021-08-23 08:40:12')
-- --------------------------------------------------------


CREATE TABLE `empresas` (
  `id` int(11) NOT NULL,
  `nit` int(25) DEFAULT NULL,
  `nrc` int(25) DEFAULT NULL,
  `nombre` char(255) NOT NULL,
  `descripcion` char(255) NOT NULL,
  `codActividad` char(255) DEFAULT NULL,
  `descActividad` char(255) DEFAULT NULL,
  `nombreComercial` char(255) DEFAULT NULL,
  `direccion` char(255) DEFAULT NULL,
  `departamento` char(255) DEFAULT NULL,
  `municipio` char(255) DEFAULT NULL,
  `telefono` char(10) DEFAULT NULL,
  `correo` char(255) DEFAULT NULL,
  `tipoEstablecimiento` char(255) DEFAULT NULL,
  `codPuntoVenta` char(255) DEFAULT NULL,
  `codEstable` char(255) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `facturas` (
  `id` int(11) NOT NULL,
  `idprestatario` char(255) DEFAULT NULL,
  `idprestamo` char(255) DEFAULT NULL,
  `descripcion` char(255) NOT NULL,
  `fecha` date DEFAULT NULL,
  `json` char(255) NOT NULL,
  `factura` int(11) DEFAULT NULL,
  `procesado` int(11) DEFAULT NULL,
  `facturado` int(11) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


--
-- Estructura de tabla para la tabla `aprobacions`
--

CREATE TABLE `aprobacions` (
  `id` int(11) NOT NULL,
  `idInvestigacion` int(11) DEFAULT NULL,
  `descripcion` char(255) NOT NULL,
  `observacion` char(255) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `estado2` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

CREATE TABLE `cierreconts` (
  `id` int(11) NOT NULL,
  `idaux` char(50) DEFAULT NULL,
  `cuenta` char(255) DEFAULT NULL,
  `tipo` char(50) DEFAULT NULL,
  `rubroDesc` char(255) NOT NULL,
  `saldoInicial` double DEFAULT NULL,
  `debe` double DEFAULT NULL,
  `haber` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `periodo` char(2) DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `estatus2` varchar(200) DEFAULT NULL,
  `fechaCierre` date DEFAULT NULL,
  `fechaUltCierre` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `cierreauxs` (
  `id` int(11) NOT NULL,
  `periodo` char(10) DEFAULT NULL,
  `anio` char(10) DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `estatus2` varchar(200) DEFAULT NULL,
  `fechaCierre` date DEFAULT NULL,
  `fechaUltCierre` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `contelementos` (
  `id` int(11) NOT NULL,
  `elemento` char(255) DEFAULT NULL,
  `padre` char(255) DEFAULT NULL,
  `hijo` char(255) DEFAULT NULL,
  `tipo` char(50) DEFAULT NULL,
  `rubroDesc` char(255) NOT NULL,
  `saldoInicial` double DEFAULT NULL,
  `debe` double DEFAULT NULL,
  `haber` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `estatus2` varchar(200) DEFAULT NULL,
  `fechaCierre` date DEFAULT NULL,
  `fechaUltCierre` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `contrubros` (
  `id` int(11) NOT NULL,
  `rubro` char(255) DEFAULT NULL,
  `padre` char(255) DEFAULT NULL,
  `hijo` char(255) DEFAULT NULL,
  `tipo` char(50) DEFAULT NULL,
  `rubroDesc` char(255) NOT NULL,
  `saldoInicial` double DEFAULT NULL,
  `debe` double DEFAULT NULL,
  `haber` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `estatus2` varchar(200) DEFAULT NULL,
  `fechaCierre` date DEFAULT NULL,
  `fechaUltCierre` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `contcuentas` (
  `id` int(11) NOT NULL,
  `cuenta` char(255) DEFAULT NULL,
  `padre` char(255) DEFAULT NULL,
  `hijo` char(255) DEFAULT NULL,
  `tipo` char(50) DEFAULT NULL,
  `rubroDesc` char(255) NOT NULL,
  `saldoInicial` double DEFAULT NULL,
  `debe` double DEFAULT NULL,
  `haber` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `estatus2` varchar(200) DEFAULT NULL,
  `fechaCierre` date DEFAULT NULL,
  `fechaUltCierre` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `contsubcuentas` (
  `id` int(11) NOT NULL,
  `subcuenta` char(255) DEFAULT NULL,
  `padre` char(255) DEFAULT NULL,
  `hijo` char(255) DEFAULT NULL,
  `tipo` char(50) DEFAULT NULL,
  `rubroDesc` char(255) NOT NULL,
  `saldoInicial` double DEFAULT NULL,
  `debe` double DEFAULT NULL,
  `haber` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `estatus2` varchar(200) DEFAULT NULL,
  `fechaCierre` date DEFAULT NULL,
  `fechaUltCierre` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `contcuentadetalles` (
  `id` int(11) NOT NULL,
  `cuentaDetalle` char(255) DEFAULT NULL,
  `padre` char(255) DEFAULT NULL,
  `hijo` char(255) DEFAULT NULL,
  `tipo` char(50) DEFAULT NULL,
  `rubroDesc` char(255) NOT NULL,
  `saldoInicial` double DEFAULT NULL,
  `debe` double DEFAULT NULL,
  `haber` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `estatus2` varchar(200) DEFAULT NULL,
  `fechaCierre` date DEFAULT NULL,
  `fechaUltCierre` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


--
-- Estructura de tabla para la tabla `catcuentas`
--

CREATE TABLE `catcuentas` (
  `id` int(11) NOT NULL,
  `cuenta` char(255) DEFAULT NULL,
  `subcuenta` char(255) DEFAULT NULL,
  `cuentaDetalle` char(255) DEFAULT NULL,
  `tipo` char(50) DEFAULT NULL,
  `rubroDesc` char(255) NOT NULL,
  `estatus` char(2) DEFAULT NULL,
  `estatus2` varchar(200) DEFAULT NULL,
  `saldoInicial` double DEFAULT NULL,
  `debe` double DEFAULT NULL,
  `haber` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `catcuentas`
--

INSERT INTO `catcuentas` (`id`, `cuenta`, `subcuenta`, `cuentaDetalle`, `tipo`, `rubroDesc`, `estatus`, `estatus2`, `saldoInicial`, `debe`, `haber`, `saldo`, `created_at`, `updated_at`) VALUES
(1, '1', NULL, NULL, NULL, ' Activo', '1', '1', 0, NULL, NULL, NULL, '2023-02-07 00:37:31', '2023-03-13 18:22:00'),
(2, '11', NULL, NULL, NULL, ' Activo Corriente', '1', NULL, 0, NULL, NULL, NULL, '2023-02-07 02:10:18', '2023-02-07 02:10:18'),
(3, '1101', NULL, NULL, NULL, ' Efectivo y Equivalentes de Efectivo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-12 00:35:19', '2023-02-12 00:35:19'),
(4, NULL, '110101', NULL, NULL, ' Caja', '1', '4', 0, NULL, NULL, NULL, '2023-02-12 00:35:31', '2023-02-27 13:56:46'),
(5, NULL, NULL, '11010101', 'Activo', ' Caja General', '1', '5', 0, NULL, NULL, NULL, '2023-02-12 00:35:50', '2023-02-12 00:35:50'),
(6, NULL, NULL, '11010102', 'Activo', ' Caja Chica', '1', NULL, 0, NULL, NULL, NULL, '2023-02-12 01:14:37', '2023-02-12 01:14:37'),
(7, NULL, '110102', NULL, NULL, 'Fondos de tránsito', '1', NULL, 0, NULL, NULL, NULL, '2023-02-12 01:14:37', '2023-02-12 01:14:37'),
(8, NULL, NULL, '11010201', 'Activo', 'Fondos en tránsito tarjetas de crédito', '1', NULL, 0, NULL, NULL, NULL, '2023-02-12 01:14:37', '2023-02-12 01:14:37'),
(9, NULL, '110103', NULL, 'Activo', 'Bancos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-12 01:14:37', '2023-02-12 01:14:37'),
(10, NULL, '110104', NULL, 'Activo', 'Fondos en tránsito tarjetas de crédito', '1', NULL, 0, NULL, NULL, NULL, '2023-02-12 01:14:37', '2023-02-12 01:14:37'),
(11, NULL, NULL, '11010401', 'Activo', 'Depósitos en cuenta corriente', '1', NULL, 0, NULL, NULL, NULL, '2023-02-12 01:14:37', '2023-02-12 01:14:37'),
(12, '1102', NULL, NULL, NULL, 'Cuentas y documentos por cobrar comerciales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:37:44', '2023-02-19 00:37:44'),
(13, NULL, '110201', NULL, NULL, 'Cuotas por devengar', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:19', '2023-02-19 00:38:19'),
(14, NULL, NULL, '11020101', NULL, 'Cuotas por devengar de seguros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(15, NULL, '110202', NULL, NULL, 'Cuotas devengadas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(16, NULL, NULL, '11020201', NULL, 'Cuotas devengadas de seguros', '1', '1', 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(17, NULL, NULL, '11020202', NULL, 'Honorarios de abogados por cobranza', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(18, NULL, NULL, '11020203', NULL, 'Cargo de cheques rechazados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(19, NULL, NULL, '11020204', NULL, 'Gastos de estructuración', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(20, NULL, NULL, '11020205', NULL, 'Otros cargos por cobrar a clientes', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(21, NULL, '110203', NULL, NULL, 'Cuentas por cobrar a Aseguradoras', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(22, NULL, '110204', NULL, NULL, 'Cuentas por cobrar capitales vencidos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(23, NULL, '110205', NULL, NULL, 'Estimación de cuentas incobrables', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(24, NULL, '110206', NULL, NULL, 'Ingresos financieros no devengados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(25, NULL, NULL, '11020601', NULL, 'Ingresos financieros cuotas no devengadas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(26, NULL, '110207', NULL, NULL, 'Intereses por cobrar', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(27, NULL, NULL, '11020701', NULL, 'Intereses por cobrar exigibles', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(28, NULL, NULL, '11020702', NULL, 'Intereses por cobrar vencidos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(29, NULL, '110208', NULL, NULL, 'Cuentas por cobrar a aseguradoras', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(30, NULL, NULL, '11020801', NULL, 'Reclamos sobre seguros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(31, NULL, NULL, '11020802', NULL, 'Reintegros de seguros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(32, NULL, '110209', '', NULL, 'Otras cuentas por cobrar', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(33, NULL, NULL, '11020901', NULL, 'Cuentas por cobrar a accionistas', '1', '3', 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(34, NULL, NULL, '11020902', NULL, 'Cuentas por cobrar a empleados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(35, NULL, NULL, '11020903', NULL, 'Otras cuentas por cobrar', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(36, '1103', NULL, NULL, NULL, 'Cuentas por cobrar a partes relacionadas, corto plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(37, NULL, '110301', NULL, NULL, 'Cuentas por cobrar compañías relacionadas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(38, NULL, '110302', NULL, NULL, 'Préstamos por cobrar compañías relacionadas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(39, NULL, '110303', NULL, NULL, 'Intereses por cobrar compañías relacionadas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(40, '1104', NULL, NULL, NULL, 'Inventarios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(41, NULL, '110401', NULL, NULL, 'Inventario de vehículos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(42, NULL, '110402', NULL, NULL, 'Inventario de productos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(43, NULL, '110403', NULL, NULL, 'Otros inventarios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(44, '1105', NULL, NULL, NULL, 'Pagos anticipados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(45, NULL, '110501', NULL, NULL, 'Gastos pagados por anticipados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(46, NULL, '110502', NULL, NULL, 'Anticipos sueldos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(47, NULL, '110503', NULL, NULL, 'Seguros pagados por anticipado', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(48, NULL, '110504', NULL, NULL, 'Intereses pagados por anticipado', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(49, NULL, '110505', NULL, NULL, 'Intereses por títulos pagados por anticipado', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(50, NULL, '110506', NULL, NULL, 'Comisiones bancarias pagadas por anticipado', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(51, NULL, '110507', NULL, NULL, 'Prendas bancarias por prestamos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(52, NULL, '110508', NULL, NULL, 'Comisiones por emisión de títulos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(53, '1106', NULL, NULL, NULL, 'Impuestos por acreditar', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(54, NULL, '110601', NULL, NULL, 'Anticipos de impuesto sobre renta anual', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(55, NULL, '110602', NULL, NULL, 'Impuestos retenidos por instituciones financieras', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(56, NULL, '110603', NULL, NULL, 'Remanentes de renta anual', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(57, NULL, '110604', NULL, NULL, 'Impuesto transferencia de bienes - compras local', '1', '5', 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(58, NULL, '110605', NULL, NULL, 'Impuesto transferencia de bienes - compras del exterior', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(59, NULL, '110606', NULL, NULL, 'Impuesto transferencia de bienes - retenciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(60, NULL, '110607', NULL, NULL, 'Impuesto transferencia de bienes - retenciones por cobrar', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(61, NULL, '110608', NULL, NULL, 'Impuesto transferencia de bienes - percepciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(62, NULL, '110609', NULL, NULL, 'Impuesto transferencia de bienes - remanentes a favor', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(63, NULL, '110610', NULL, NULL, 'Impuestos localidades - remanentes a favor', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(64, NULL, '110611', NULL, NULL, 'Otros impuestos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(65, NULL, '110612', NULL, NULL, 'Retención 2% entidades gubernamentales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(66, NULL, '110613', NULL, NULL, 'Impuesto transferencia de bienes - retención 13%', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(67, NULL, '110614', NULL, NULL, 'IVA percibido', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(68, NULL, '110615', NULL, NULL, 'Impuesto sobre renta - retenciones locales 1% (retención)', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(69, '1107', NULL, NULL, NULL, 'Bienes recibidos en pago o adjudicados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(70, NULL, '110701', NULL, NULL, 'Valor de adquisición activos fijos disponible para la venta', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(71, NULL, '110702', NULL, NULL, 'Depreciación acumulada activos fijos disponibles para la venta', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(72, '12', NULL, NULL, NULL, 'Activos no corrientes', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(73, '1201', NULL, NULL, NULL, 'Cuentas y documentos por cobrar a largo plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(74, NULL, '120101', NULL, NULL, 'Documentos por cobrar LP', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(75, NULL, '120102', NULL, NULL, 'Estimación documentos LP', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(76, NULL, '120103', NULL, NULL, 'Ingresos financieros no devengados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(77, '1202', NULL, NULL, NULL, 'Propiedad planta y equipo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(78, NULL, '120201', NULL, NULL, 'Terrenos valor de adquisición', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(79, NULL, '120202', NULL, NULL, 'Revaluación de terrenos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(80, NULL, '120203', NULL, NULL, 'Valor de adquisición edificios e instalaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(81, NULL, '120204', NULL, NULL, 'Revaluación de edificios e instalaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(82, NULL, '120205', NULL, NULL, 'Mejoras en edificios e instalaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(83, NULL, '120206', NULL, NULL, 'Depreciación acumulada de edificios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(84, NULL, '120207', NULL, NULL, 'Depreciación acumulada de edificios (revalúo)', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(85, NULL, '120208', NULL, NULL, 'Valor de adquisición vehículos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(86, NULL, '120209', NULL, NULL, 'Depreciación acumulada de vehículos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(87, NULL, '120210', NULL, NULL, 'Valor de adquisición maquinaria', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(88, NULL, '120211', NULL, NULL, 'Mejoras de maquinaria', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(89, NULL, '120212', NULL, NULL, 'Depreciación acumulada de maquinaria', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(90, NULL, '120213', NULL, NULL, 'Valor de adquisición herramientas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(91, NULL, '120214', NULL, NULL, 'Mejoras de herramientas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(92, NULL, '120215', NULL, NULL, 'Depreciación acumulada de herramientas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(93, NULL, '120216', NULL, NULL, 'Valor de adquisición equipo de computo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(94, NULL, '120217', NULL, NULL, 'Depreciación acumulada de equipo de computo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(95, NULL, '120218', NULL, NULL, 'Valor de adquisición mobiliario y equipo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(96, NULL, '120219', NULL, NULL, 'Depreciación acumulada de mobiliario y equipo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(97, NULL, '120220', NULL, NULL, 'Valor de adquisición rótulos publicitarios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(98, NULL, '120221', NULL, NULL, 'Depreciación acumulada de rótulos publicitarios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(99, NULL, '120222', NULL, NULL, 'Valor de adquisición mejoras en propiedades arrendadas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(100, NULL, '120223', NULL, NULL, 'Depreciación acumulada de mejoras arrendadas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(101, NULL, '120224', NULL, NULL, 'Valor de adquisición equipos diversos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(102, NULL, '120225', NULL, NULL, 'Depreciación acumulada de equipos diversos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(103, NULL, '120226', NULL, NULL, 'Bienes en arrendamiento valor de adquisición', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(104, NULL, '120227', NULL, NULL, 'Vehículos en arrendamiento financiero', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(105, NULL, '120228', NULL, NULL, 'mobiliario y equipo de oficina en arrendamiento financiero', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(106, NULL, '120229', NULL, NULL, 'Depreciación acumulada de bienes en arrendamiento', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(107, NULL, '120230', NULL, NULL, 'Maquinaria, equipo y herramientas en arrendamiento', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(108, NULL, '120231', NULL, NULL, 'Depreciación acumulada de maquinaria, equipo y herramientas en arrendamiento', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(109, NULL, '120232', NULL, NULL, 'Altas obras en proceso', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(110, NULL, '120233', NULL, NULL, 'Altas intangibles en proceso', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(111, NULL, '120234', NULL, NULL, 'Altas mobiliario y equipo en proceso', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(112, NULL, '120235', NULL, NULL, 'Valor de adquisición de vehículos en arrendamiento', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(113, NULL, '120236', NULL, NULL, 'Depreciación acumulada de vehículos en arrendamiento', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(114, NULL, '120237', NULL, NULL, 'Valor de adquisición equipos de cómputo en arrendamiento', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(115, NULL, '120238', NULL, NULL, 'Depreciación acumulada de equipo de cómputo en arrendamiento', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(116, NULL, '120239', NULL, NULL, 'Valor de adquisición de mobiliario y equipo de cómputo en arrendamiento', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(117, NULL, '120240', NULL, NULL, 'Depreciación acumulada de mobiliario y equipo en arrendamiento', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(118, '1203', NULL, NULL, NULL, 'Inversiones permanentes', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(119, NULL, '120301', NULL, NULL, 'Inversiones permanentes en acciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(120, NULL, '120302', NULL, NULL, 'Inversiones permanentes en bonos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(121, NULL, '120303', NULL, NULL, 'Inversiones permanentes en certificados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(122, NULL, '120304', NULL, NULL, 'Deterioro de las inversiones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(123, NULL, '120305', NULL, NULL, 'Variación en inversiones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(124, '1204', NULL, NULL, NULL, 'Activos intangibles', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(125, NULL, '120401', NULL, NULL, 'Valor de adquisición derechos de marca', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(126, NULL, '120402', NULL, NULL, 'Amortización de marcas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(127, NULL, '120403', NULL, NULL, 'Deterioro de intangibles', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(128, NULL, '120404', NULL, NULL, 'Valor de adquisición software', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(129, NULL, '120405', NULL, NULL, 'Amortización de software', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(130, NULL, '120406', NULL, NULL, 'Crédito mercantil', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(131, NULL, '120407', NULL, NULL, 'Good Will', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(132, NULL, '120408', NULL, NULL, 'Activos intangibles disponibles para la venta', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(133, '1205', NULL, NULL, NULL, 'Propiedad de inversión', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(134, NULL, '120501', NULL, NULL, 'Propiedades de inversión', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(135, NULL, '120502', NULL, NULL, 'Revaluación propiedades de inversión', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(136, NULL, '120503', NULL, NULL, 'Depreciación acumulada de propiedades de inversión', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(137, '13', NULL, NULL, NULL, 'Otros activos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(138, '1301', NULL, NULL, NULL, 'Depósitos en garantía a largo plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(139, NULL, '130101', NULL, NULL, 'Depósitos en garantía', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(140, '1302', NULL, NULL, NULL, 'Activo por impuestos diferidos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(141, NULL, '130201', NULL, NULL, 'Impuesto sobre la renta diferido', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(142, '1303', NULL, NULL, NULL, 'Otros activos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(143, NULL, '130301', NULL, NULL, 'Activos disponibles para la venta', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(144, NULL, '130302', NULL, NULL, 'Otros activos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(145, '2', NULL, NULL, NULL, 'Pasivo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(146, '21', NULL, NULL, NULL, 'Pasivo corriente', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(147, '2101', NULL, NULL, NULL, 'Préstamos por pagar a corto plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(148, NULL, '210101', NULL, NULL, 'Préstamos locales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(149, NULL, '210102', NULL, NULL, 'Préstamos a corto plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(150, NULL, '210103', NULL, NULL, 'Cartas de crédito locales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(151, NULL, '210104', NULL, NULL, 'Obligación por emisiones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(152, NULL, '210105', NULL, NULL, 'Porción corriente titularización', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(153, NULL, '210106', NULL, NULL, 'Porción corriente de préstamos a largo plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(154, NULL, '210107', NULL, NULL, 'Sobregiros bancarios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(155, '2102', NULL, NULL, NULL, 'Depósitos por pagar a corto plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(156, NULL, '210201', NULL, NULL, 'Depósitos por pagar a corto plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(157, '2103', NULL, NULL, NULL, 'Dividendos por pagar', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(158, NULL, '210301', NULL, NULL, 'Dividendos por pagar', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(159, '2104', NULL, NULL, NULL, 'Títulos de emisión propia a corto plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(160, NULL, '210401', NULL, NULL, 'Certificados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(161, NULL, '210402', NULL, NULL, 'Bonos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(162, NULL, '210403', NULL, NULL, 'Documentos por pagar corto plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(163, '2105', NULL, NULL, NULL, 'Deudas comerciales por pagar a corto plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(164, NULL, '210501', NULL, NULL, 'Anticipo cuentas de clientes', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(165, NULL, '210502', NULL, NULL, 'Proveedores locales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(166, NULL, '210503', NULL, NULL, 'Proveedores del exterior', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(167, NULL, '210504', NULL, NULL, 'Provisión de facturas de proveedores a recibir', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(168, '2106', NULL, NULL, NULL, 'Cuentas por pagar a partes relacionadas corto plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(169, NULL, '210601', NULL, NULL, 'Cuentas por pagar compañías relacionadas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(170, NULL, '210602', NULL, NULL, 'Prestamos por pagar compañías relacionadas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(171, NULL, '210603', NULL, NULL, 'Intereses por pagar compañías relacionadas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(172, '2107', NULL, NULL, NULL, 'Anticipos de clientes', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(173, NULL, '210701', NULL, NULL, 'Anticipo clientes', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(174, NULL, '210702', NULL, NULL, 'Ingresos diferidos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(175, NULL, '210703', NULL, NULL, 'Anticipo y/o primas de productos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(176, NULL, '210704', NULL, NULL, 'Anticipo clientes IVA', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(177, NULL, '210705', NULL, NULL, 'Primas Compañías aseguradoras por financiamientos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(178, NULL, '210706', NULL, NULL, 'Primas Compañías aseguradoras seguro daños capitalizables', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(179, NULL, '210707', NULL, NULL, 'Reintegro de primas aseguradoras', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(180, NULL, '210708', NULL, NULL, 'GPS Compañías proveedoras de servicio', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(181, NULL, '210709', NULL, NULL, 'Comisiones recibidas por anticipado', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(182, NULL, '210710', NULL, NULL, 'Devoluciones a clientes', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(183, NULL, '210711', NULL, NULL, 'Otros pasivos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(184, '2108', NULL, NULL, NULL, 'Impuestos por pagar', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(185, NULL, '210801', NULL, NULL, 'Impuesto transferencia de bienes', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(186, NULL, NULL, '21080101', NULL, 'Impuesto transferencia de bienes -ventas', '1', '1', 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(187, NULL, NULL, '21080102', NULL, 'Impuesto transferencia de bienes – retenciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(188, NULL, '', '21080103', NULL, 'Impuesto transferencia de bienes – percepciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(189, NULL, '210802', NULL, NULL, 'Impuesto sobre la renta', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(190, NULL, NULL, '21080201', NULL, 'Impuesto sobre renta - retenciones extranjeros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(191, NULL, NULL, '21080202', NULL, 'Impuesto sobre renta – empleados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(192, NULL, NULL, '21080203', NULL, 'Impuestos por terceros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(193, NULL, '210803', NULL, NULL, 'Impuesto sobre la renta anual', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(194, NULL, '210804', NULL, NULL, 'Impuestos municipales localidades', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(195, NULL, '210805', NULL, NULL, 'Impuesto al activo neto', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(196, NULL, '210806', NULL, NULL, 'Retenciones del ISR - bienes y servicios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(197, '2109', NULL, NULL, NULL, 'Otras cuentas por pagar a corto plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(198, NULL, '210901', NULL, NULL, 'Reembolso a clientes', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(199, NULL, '210902', NULL, NULL, 'Sueldos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(200, NULL, '210903', NULL, NULL, 'Otros sueldos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(201, NULL, '210904', NULL, NULL, 'Comisiones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(202, NULL, '210905', NULL, NULL, 'Gratificaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(203, NULL, '210906', NULL, NULL, 'Aguinaldo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(204, NULL, '210907', NULL, NULL, 'Vacaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(205, NULL, '210908', NULL, NULL, 'Seguro colectivo de vida', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(206, NULL, '210909', NULL, NULL, 'Seguro médico hospitalario', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(207, NULL, '210910', NULL, NULL, 'Asistencia social', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(208, NULL, '210911', NULL, NULL, 'Becas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(209, NULL, '210912', NULL, NULL, 'Alimentación', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(210, NULL, '210913', NULL, NULL, 'Uniformes', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(211, NULL, '210914', NULL, NULL, 'Atenciones y festejos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(212, NULL, '210915', NULL, NULL, 'Comisiones retenidas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(213, NULL, '210916', NULL, NULL, 'Bonificaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(214, NULL, '210917', NULL, NULL, 'Intereses por pagar', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(215, NULL, '210918', NULL, NULL, 'Intereses por pagar emisiones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(216, NULL, '210919', NULL, NULL, 'Intereses por pagar titularización', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(217, NULL, '210920', NULL, NULL, 'Seguros incendios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(218, NULL, '210921', NULL, NULL, 'Seguro transporte mercaderías', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(219, NULL, '210922', NULL, NULL, 'Seguro de vehículos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(220, NULL, '210923', NULL, NULL, 'Seguro contra robo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(221, NULL, '210924', NULL, NULL, 'Instituciones públicas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(222, NULL, '210925', NULL, NULL, 'Instituciones financieras', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(223, NULL, '210926', NULL, NULL, 'Fondo de pensiones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(224, NULL, '210927', NULL, NULL, 'Formación profesional', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(225, NULL, '210928', NULL, NULL, 'Fondo para la vivienda', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(226, NULL, '210929', NULL, NULL, 'Retenciones jurídicas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(227, NULL, '210930', NULL, NULL, 'Otras retenciones a empleados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(228, NULL, '210931', NULL, NULL, 'Pensiones por jubilación', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(229, NULL, '210932', NULL, NULL, 'Seguro educativo - cuota patronal', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(230, NULL, '210933', NULL, NULL, 'Aporte patronal', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(231, NULL, '210934', NULL, NULL, 'Energía eléctrica', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(232, NULL, '210935', NULL, NULL, 'Teléfono', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(233, NULL, '210936', NULL, NULL, 'Agua', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(234, NULL, '210937', NULL, NULL, 'Honorarios legales, de auditoría, consultoría y otros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(235, NULL, '210938', NULL, NULL, 'Publicidad', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(236, NULL, '210939', NULL, NULL, 'Combustible', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(237, NULL, '210940', NULL, NULL, 'Seguridad privada', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(238, NULL, '210941', NULL, NULL, 'Seguros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(239, NULL, '210942', NULL, NULL, 'Seguro de deuda', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(240, NULL, '210943', NULL, NULL, 'Promoción del mes – provisión', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(241, NULL, '210944', NULL, NULL, 'Servicios subcontratados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(242, '22', NULL, NULL, NULL, 'Pasivos no corrientes', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(243, '2201', NULL, NULL, NULL, 'Prestamos por pagar a largo plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(244, NULL, '220101', NULL, NULL, 'Préstamos locales LP', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(245, NULL, '220102', NULL, NULL, 'Préstamos bancarios locales largo plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(246, NULL, '220103', NULL, NULL, 'Préstamos bancarios del exterior largo plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(247, NULL, '220104', NULL, NULL, 'Cartas de crédito largo plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(248, NULL, '220105', NULL, NULL, 'Hipotecas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(249, NULL, '220106', NULL, NULL, 'Documentos por pagar largo plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(250, NULL, '220107', NULL, NULL, 'Obligación por emisiones largo plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(251, NULL, '220108', NULL, NULL, 'Titularización largo plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(252, '2202', NULL, NULL, NULL, 'Obligaciones a largo plazo por beneficios a empleados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(253, NULL, '220201', NULL, NULL, 'Pasivo laboral', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(254, NULL, '220202', NULL, NULL, 'Provisión prima por antigüedad', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(255, '2203', NULL, NULL, NULL, 'Pasivo por impuestos diferidos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(256, NULL, '220301', NULL, NULL, 'Impuesto sobre la renta diferido', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(257, '2204', NULL, NULL, NULL, 'Títulos de emisión propia a largo plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(258, NULL, '220401', NULL, NULL, 'Certificados LP', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(259, NULL, '220402', NULL, NULL, 'Bonos LP', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(260, NULL, '220403', NULL, NULL, 'Documentos por pagar LP', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(261, '3', NULL, NULL, NULL, 'Patrimonio', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(262, '31', NULL, NULL, NULL, 'Capital', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(263, '3101', NULL, NULL, NULL, 'Capital social mínimo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(264, NULL, '310101', NULL, NULL, 'Capital social mínimo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(265, '3102', NULL, NULL, NULL, 'Capital social variable', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(266, NULL, '310201', NULL, NULL, 'Capital social variable', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(267, '3103', NULL, NULL, NULL, 'Capital social no pagado', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(268, NULL, '310301', NULL, NULL, 'Capital social suscrito no pagado', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(269, '3104', NULL, NULL, NULL, 'Aportaciones adicionales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(270, NULL, '310401', NULL, NULL, 'Aportes adicionales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(271, '32', NULL, NULL, NULL, 'Reservas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(272, '3201', NULL, NULL, NULL, 'Reservas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(273, NULL, '320101', NULL, NULL, 'Reserva legal', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(274, '3202', NULL, NULL, NULL, 'Otras reservas de capital', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(275, NULL, '320201', NULL, NULL, 'Otras reservas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(276, '3203', NULL, NULL, NULL, 'Reservas de cobertura', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(277, NULL, '320301', NULL, NULL, 'Instrumento financiero de cobertura', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(278, '3204', NULL, NULL, NULL, 'Ajuste y efectos por valuación', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(279, NULL, '320401', NULL, NULL, 'Revaluaciones de activos fijos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(280, '33', NULL, NULL, NULL, 'Resultados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(281, '3301', NULL, NULL, NULL, 'Resultados acumulados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(282, NULL, '330101', NULL, NULL, 'Resultados de años anteriores', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(283, '3302', NULL, NULL, NULL, 'Resultado del ejercicio', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(284, NULL, '330201', NULL, NULL, 'Resultados del presente ejercicio', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(285, '3303', NULL, NULL, NULL, 'Otros resultados integrales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(286, NULL, '330301', NULL, NULL, 'Efecto de conversión de moneda', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(287, '3304', NULL, NULL, NULL, 'Otros ajustes', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(288, NULL, '330401', NULL, NULL, 'Otras transacciones y ajustes', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(289, '34', NULL, NULL, NULL, 'Ajustes y efectos por revaluación y cambios de valor', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(290, '3401', NULL, NULL, NULL, 'Ajustes y efectos por revaluación en bienes no depreciables', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(291, NULL, '340101', NULL, NULL, 'Superávit por revaluación de terrenos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(292, '3402', NULL, NULL, NULL, 'Ajustes y efectos por revaluación en bienes depreciables y cambios de valor', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(293, NULL, '340201', NULL, NULL, 'Superávit por revaluación de edificios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(294, NULL, '340202', NULL, NULL, 'Utilidades por realizar en venta de inmuebles', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(295, '4', NULL, NULL, NULL, 'Cuentas de resultado acreedoras-ingresos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(296, '41', NULL, NULL, NULL, 'Ingresos de actividades ordinarias', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(297, '4101', NULL, NULL, NULL, 'Ingresos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(298, NULL, '410101', NULL, NULL, 'Ingreso por intereses-provisión', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(299, NULL, '410102', NULL, NULL, 'Ingreso por intereses', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(300, NULL, '410103', NULL, NULL, 'Ingresos por intereses moratorios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(301, NULL, '410104', NULL, NULL, 'Otros ingresos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(302, NULL, '410105', NULL, NULL, 'Comision Apertura de Credito', '1', '3', 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(303, '42', NULL, NULL, NULL, 'Ingresos por administración de créditos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(304, '4201', NULL, NULL, NULL, 'Ingresos por administración de créditos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(305, NULL, '420101', NULL, NULL, 'Ingresos por administración del crédito', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(306, '43', NULL, NULL, NULL, 'Ingresos de productos   por   cartera de créditos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(307, '4301', NULL, NULL, NULL, 'Ingresos por honorarios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(308, NULL, '430101', NULL, NULL, 'Ingresos por honorarios de abogados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(309, '4302', NULL, NULL, NULL, 'Ingresos por administración de cartera', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(310, NULL, '430201', NULL, NULL, 'Comisión por administración carteras vendidas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(311, '4303', NULL, NULL, NULL, 'Ingresos por servicios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(312, NULL, '430301', NULL, NULL, 'Ingresos por servicios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(313, '4304', NULL, NULL, NULL, 'Ingresos por venta de activo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(314, NULL, '430401', NULL, NULL, 'Ingresos por venta de activos fijos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(315, '4305', NULL, NULL, NULL, 'Otros ingresos no operacionales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(316, NULL, '430501', NULL, NULL, 'Sobrantes de caja', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(317, NULL, '430502', NULL, NULL, 'Ingresos compañías relacionadas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(318, NULL, '430503', NULL, NULL, 'Ingresos por publicidad', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(319, NULL, '430504', NULL, NULL, 'Ganancia por bienes recuperados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(320, NULL, '430505', NULL, NULL, 'Recuperación cartera perdida reservas gravable', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(321, NULL, '430506', NULL, NULL, 'Recuperación cartera perdida reservas no gravable', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(322, NULL, '430507', NULL, NULL, 'Ingresos por seguros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(323, NULL, '430508', NULL, NULL, 'Ingresos por GPS', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(324, NULL, '430509', NULL, NULL, 'Ingreso intereses no operacionales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(325, NULL, '430510', NULL, NULL, 'Servicios de grúa', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(326, NULL, '430511', NULL, NULL, 'Servicios de valúo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(327, NULL, '430512', NULL, NULL, 'Otros ingresos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(328, NULL, '430513', NULL, NULL, 'Comisiones en instrumentos financieros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(329, NULL, '430514', NULL, NULL, 'Otras comisiones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(330, '44', NULL, NULL, NULL, 'Participación en resultados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(331, '4401', NULL, NULL, NULL, 'Participación en resultados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(332, NULL, '440101', NULL, NULL, 'Dividendos recibidos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(333, '4402', NULL, NULL, NULL, 'Inversiones en negocios conjuntos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(334, NULL, '440201', NULL, NULL, 'Indemnizaciones por reclamos de seguros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(335, '5', NULL, NULL, NULL, 'Cuenta de resultado deudoras-costos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(336, '51', NULL, NULL, NULL, 'Costos de actividades ordinarias', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(337, '5101', NULL, NULL, NULL, 'Costo por intereses', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(338, NULL, '510101', NULL, NULL, 'Intereses prestamos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(339, NULL, '510102', NULL, NULL, 'Intereses títulos valores', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(340, NULL, '510103', NULL, NULL, 'Intereses depósitos a plazo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(341, NULL, '510104', NULL, NULL, 'Intereses depósitos de ahorro', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(342, NULL, '510105', NULL, NULL, 'Intereses compañías relacionadas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(343, NULL, '510106', NULL, NULL, 'Intereses titularización', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(344, '52', NULL, NULL, NULL, 'Comisiones bancarias', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(345, '5201', NULL, NULL, NULL, 'Comisiones bancarias', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(346, NULL, '520101', NULL, NULL, 'Comisiones bancarias', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52');
INSERT INTO `catcuentas` (`id`, `cuenta`, `subcuenta`, `cuentaDetalle`, `tipo`, `rubroDesc`, `estatus`, `estatus2`, `saldoInicial`, `debe`, `haber`, `saldo`, `created_at`, `updated_at`) VALUES
(347, NULL, '520102', NULL, NULL, 'Comisiones títulos valores', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(348, '5202', NULL, NULL, NULL, 'Comisiones terceros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(349, NULL, '520201', NULL, NULL, 'Comisiones terceros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(350, '5203', NULL, NULL, NULL, 'Otras comisiones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(351, NULL, '520301', NULL, NULL, 'Otras comisiones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(352, '53', NULL, NULL, NULL, 'Costos por retenciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(353, '5301', NULL, NULL, NULL, 'Costos por retenciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(354, NULL, '530101', NULL, NULL, 'Retenciones prestamos exterior', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(355, NULL, '530102', NULL, NULL, 'Otros costos por préstamos bancarios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(356, NULL, '530103', NULL, NULL, 'Otros costos por títulos valores', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(357, '6', NULL, NULL, NULL, 'Cuentas de resultado deudoras-gastos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(358, '61', NULL, NULL, NULL, 'Gastos de actividades ordinarias', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(359, '6101', NULL, NULL, NULL, 'Gastos de operación', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(360, NULL, '610101', NULL, NULL, 'Sueldos y salarios permanente', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(361, NULL, '610102', NULL, NULL, 'Horas extras', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(362, NULL, '610103', NULL, NULL, 'Comisiones sobre ventas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(363, NULL, '610104', NULL, NULL, 'Comisiones sobre cobros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(364, NULL, '610105', NULL, NULL, 'Bonificaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(365, NULL, '610106', NULL, NULL, 'Gratificaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(366, NULL, '610107', NULL, NULL, 'Comisión a terceros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(367, NULL, '610108', NULL, NULL, 'Honorarios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(368, NULL, '610109', NULL, NULL, 'Servicios profesionales de planilla', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(369, NULL, '6101010', NULL, NULL, 'Seguro médico hospitalario', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(370, NULL, '6101011', NULL, NULL, 'Atenciones y festejos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(371, NULL, '6101012', NULL, NULL, 'Pensiones por jubilación', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(372, NULL, '6101013', NULL, NULL, 'Aguinaldo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(373, NULL, '6101014', NULL, NULL, 'Catorceavo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(374, NULL, '6101015', NULL, NULL, 'Vacaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(375, NULL, '6101016', NULL, NULL, 'Seguro colectivo de vida', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(376, NULL, '6101017', NULL, NULL, 'Uniformes', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(377, NULL, '6101018', NULL, NULL, 'Asistencia social', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(378, NULL, '6101019', NULL, NULL, 'Becas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(379, NULL, '6101020', NULL, NULL, 'Alimentación y transporte', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(380, NULL, '6101021', NULL, NULL, 'Capacitaciones al personal', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(381, NULL, '6101022', NULL, NULL, 'Compras e insumos de cafetería', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(382, NULL, '6101023', NULL, NULL, 'Gastos de representación', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(383, NULL, '6101024', NULL, NULL, 'Medicinas y botiquines', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(384, NULL, '6101025', NULL, NULL, 'Prestaciones e indemnizaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(385, NULL, '6101026', NULL, NULL, 'Seguridad social', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(386, NULL, '6101027', NULL, NULL, 'Planes de retiro', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(387, NULL, '6101028', NULL, NULL, 'Formación profesional', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(388, NULL, '6101029', NULL, NULL, 'Fondo para vivienda', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(389, NULL, '6101030', NULL, NULL, 'Seguro educativo - cuota patronal', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(390, NULL, '6101031', NULL, NULL, 'Honorarios de personal indemnización', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(391, NULL, '6101032', NULL, NULL, 'Papelería y útiles de oficina', '1', '3', 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(392, NULL, '6101033', NULL, NULL, 'Catálogos y manuales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(393, NULL, '6101034', NULL, NULL, 'Materiales y útiles de aseo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(394, NULL, '6101035', NULL, NULL, 'Materiales de empaque', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(395, NULL, '6101036', NULL, NULL, 'Materiales diversos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(396, NULL, '6101037', NULL, NULL, 'Combustible, lubricantes y depreciaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(397, NULL, '6101038', NULL, NULL, 'Reparaciones y mantenimiento de vehículos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(398, NULL, '6101039', NULL, NULL, 'Fondo de red vial', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(399, NULL, '6101040', NULL, NULL, 'Mantenimiento de edificios e instalaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(400, NULL, '6101041', NULL, NULL, 'Reparaciones de edificios e instalaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(401, NULL, '6101042', NULL, NULL, 'Mantenimiento equipo taller', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(402, NULL, '6101043', NULL, NULL, 'Reparaciones equipo taller', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(403, NULL, '6101044', NULL, NULL, 'Mantenimiento de vehículos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(404, NULL, '6101045', NULL, NULL, 'Reparaciones de vehículos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(405, NULL, '6101046', NULL, NULL, 'Mantenimiento de mobiliario y equipo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(406, NULL, '6101047', NULL, NULL, 'Mantenimiento de equipo de computo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(407, NULL, '6101048', NULL, NULL, 'Diferencias en precios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(408, NULL, '6101049', NULL, NULL, 'Faltantes en inventarios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(409, NULL, '6101050', NULL, NULL, 'Alquiler de edificio', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(410, NULL, '6101051', NULL, NULL, 'Alquiler compañías relacionadas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(411, NULL, '6101052', NULL, NULL, 'Alquiler de equipo de informática', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(412, NULL, '6101053', NULL, NULL, 'Arrendamientos vehículos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(413, NULL, '6101054', NULL, NULL, 'Servicio de seguridad', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(414, NULL, '6101055', NULL, NULL, 'Traslado de valores', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(415, NULL, '6101056', NULL, NULL, 'Servicio de limpieza y mantenimiento', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(416, NULL, '6101057', NULL, NULL, 'Servicio de mensajería', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(417, NULL, '6101058', NULL, NULL, 'Otros servicios. Subcontratados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(418, NULL, '6101059', NULL, NULL, 'Servicios contratados propios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(419, NULL, '6101060', NULL, NULL, 'Contratos servicio profesionales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(420, NULL, '6101061', NULL, NULL, 'Otros servicios externos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(421, NULL, '6101062', NULL, NULL, 'Deducibles seguros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(422, NULL, '6101063', NULL, NULL, 'Costo por servicios prestados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(423, NULL, '6101064', NULL, NULL, 'Honorarios legales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(424, NULL, '6101065', NULL, NULL, 'Honorarios de auditoría externa', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(425, NULL, '6101066', NULL, NULL, 'Otros honorarios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(426, NULL, '6101067', NULL, NULL, 'Mantenimiento maquinaria y herramientas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(427, NULL, '6101068', NULL, NULL, 'Mantenimiento software', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(428, NULL, '6101069', NULL, NULL, 'Comunicaciones (Internet)', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(429, NULL, '6101070', NULL, NULL, 'Asesoría', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(430, NULL, '6101071', NULL, NULL, 'Servicios hosting', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(431, NULL, '6101072', NULL, NULL, 'Teléfono', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(432, NULL, '6101073', NULL, NULL, 'Energía eléctrica', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(433, NULL, '6101074', NULL, NULL, 'Agua', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(434, NULL, '6101075', NULL, NULL, 'Gas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(435, NULL, '6101076', NULL, NULL, 'Suscripciones oficiales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(436, NULL, '6101077', NULL, NULL, 'Teléfonos vendedores', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(437, NULL, '6101078', NULL, NULL, 'Seguros incendios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(438, NULL, '6101079', NULL, NULL, 'Seguro sobre vehículos recuperados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(439, NULL, '6101080', NULL, NULL, 'Seguros vehículos de la compañía', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(440, NULL, '6101081', NULL, NULL, 'Seguro contra robo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(441, NULL, '6101082', NULL, NULL, 'Flete, transporte expreso y embalaje', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(442, NULL, '6101083', NULL, NULL, 'Diferencial en provisiones retaceos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(443, NULL, '6101084', NULL, NULL, 'Producción', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(444, NULL, '6101085', NULL, NULL, 'Medios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(445, NULL, '6101086', NULL, NULL, 'Correo directo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(446, NULL, '6101087', NULL, NULL, 'Estudios de investigación de mercado', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(447, NULL, '6101088', NULL, NULL, 'Mercadeo institución', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(448, NULL, '6101089', NULL, NULL, 'Suscripciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(449, NULL, '6101090', NULL, NULL, 'Promoción del mes', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(450, NULL, '6101091', NULL, NULL, 'Apoyos publicitarios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(451, NULL, '6101092', NULL, NULL, 'Pasajes e impuestos de viaje', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(452, NULL, '6101093', NULL, NULL, 'Hospedaje y alimentación exterior', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(453, NULL, '6101094', NULL, NULL, 'Otros exteriores', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(454, NULL, '6101095', NULL, NULL, 'Pasajes e impuestos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(455, NULL, '6101096', NULL, NULL, 'Hospedaje y alimentación', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(456, NULL, '6101097', NULL, NULL, 'Gastos de viaje otros interiores', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(457, NULL, '6101098', NULL, NULL, 'Depreciación edificios e instalaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(458, NULL, '6101099', NULL, NULL, 'Depreciación equipo de computo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(459, NULL, '61010100', NULL, NULL, 'Depreciación equipo diversos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(460, NULL, '61010101', NULL, NULL, 'Depreciación herramientas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(461, NULL, '61010102', NULL, NULL, 'Depreciación maquinaria', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(462, NULL, '61010103', NULL, NULL, 'Depreciación mejoras en propiedades arrendadas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(463, NULL, '61010104', NULL, NULL, 'Depreciación mobiliario y equipo', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(464, NULL, '61010105', NULL, NULL, 'Depreciación rótulos publicitarios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(465, NULL, '61010106', NULL, NULL, 'Depreciación vehículos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(466, NULL, '61010107', NULL, NULL, 'Depreciación revalúo edificios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(467, NULL, '61010108', NULL, NULL, 'Amortizaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(468, NULL, '61010109', NULL, NULL, 'Amortización marcas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(469, NULL, '61010110', NULL, NULL, 'Amortización software', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(470, NULL, '61010111', NULL, NULL, 'Amortizaciones intangibles', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(471, NULL, '61010112', NULL, NULL, 'Estimación cuentas incobrables', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(472, NULL, '61010113', NULL, NULL, 'Obsolescencia de inventarios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(473, NULL, '61010114', NULL, NULL, 'Matrículas y traspasos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(474, NULL, '61010115', NULL, NULL, 'Demostración y gastos de entrega', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(475, NULL, '61010116', NULL, NULL, 'Trabajos de limpieza de vehículos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(476, NULL, '61010117', NULL, NULL, 'Revisiones de vehículos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(477, NULL, '61010118', NULL, NULL, 'Impuestos localidades', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(478, NULL, '61010119', NULL, NULL, 'Tasas locales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(479, NULL, '61010120', NULL, NULL, 'Aportaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(480, NULL, '61010121', NULL, NULL, 'Matrículas de comercio', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(481, NULL, '61010122', NULL, NULL, 'Derechos de registros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(482, NULL, '61010123', NULL, NULL, 'Impuestos al activo neto', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(483, NULL, '61010124', NULL, NULL, 'Impuesto sobre la renta', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(484, NULL, '61010125', NULL, NULL, 'Impuestos sobre transferencias financieras', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(485, NULL, '61010126', NULL, NULL, 'Impuesto sobre la renta diferido', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(486, '6102', NULL, NULL, NULL, 'Gastos por donaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(487, NULL, '610201', NULL, NULL, 'Donaciones parte deducible de impuestos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(488, NULL, '610202', NULL, NULL, 'Donaciones parte no deducible', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(489, '6103', NULL, NULL, NULL, 'Otros gastos operacionales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(490, NULL, '610301', NULL, NULL, 'Perdidas en venta de activos fijos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(491, NULL, '610302', NULL, NULL, 'Pérdidas por bajas de activos fijos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(492, NULL, '610303', NULL, NULL, 'Otros honorarios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(493, NULL, '610304', NULL, NULL, 'Gastos no deducibles', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(494, NULL, '610305', NULL, NULL, 'Gastos no deducibles perdida cartera', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(495, NULL, '610306', NULL, NULL, 'Gastos no deducibles perdida activo eventual', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(496, NULL, '610307', NULL, NULL, 'Gastos no deducibles perdida seguros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(497, NULL, '610308', NULL, NULL, 'Tasa de seguridad', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(498, NULL, '610309', NULL, NULL, 'Impuesto municipal', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(499, NULL, '610310', NULL, NULL, 'Perdidas venta activos fijos (revalúo)', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(500, NULL, '610311', NULL, NULL, 'Gastos de licitaciones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(501, NULL, '610312', NULL, NULL, 'Multas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(502, NULL, '610313', NULL, NULL, 'Retenciones de intereses', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(503, NULL, '610314', NULL, NULL, 'Retenciones de honorarios', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(504, NULL, '610315', NULL, NULL, 'Otros gastos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(505, NULL, '610316', NULL, NULL, 'Otros gastos perdida en cartera', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(506, NULL, '610317', NULL, NULL, 'Costos de activos no depreciables', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(507, NULL, '610318', NULL, NULL, 'Gastos compañías relacionadas', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(508, NULL, '610319', NULL, NULL, 'Variación en inversión (gasto)', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(509, '7', NULL, NULL, NULL, 'Cuenta de resultado acreedora- otros ingresos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(510, '71', NULL, NULL, NULL, 'Otros ingresos de actividad no ordinaria', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(511, '7101', NULL, NULL, NULL, 'Otros ingresos no operacionales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(512, NULL, '710101', NULL, NULL, 'Intereses ganados', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(513, NULL, '710102', NULL, NULL, 'Comisiones bancarias', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(514, NULL, '710103', NULL, NULL, 'Ganancias en diferencial cambiario', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(515, NULL, '710104', NULL, NULL, 'Ganancias valoración moneda extranjera', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(516, NULL, '710105', NULL, NULL, 'Descuentos obtenidos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(517, '72', NULL, NULL, NULL, 'Otros productos', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(518, '7201', NULL, NULL, NULL, 'Productos financieros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(519, NULL, '720101', NULL, NULL, 'Intereses financieros', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(520, NULL, '720102', NULL, NULL, 'Intereses comerciales', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(521, NULL, '720103', NULL, NULL, 'Comisiones bancarias', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(522, NULL, '720104', NULL, NULL, 'Comisiones por intermediación', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(523, NULL, '720105', NULL, NULL, 'Diferencial cambiario', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(524, NULL, '720106', NULL, NULL, 'Perdidas en valoración moneda extranjera', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(525, NULL, '720107', NULL, NULL, 'Comisiones por valuación de inversiones', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(526, NULL, '720108', NULL, NULL, 'Descuentos por pronto pago', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(527, NULL, '720109', NULL, NULL, 'Comisión tarjeta de crédito', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(528, '8', NULL, NULL, NULL, 'Cuentas liquidadoras', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(529, '81', NULL, NULL, NULL, 'Pérdidas y ganancias', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(530, '8101', NULL, NULL, NULL, 'Pérdidas y ganancias', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(531, NULL, '810101', NULL, NULL, 'Pérdidas y ganancias', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(532, '9', NULL, NULL, NULL, 'Cuentas de orden', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(533, '91', NULL, NULL, NULL, 'Cuentas de orden', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(534, '9101', NULL, NULL, NULL, 'Cuentas de orden', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(535, NULL, '910101', NULL, NULL, 'Cuentas de orden', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52'),
(536, NULL, '910102', NULL, NULL, 'Cuentas de orden por contra', '1', NULL, 0, NULL, NULL, NULL, '2023-02-19 00:38:52', '2023-02-19 00:38:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clases`
--

CREATE TABLE `clases` (
  `id` int(11) NOT NULL,
  `idFilum` int(11) DEFAULT NULL,
  `nombreClase` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coleccions`
--

CREATE TABLE `coleccions` (
  `id` int(11) NOT NULL,
  `nombreColeccion` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contribuyentes`
--

CREATE TABLE `contribuyentes` (
  `id` int(11) NOT NULL,
  `tipo` char(255) DEFAULT NULL,
  `tipo2` char(255) DEFAULT NULL,
  `nombreContrib` char(255) DEFAULT NULL,
  `noIdentif` char(25) DEFAULT NULL,
  `noRegistro` char(25) DEFAULT NULL,
  `giro` char(255) DEFAULT NULL,
  `direccion` char(255) DEFAULT NULL,
  `categoria` char(255) DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `estatus2` char(2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `contribuyentes`
--

INSERT INTO `contribuyentes` (`id`, `tipo`, `tipo2`, `nombreContrib`, `noIdentif`, `noRegistro`, `giro`, `direccion`, `categoria`, `estatus`, `estatus2`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, 'Diego', '123456', NULL, 'p', 'prueba direccion', '1', NULL, NULL, '2023-07-30 03:19:28', '2023-07-30 03:19:28'),
(2, NULL, NULL, 'prueba contribuyente', '654321', NULL, '654654', 'prueba direccion2', '1', NULL, NULL, '2023-07-30 03:50:25', '2023-07-30 03:50:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id` int(11) NOT NULL,
  `nombreDepto` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleusuarios`
--

CREATE TABLE `detalleusuarios` (
  `id` int(11) NOT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `idRol` int(11) DEFAULT NULL,
  `permisoInvestigacion` char(255) NOT NULL,
  `fechaInicioPermiso` date NOT NULL,
  `fechaFinPermiso` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dominios`
--

CREATE TABLE `dominios` (
  `id` int(11) NOT NULL,
  `idReino` int(11) DEFAULT NULL,
  `nombreDominio` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especieamenazadas`
--

CREATE TABLE `especieamenazadas` (
  `id` int(11) NOT NULL,
  `idRiesgo` int(11) DEFAULT NULL,
  `nomEspamen` char(255) NOT NULL,
  `nomComEspamen` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especies`
--

CREATE TABLE `especies` (
  `id` int(11) NOT NULL,
  `idGenero` int(11) DEFAULT NULL,
  `idTaxonomia` int(11) DEFAULT NULL,
  `idEspamen` int(11) DEFAULT NULL,
  `nombreEspecie` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `especimens`
--

CREATE TABLE `especimens` (
  `id` int(11) NOT NULL,
  `idTaxonomia` int(11) DEFAULT NULL,
  `idSecuencia` int(11) DEFAULT NULL,
  `idEspecie` int(11) DEFAULT NULL,
  `idInvestigacion` int(11) DEFAULT NULL,
  `fechaColecta` date NOT NULL,
  `horaSecuenciacion1` time NOT NULL,
  `colector` char(255) NOT NULL,
  `codigoEspecimen` char(255) NOT NULL,
  `latitud` float NOT NULL,
  `longitud` float NOT NULL,
  `tecnicaRecoleccion` char(255) NOT NULL,
  `cantidad` char(255) NOT NULL,
  `tipoMuestra` char(255) NOT NULL,
  `caracteristicas` char(255) NOT NULL,
  `peso` float NOT NULL,
  `tamano` float NOT NULL,
  `habitat` char(255) NOT NULL,
  `imagen` char(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familias`
--

CREATE TABLE `familias` (
  `id` int(11) NOT NULL,
  `idOrden` int(11) DEFAULT NULL,
  `nombreFamilia` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `filums`
--

CREATE TABLE `filums` (
  `id` int(11) NOT NULL,
  `idReino` int(11) DEFAULT NULL,
  `nombreFilum` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `generos`
--

CREATE TABLE `generos` (
  `id` int(11) NOT NULL,
  `idFamilia` int(11) DEFAULT NULL,
  `nombreGenero` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `investigacions`
--

CREATE TABLE `investigacions` (
  `id` int(11) NOT NULL,
  `idUsuario` int(11) DEFAULT NULL,
  `idTipo` int(11) DEFAULT NULL,
  `idZona` int(11) DEFAULT NULL,
  `nombreInv` char(255) NOT NULL,
  `fechaIngreso` date NOT NULL,
  `lugarInv` char(255) NOT NULL,
  `responsableInv` char(255) NOT NULL,
  `objetivo` char(255) NOT NULL,
  `contacto` char(255) NOT NULL,
  `unidadEncargada` char(255) NOT NULL,
  `otrasInstit` char(255) DEFAULT NULL,
  `documentacion` char(255) NOT NULL,
  `descripcionInvestigacion` char(255) NOT NULL,
  `correoElectronico` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libcompras`
--

CREATE TABLE `libcompras` (
  `id` int(11) NOT NULL,
  `tipoDoc` char(255) DEFAULT NULL,
  `tipo2` char(255) DEFAULT NULL,
  `ccf` varchar(50) DEFAULT NULL,
  `fechaccf` date DEFAULT NULL,
  `mes` char(255) DEFAULT NULL,
  `año` char(20) DEFAULT NULL,
  `idContribuyente` char(20) DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `montoGravado` double DEFAULT NULL,
  `montoNoSujeto` double DEFAULT NULL,
  `montoExento` double DEFAULT NULL,
  `sneto` double DEFAULT NULL,
  `iva` double DEFAULT NULL,
  `riva` double DEFAULT NULL,
  `renta` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `libcompras`
--

INSERT INTO `libcompras` (`id`, `tipoDoc`, `tipo2`, `ccf`, `fechaccf`, `mes`, `año`, `idContribuyente`, `estatus`, `montoGravado`, `sneto`, `iva`, `riva`, `renta`, `created_at`, `updated_at`) VALUES
(5, 'Compra Local', NULL, '100', '2023-04-01', '04', '2023', 'prueba contribuyente', '1', 100, 88.5, 11.5, NULL, NULL, '2023-04-02 09:44:00', '2023-04-02 09:44:00'),
(6, 'Compra Local', 'En blanco', '1a', '2023-05-10', '06', '2023', 'prueba contribuyente', '1', 113, 100, 13, 0, 0, '2023-05-11 17:39:59', '2023-05-11 17:39:59'),
(7, 'Compra Local', 'Iva', '1b', '2023-05-10', '05', '2023', 'prueba contribuyente', '1', 100, 88.5, 11.5, 0, 0, '2023-05-11 17:39:59', '2023-05-11 17:39:59'),
(8, 'Compra Local', 'Iva', '1a', '2023-08-01', '08', '2023', 'Diego', '1', 100, 88.5, 11.5, 0, 0, '2023-08-02 09:48:52', '2023-08-02 09:48:52');

-- --------------------------------------------------------

CREATE TABLE `libventas` (
  `id` int(11) NOT NULL,
  `correlativo` int(11) DEFAULT NULL,
  `tipoDoc` char(255) DEFAULT NULL,
  `tipo2` char(255) DEFAULT NULL,
  `ccf` varchar(50) DEFAULT NULL,
  `fechaccf` date DEFAULT NULL,
  `mes` char(255) DEFAULT NULL,
  `año` char(20) DEFAULT NULL,
  `idContribuyente` char(20) DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `montoGravado` double DEFAULT NULL,
  `montoNoSujeto` double DEFAULT NULL,
  `montoExento` double DEFAULT NULL,
  `sneto` double DEFAULT NULL,
  `iva` double DEFAULT NULL,
  `riva` double DEFAULT NULL,
  `renta` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ---------------------------------------------------------

CREATE TABLE `libventascs` (
  `id` int(11) NOT NULL,
  `correlativo` int(11) DEFAULT NULL,
  `tipoDoc` char(255) DEFAULT NULL,
  `tipo2` char(255) DEFAULT NULL,
  `ccf` varchar(50) DEFAULT NULL,
  `fechaccf` date DEFAULT NULL,
  `mes` char(255) DEFAULT NULL,
  `año` char(20) DEFAULT NULL,
  `idContribuyente` char(20) DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `montoGravado` double DEFAULT NULL,
  `montoNoSujeto` double DEFAULT NULL,
  `montoExento` double DEFAULT NULL,
  `sneto` double DEFAULT NULL,
  `iva` double DEFAULT NULL,
  `riva` double DEFAULT NULL,
  `renta` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Estructura de tabla para la tabla `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(1, 'App\\User', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios`
--

CREATE TABLE `municipios` (
  `id` int(11) NOT NULL,
  `idDepto` int(11) DEFAULT NULL,
  `nombreMunicipio` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `obtenido_ens`
--

CREATE TABLE `obtenido_ens` (
  `id` int(11) NOT NULL,
  `idInv` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordens`
--

CREATE TABLE `ordens` (
  `id` int(11) NOT NULL,
  `idClase` int(11) DEFAULT NULL,
  `nombreOrden` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `id` int(11) NOT NULL,
  `idprest` char(255) DEFAULT NULL,
  `idprest2` char(255) DEFAULT NULL,
  `prestamo` varchar(50) DEFAULT NULL,
  `fechadepago` char(255) DEFAULT NULL,
  `cantidad` char(255) DEFAULT NULL,
  `intereses` char(255) DEFAULT NULL,
  `capitalpagado` double DEFAULT NULL,
  `diasIntereses` int(200) DEFAULT NULL,
  `comentario` char(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `pagos`
--

INSERT INTO `pagos` (`id`, `idprest`, `idprest2`, `prestamo`, `fechadepago`, `cantidad`, `intereses`, `capitalpagado`, `diasIntereses`, `comentario`, `created_at`, `updated_at`) VALUES
(1, '1', '1', '1065.52', '2022-11-11', '400', '400', 0.0000000000000568434, 30, 'Solo pago los intereses del mes', NULL, NULL),
(2, '2', '2', '', '2022-11-24', '310', '280', 30, 30, 'Primer pago', NULL, NULL),
(3, '1', '1', '1065.52', '2022-12-19', '400', '400', 0.0000000000000568434, 30, '', NULL, NULL),
(4, '4', '3', '72.47', '2022-11-24', '72.47', '50', 22.47, 30, '', NULL, NULL),
(5, '3', '4', '281.67', '2022-11-29', '282.00', '238.8', 43.2, 30, '', NULL, NULL),
(6, '6', '5', '397.82', '2022-12-21', '400.00', '360', 40, 30, '', NULL, NULL),
(7, '2', '2', '', '2022-12-24', '310', '278.8', 31.2, 30, '', NULL, NULL),
(8, '5', '6', '181.18', '2023-01-09', '185', '125', 60, 30, '', NULL, NULL),
(9, '4', '3', '72.47', '2022-12-27', '72.47', '48.8765', 23.5935, 30, '', NULL, NULL),
(10, '6', '5', '--Seleccione Préstamo--', '2023-01-16', '9270.61', '310.613', 8960, 26, 'Con este abono cancela su crédito', NULL, NULL),
(11, '2', '2', '', '2023-01-24', '310', '277.552', 32.448, 30, '', NULL, NULL),
(12, '8', '9', '78.98', '2023-01-20', '78.98', '35', 43.98, 30, '', NULL, NULL),
(13, '10', '11', '98.51', '2023-01-27', '120', '25', 95, 30, '', NULL, NULL),
(14, '3', '4', '281.67', '2022-12-29', '290.00', '237.072', 52.928, 30, '', NULL, NULL),
(15, '5', '6', '181.18', '2023-02-09', '185.00', '122', 63, 30, '', NULL, NULL),
(16, '10', '11', '98.51', '2023-02-10', '413.78', '8.78', 405, 13, '', NULL, NULL),
(17, '3', '4', '--Seleccione Préstamo--', '2023-01-29', '290.00', '234.955', 55.0452, 30, '', NULL, NULL),
(18, '7', '7', '--Seleccione Préstamo--', '2022-12-19', '194.00', '175.2', 18.8, 30, '', NULL, NULL),
(19, '7', '8', '--Seleccione Préstamo--', '2022-12-19', '94.00', '84.8', 9.2, 30, '', NULL, NULL),
(20, '1', '1', '--Seleccione Préstamo--', '2023-01-11', '400.00', '400', 0.0000000000000568434, 30, '', NULL, NULL),
(21, '1', '13', '1065.52', '2023-01-12', '175.20', '175.2', 0, 30, '', NULL, NULL),
(22, '1', '13', '1065.52', '2023-02-12', '175.20', '175.2', 0, 30, '', NULL, NULL),
(23, '1', '1', '1065.52', '2023-02-11', '400', '400', 0.0000000000000568434, 30, '', NULL, NULL),
(24, '2', '2', '', '2023-02-24', '310', '276.254', 33.746, 30, '', NULL, NULL),
(25, '11', '10', '30.20', '2023-02-21', '30.22', '25', 5.22, 30, '', NULL, NULL),
(26, '5', '6', '--Seleccione Préstamo--', '2023-03-09', '185', '118.85', 66.15, 30, '', NULL, NULL),
(27, '2', '2', '', '2023-03-23', '310.00', '274.904', 35.096, 30, '', NULL, NULL),
(28, '2', '19', '', '2023-03-20', '300', '75', 225, 30, '', NULL, NULL),
(29, '3', '4', '--Seleccione Préstamo--', '2023-02-28', '290', '232.753', 57.2472, 30, '', NULL, NULL),
(30, '8', '9', '--Seleccione Préstamo--', '2023-02-20', '78.98', '32.801', 46.179, 30, '', NULL, NULL),
(31, '11', '10', '30.20', '2023-03-21', '30.22', '24.739', 5.481, 30, '', NULL, NULL),
(32, '13', '15', '763.05', '2023-03-17', '160.00', '160', 0, 30, '', NULL, NULL),
(33, '13', '16', '763.05', '2023-03-17', '168.00', '168', 0, 30, '', NULL, NULL),
(34, '13', '17', '763.05', '2023-03-17', '40.00', '40', 0, 30, '', NULL, NULL),
(35, '13', '18', '--Seleccione Préstamo--', '2023-03-17', '172.00', '172', 0, 30, '', NULL, NULL),
(36, '5', '6', '--Seleccione Préstamo--', '2023-04-10', '185', '115.543', 69.4575, 30, '', NULL, NULL),
(37, '8', '9', '78.98', '2023-03-20', '80', '30.4921', 49.5079, 30, '', NULL, NULL),
(38, '12', '14', '--Seleccione Préstamo--', '2023-03-14', '111.30', '65', 46.3, 30, '', NULL, NULL),
(39, '4', '3', '72.47', '2023-03-31', '72.47', '47.6968', 24.7731, 30, '', NULL, NULL),
(40, '4', '3', '72.47', '2023-03-31', '72.47', '46.4582', 26.0118, 30, '', NULL, NULL),
(41, '2', '2', '', '2023-04-24', '310', '273.5', 36.5, 30, '', NULL, NULL),
(42, '3', '4', '281.67', '2023-04-13', '290', '230.463', 59.5372, 30, '', NULL, NULL),
(43, '8', '9', '78.98', '2023-04-28', '80', '28.0166', 51.9833, 30, '', NULL, NULL),
(44, '11', '10', '30.20', '2023-04-27', '30.22', '24.465', 5.75505, 30, '', NULL, NULL),
(45, '12', '14', '111.21', '2023-04-14', '111.25', '62.685', 48.565, 30, '', NULL, NULL),
(46, '13', '20', '763.05', '2023-05-02', '1560', '60', 1500, 30, '', NULL, NULL),
(47, '2', '19', '', '2023-04-21', '300', '63.75', 236.25, 30, '', NULL, NULL),
(48, '14', '21', '120.87', '2023-04-14', '120.87', '100', 20.87, 30, '', NULL, NULL),
(49, '15', '22', '151.09', '2023-04-17', '165', '125', 40, 30, '', NULL, NULL),
(50, '17', '24', '67.70', '2023-04-18', '70', '30', 40, 30, '', NULL, NULL),
(51, '16', '23', '763.05', '2023-04-22', '763.05', '160', 603.05, 30, '', NULL, NULL),
(52, '18', '25', '154.71', '2023-05-03', '155', '140', 15, 30, '', NULL, NULL),
(53, '7', '8', '193.60', '2023-05-03', '100', '84.432', 15.568, 30, '', NULL, NULL),
(54, '7', '7', '193.60', '2023-05-03', '200', '174.448', 25.552, 30, '', NULL, NULL),
(55, '5', '6', '181.18', '2023-05-09', '185', '112.07', 72.9305, 30, '', NULL, NULL),
(56, '12', '14', '111.21', '2023-05-16', '111.25', '60.257', 50.993, 30, '', NULL, NULL),
(57, '14', '21', '120.87', '2023-05-13', '120.87', '98.9565', 21.9135, 30, '', NULL, NULL),
(58, '15', '22', '151.09', '2023-05-15', '170', '123', 47, 30, '', NULL, NULL),
(59, '20', '27', '197.02', '2023-05-04', '1050', '50', 1000, 30, '', NULL, NULL),
(60, '16', '23', '763.05', '2023-05-23', '763.05', '135.878', 627.172, 30, '', NULL, NULL),
(61, '3', '4', '281.67', '2023-05-25', '300', '228.081', 71.9188, 30, '', NULL, NULL),
(62, '3', '30', '281.67', '2023-05-25', '100', '100', 0, 30, '', NULL, NULL),
(63, '2', '2', '', '2023-05-22', '310', '272.04', 37.96, 30, '', NULL, NULL),
(64, '2', '19', '', '2023-05-22', '300', '51.9375', 248.062, 30, '', NULL, NULL),
(65, '8', '9', '78.98', '2023-05-25', '80', '25.4175', 54.5825, 30, '', NULL, NULL),
(66, '11', '10', '30.20', '2023-05-29', '30.25', '24.1772', 6.0728, 30, '', NULL, NULL),
(67, '17', '24', '67.70', '2023-05-19', '70', '28', 42, 30, '', NULL, NULL),
(68, '18', '25', '154.71', '2023-05-30', '155', '139.4', 15.6, 30, '', NULL, NULL),
(69, '19', '26', '123.20', '2023-05-24', '123.20', '85', 38.2, 30, '', NULL, NULL),
(70, '21', '28', '1549.50', '2023-05-29', '172', '172', 0, 30, '', NULL, NULL),
(71, '21', '29', '1549.50', '2023-05-29', '88', '88', 0, 30, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partidacs`
--

CREATE TABLE `partidacs` (
  `id` int(11) NOT NULL,
  `idcatalogo` varchar(255) DEFAULT NULL,
  `tipo` char(255) DEFAULT NULL,
  `tipo2` char(255) DEFAULT NULL,
  `correlativo` char(255) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `descripcion` char(255) DEFAULT NULL,
  `saldoInicial` double DEFAULT NULL,
  `debe` double DEFAULT NULL,
  `haber` double DEFAULT NULL,
  `saldo` double DEFAULT NULL,
  `estatus` char(10) DEFAULT NULL,
  `estatus2` char(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `partidacs`
--

INSERT INTO `partidacs` (`id`, `idcatalogo`, `tipo`, `tipo2`, `correlativo`, `fecha`, `descripcion`, `saldoInicial`, `debe`, `haber`, `saldo`, `estatus`, `estatus2`, `created_at`, `updated_at`) VALUES
(1, '11010401', '1', 'Debe', '1', '2023-08-04', 'Prestamo nuevo', NULL, 100, 0, NULL, '1', NULL, '2023-08-05 06:06:51', '2023-08-05 06:06:51'),
(2, '11010401', '1', 'Haber', '1', '2023-08-04', 'Prestamo nuevo', NULL, 0, 100, NULL, '1', NULL, '2023-08-05 06:06:51', '2023-08-05 06:06:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `guard_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Roles', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(2, 'Crear Rol', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(3, 'Editar Rol', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(4, 'Eliminar Rol', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(5, 'Usuarios', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(6, 'Crear Usuarios', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(7, 'Editar Usuarios', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(8, 'Eliminar Usuarios', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(9, 'Departamentos', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(10, 'Crear Departamentos', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(11, 'Editar Departamentos', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(12, 'Eliminar Departamentos', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(13, 'Colecciones', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(14, 'Crear Colecciones', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(15, 'Editar Colecciones', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(16, 'Eliminar Colecciones', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(17, 'EspeciesAmenazadas', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(18, 'Crear EspecieAmenazada', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(19, 'Editar EspecieAmenazada', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(20, 'Eliminar EspecieAmenazada', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(21, 'Zonas', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(22, 'Crear Zona', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(23, 'Editar Zona', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(24, 'Eliminar Zona', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(25, 'Secuencias', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(26, 'Crear Secuencia', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(27, 'Editar Secuencia', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(28, 'Eliminar Secuencia', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(29, 'TiposInvestigaciones', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(30, 'Crear TipoInvestigacion', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(31, 'Editar TipoInvestigacion', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(32, 'Eliminar TipoInvestigacion', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(33, 'Riesgos', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(34, 'Crear Riesgo', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(35, 'Editar Riesgo', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(36, 'Eliminar Riesgo', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(37, 'Municipios', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(38, 'Crear Municipio', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(39, 'Editar Municipio', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(40, 'Eliminar Municipio', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(41, 'Taxonomias', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(42, 'Crear Taxonomia', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(43, 'Editar Taxonomia', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(44, 'Eliminar Taxonomia', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(45, 'Especimenes', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(46, 'Crear Especimen', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(47, 'Editar Especimen', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(48, 'Eliminar Especimen', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(49, 'Clases', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(50, 'Crear Clase', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(51, 'Editar Clase', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(52, 'Eliminar Clase', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(53, 'Dominios', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(54, 'Crear Dominio', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(55, 'Editar Dominio', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(56, 'Eliminar Dominio', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(57, 'Especies', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(58, 'Crear Especie', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(59, 'Editar Especie', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(60, 'Eliminar Especie', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(61, 'Familias', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(62, 'Crear Familia', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(63, 'Editar Familia', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(64, 'Eliminar Familia', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(65, 'Filums', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(66, 'Crear Filum', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(67, 'Editar Filum', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(68, 'Eliminar Filum', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(69, 'Generos', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(70, 'Crear Genero', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(71, 'Editar Genero', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(72, 'Eliminar Genero', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(73, 'Investigaciones', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(74, 'Crear Investigacion', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(75, 'Editar Investigacion', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(76, 'Eliminar Investigacion', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(77, 'Ordenes', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(78, 'Crear Orden', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(79, 'Editar Orden', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(80, 'Eliminar Orden', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(81, 'Reinos', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(82, 'Crear Reino', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(83, 'Editar Reino', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(84, 'Eliminar Reino', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(85, 'Secuencias', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(86, 'Crear Secuencia', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(87, 'Editar Secuencia', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(88, 'Eliminar Secuencia', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(89, 'Publicaciones', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(90, 'Crear Publicacion', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(91, 'Editar Publicacion', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(92, 'Eliminar Publicacion', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(93, 'Aprobaciones', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(94, 'Crear Aprobacion', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(95, 'Editar Aprobacion', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31'),
(96, 'Eliminar Aprobacion', 'web', '2021-02-24 16:24:31', '2021-02-24 16:24:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamos`
--

CREATE TABLE `prestamos` (
  `id` int(11) NOT NULL,
  `idprest` char(255) DEFAULT NULL,
  `codigoPrestamo` char(255) DEFAULT NULL,
  `periodicidad` char(255) DEFAULT NULL,
  `tasa` char(255) DEFAULT NULL,
  `tiempo` char(255) DEFAULT NULL,
  `fechaInicio` varchar(200) DEFAULT NULL,
  `agente` varchar(200) DEFAULT NULL,
  `estatus` varchar(10) DEFAULT NULL,
  `cantidad` char(255) DEFAULT NULL,
  `descrip` char(255) DEFAULT NULL,
  `pago` char(255) DEFAULT NULL,
  `fechadePago` char(25) DEFAULT NULL,
  `notas` varchar(100) DEFAULT NULL,
  `notas2` varchar(100) DEFAULT NULL,
  `notas3` varchar(100) DEFAULT NULL,
  `pendpago` char(255) DEFAULT NULL,
  `ultimoPago` varchar(20) DEFAULT NULL,
  `proximoPago` varchar(20) DEFAULT NULL,
  `estatus2` varchar(10) DEFAULT NULL,
  `fechapago` char(255) DEFAULT NULL,
  `intereses` char(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `prestamos`
--

INSERT INTO `prestamos` (`id`, `idprest`, `codigoPrestamo`, `periodicidad`, `tasa`, `tiempo`, `fechaInicio`, `agente`, `estatus`, `cantidad`, `descrip`, `pago`, `fechadePago`, `notas`, `notas2`, `notas3`, `pendpago`, `ultimoPago`, `proximoPago`, `estatus2`, `fechapago`, `intereses`, `created_at`, `updated_at`) VALUES
(1, '1', '11020158', NULL, '4', '1', '2022-10-11', 'Esmeralda Sandoval', '', '10000', 'Solo paga los intereses al final pagara intereses y capital', '1065.52', '11/11/2022', 'Esmeralda Sandoval', 'Fecha de pago 11 de cada mes', 'Pendiente', '10000', '2023-02-11', '2023-03-11', '0', NULL, NULL, NULL, NULL),
(2, '2', '11020114', NULL, '4', '5', '2022-10-24', 'Esmeralda Sandoval', '', '7000', '', '', '', 'Esmeralda Sandoval', '', 'Pendiente', '6763.04', '2023-05-22', '2023-06-22', '0', NULL, NULL, NULL, NULL),
(3, '4', '11020159', NULL, '5', '2', '2022-10-26', 'Esmeralda Sandoval', '', '1000', '', '72.47', '26/11/2022', 'Esmeralda Sandoval', '', 'Pendiente', '903.152', '2023-03-31', '2023-05-01', '0', NULL, NULL, NULL, NULL),
(4, '3', '11020139', NULL, '4', '4', '2022-10-29', 'Esmeralda Sandoval', '', '5970', '', '281.67', '29/11/2022', 'Esmeralda Sandoval', '', 'Pendiente', '5630.11', '2023-05-25', '2023-06-25', '0', NULL, NULL, NULL, NULL),
(5, '6', '1100160', NULL, '4', '5', '2022-11-21', 'Esmeralda Sandoval', '', '9000', '', '397.82', '21/12/2022', 'Esmeralda Sandoval', '', 'Pendiente', '0.00333333', '2023-01-16', '2023-02-16', '0', NULL, NULL, NULL, NULL),
(6, '5', '11020161', NULL, '5', '2', '2022-12-09', 'Esmeralda Sandoval', '', '2500', '', '181.18', '09/01/2023', 'Esmeralda Sandoval', '', 'Pendiente', '2168.46', '2023-05-09', '2023-06-09', '0', NULL, NULL, NULL, NULL),
(7, '7', '11020158', NULL, '4', '5', '2022-12-19', 'Esmeralda Sandoval', '', '4380', '', '193.60', '19/01/2023', 'Esmeralda Sandoval', 'Este es parte de un crédito de $6,500', 'Pendiente', '4335.65', '2023-05-03', '2023-06-03', '0', NULL, NULL, NULL, NULL),
(8, '7', '11020137-1', NULL, '4', '5', '2022-12-19', 'Esmeralda Sandoval', '', '2120', '', '93.71', '19/01/2023', 'Esmeralda Sandoval', 'Este es la otra parte de un crédito de $6,500\r\n', 'Pendiente', '2095.23', '2023-05-03', '2023-06-03', '0', NULL, NULL, NULL, NULL),
(9, '8', '11020134', NULL, '5', '1', '2022-12-20', 'Esmeralda Sandoval', '', '700', '', '78.98', '20/01/2023', 'Esmeralda Sandoval', '', 'Pendiente', '453.767', '2023-05-25', '2023-06-25', '0', NULL, NULL, NULL, NULL),
(10, '11', '11020164', NULL, '5', '3', '2023-01-21', 'Esmeralda Sandoval', '', '500', '', '30.20', '21/02/2023', 'Esmeralda Sandoval', '', 'Pendiente', '477.471', '2023-05-29', '2023-06-29', '0', NULL, NULL, NULL, NULL),
(11, '10', '11020163', NULL, '5', '0.5', '2023-01-10', 'Esmeralda Sandoval', '', '500', '', '98.51', '10/02/2023', 'Esmeralda Sandoval', '', 'Pendiente', '305.25', '2023-02-10', '2023-03-10', '0', NULL, NULL, NULL, NULL),
(12, '9', '11020162', NULL, '5', '0.16667', '2022-12-14', 'Esmeralda Sandoval', '', '200', '', '107.56', '14/01/2023', 'Esmeralda Sandoval', '', 'Pendiente', '200', '0', '0', '0', NULL, NULL, NULL, NULL),
(13, '1', '11020158', NULL, '4', '0.5', '2022-12-12', 'Esmeralda Sandoval', '', '4380', 'Solo pagara intereses 5 meses y en la sexta cuota intereses y capital', '835.54', '12/01/2023', 'Esmeralda Sandoval', '', 'Pendiente', '4380', '2023-02-12', '2023-03-12', '0', NULL, NULL, NULL, NULL),
(14, '12', '11020167', NULL, '5', '1.5', '2023-02-14', 'Esmeralda Sandoval', '', '1300', '', '111.21', '14/03/2023', 'Esmeralda Sandoval', '', 'Pendiente', '1154.15', '2023-05-16', '2023-06-16', '0', NULL, NULL, NULL, NULL),
(15, '13', '11020146', NULL, '4', '0.5', '2023-02-17', 'Esmeralda Sandoval', '', '4000', '', '763.05', '17/03/2023', 'Esmeralda Sandoval', '', 'Pendiente', '4000', '2023-03-17', '2023-04-17', '0', NULL, NULL, NULL, NULL),
(16, '13', '11020146', NULL, '4', '0.5', '2023-02-17', 'Esmeralda Sandoval', '', '4200', '', '801.20', '17/03/2023', 'Esmeralda Sandoval', '', 'Pendiente', '4200', '2023-03-17', '2023-04-17', '0', NULL, NULL, NULL, NULL),
(17, '13', '11020146', NULL, '4', '0.5', '2023-02-17', 'Esmeralda Sandoval', '', '1000', '', '190.76', '17/03/2023', 'Esmeralda Sandoval', '', 'Pendiente', '1000', '2023-03-17', '2023-04-17', '0', NULL, NULL, NULL, NULL),
(18, '13', '11020146', NULL, '4', '0.5', '2023-02-17', 'Esmeralda Sandoval', '', '4300', '', '820.28', '17/03/2023', 'Esmeralda Sandoval', '', 'Pendiente', '4300', '2023-03-17', '2023-04-17', '0', NULL, NULL, NULL, NULL),
(19, '2', '11020114', NULL, '5', '0.5', '2023-02-21', 'Esmeralda Sandoval', '', '1500', '', '295.53', '21/03/2023', 'Esmeralda Sandoval', '', 'Pendiente', '790.688', '2023-05-22', '2023-06-22', '0', NULL, NULL, NULL, NULL),
(20, '13', '11020146', NULL, '4', '0.5', '2023-03-17', 'Esmeralda Sandoval', '', '1500', '', '286.14', '17/04/2023', 'Esmeralda Sandoval', '', 'Pendiente', '0', '2023-05-02', '2023-06-02', '0', NULL, NULL, NULL, NULL),
(21, '14', '11020168', NULL, '5', '3', '2023-03-14', 'Esmeralda Sandoval', '', '2000', '', '120.87', '14/04/2023', 'Esmeralda Sandoval', '', 'Pendiente', '1957.22', '2023-05-13', '2023-06-13', '0', NULL, NULL, NULL, NULL),
(22, '15', '11020105', NULL, '5', '3', '2023-03-15', 'Esmeralda Sandoval', '', '2500', '', '151.09', '15/04/2023', 'Esmeralda Sandoval', '', 'Pendiente', '2413', '2023-05-15', '2023-06-15', '0', NULL, NULL, NULL, NULL),
(23, '16', '11020169', NULL, '4', '0.5', '2023-03-22', 'Esmeralda Sandoval', '', '4000', '', '763.05', '22/04/2023', 'Esmeralda Sandoval', '', 'Pendiente', '2769.78', '2023-05-23', '2023-06-23', '0', NULL, NULL, NULL, NULL),
(24, '17', '11020104', NULL, '5', '1', '2023-03-23', 'Esmeralda Sandoval', '', '600', '', '67.70', '23/04/2023', 'Esmeralda Sandoval', '', 'Pendiente', '518', '2023-05-19', '2023-06-19', '0', NULL, NULL, NULL, NULL),
(25, '18', '11020170', NULL, '4', '5', '2023-03-28', 'Esmeralda Sandoval', '', '3500', '', '154.71', '28/04/2023', 'Esmeralda Sandoval', '', 'Pendiente', '3469.4', '2023-05-30', '2023-06-30', '0', NULL, NULL, NULL, NULL),
(26, '19', '11020172', NULL, '5', '2', '2023-04-11', 'Esmeralda Sandoval', '', '1700', '', '123.20', '11/05/2023', 'Esmeralda Sandoval', '', 'Pendiente', '1661.8', '2023-05-24', '2023-06-24', '0', NULL, NULL, NULL, NULL),
(27, '20', '11020173', NULL, '5', '0.5', '2023-04-14', 'Esmeralda Sandoval', '', '1000', '', '197.02', '14/05/2023', 'Esmeralda Sandoval', '', 'Pendiente', '0', '2023-05-04', '2023-06-04', '0', NULL, NULL, NULL, NULL),
(28, '21', '11020176', NULL, '4', '0.25', '2023-04-24', 'Esmeralda Sandoval', '', '4300', '', '1549.50', '24/05/2023', 'Esmeralda Sandoval', '', 'Pendiente', '4300', '2023-05-29', '2023-06-29', '0', NULL, NULL, NULL, NULL),
(29, '21', '11020176', NULL, '4', '0.25', '2023-04-24', 'Esmeralda Sandoval', '', '2200', '', '792.77', '24/05/2023', 'Esmeralda Sandoval', '', 'Pendiente', '2200', '2023-05-29', '2023-06-29', '0', NULL, NULL, NULL, NULL),
(30, '3', '11020139-1', NULL, '5', '0.25', '2023-03-31', 'Esmeralda Sandoval', '', '2000', '', '734.42', '30/04/2023', 'Esmeralda Sandoval', '', 'Pendiente', '2000', '2023-05-25', '2023-06-25', '0', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestatarios`
--

CREATE TABLE `prestatarios` (
  `id` int(11) NOT NULL,
  `pnombre` char(255) DEFAULT NULL,
  `snombre` char(255) DEFAULT NULL,
  `papellido` char(255) DEFAULT NULL,
  `sapellido` char(255) DEFAULT NULL,
  `capellido` char(255) DEFAULT NULL,
  `dui` varchar(200) DEFAULT NULL,
  `email` char(255) DEFAULT NULL,
  `tel` char(25) DEFAULT NULL,
  `direccion` char(255) DEFAULT NULL,
  `direccion2` char(255) DEFAULT NULL,
  `ciudad` char(255) DEFAULT NULL,
  `municipio` char(255) DEFAULT NULL,
  `zip` char(255) DEFAULT NULL,
  `pais` char(255) DEFAULT NULL,
  `comentario` char(200) DEFAULT NULL,
  `cuenta` char(200) DEFAULT NULL,
  `balance` char(200) DEFAULT NULL,
  `imagen` char(200) DEFAULT NULL,
  `estatus` char(2) DEFAULT NULL,
  `estatus2` char(2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Volcado de datos para la tabla `prestatarios`
--

INSERT INTO `prestatarios` (`id`, `pnombre`, `snombre`, `papellido`, `sapellido`, `capellido`, `dui`, `email`, `tel`, `direccion`, `direccion2`, `ciudad`, `municipio`, `zip`, `pais`, `comentario`, `cuenta`, `balance`, `imagen`, `estatus`, `estatus2`, `created_at`, `updated_at`) VALUES
(1, 'DOUGLAS', 'LEONEL', 'LOPEZ', 'SURIA', '', '02802981-6', '', '76020524', 'BARRIO SAN NICOLAS PASAJE TERRANOVA CASA NUMERO 47', '', 'San Salvador', 'Tonacatepeque', '01101', 'SV', 'nuevo', NULL, '0.0', 'img/Douglas lopez.jpg', 'Pe', NULL, '2022-12-06 11:19:04', '2022-12-06 11:19:04'),
(2, 'JORGE', 'ALBERTO', ' RAMIREZ', 'HERNANDEZ', '', '01174546-9', '', '77041926', 'COLONIA LOS LLANITOS, CALLE PRINCIPAL, CASA NUMERO 8, BARRIO SAN NICOLAS, PASAJE TERRANOVA , CASA NUMERO 47', '', 'San Salvador', 'Tonacatepeque', '01101', 'SV', '', NULL, '0.0', 'img/en blanco.webp.png', 'Pe', NULL, '2022-12-14 11:19:04', '2022-12-14 11:19:04'),
(3, 'MAYRA', 'CORINA', 'MUÑOZ', 'TOBAR', '', '001991856', '', '74920848', 'COMP RESIDENSIAL AUSTRALIA SENDA2, POLIGONO C, CASA NUMERO 8', '', 'San Salvador', 'Mejicanos', '01101', 'SV', '', NULL, '0.0', 'img/Mayara Corina.jpg', 'Pe', NULL, '2022-12-15 11:19:04', '2022-12-15 11:19:04'),
(4, 'LUIS', 'ALONSO', 'LAZO', 'LUNA', '', '03073543-1', '', '75909166', 'BARRIO EL CALVARIO 6ª AVENIA SUR, CASA 304', '', 'San Miguel', 'San Miguel', '03301', 'SV', '', NULL, '0.0', 'img/1.png', 'Pe', NULL, '2022-12-28 11:19:04', '2022-12-28 11:19:04'),
(5, 'JOSE', 'ROLANDO', 'AMAYA', 'MEZA', '', '032600450-5', '', '61081437', 'RESIDENCIAL ALTA VISTA PAAJE 1 SUR, POLIGONO A CASA 342', '', 'San Salvador', 'San Salvador', '01101', 'SV', '', NULL, '0.0', 'img/2.png', 'Pe', NULL, '2022-12-28 11:19:04', '2022-12-28 11:19:04'),
(6, 'KEVIN', 'GIOVANNI', 'ESCOBAR', 'MUÑOZ', '', '05323088-2', '', '+ 505 58195471', 'COLONIA ESCALON, CALLE ARTURO ANBROGUI, SUITES MAFERRER', '', 'San Salvador', 'San Salvador', '01101', 'SV', '', NULL, '0.0', 'img/blanco 1.png', 'Pe', NULL, '2022-12-28 11:19:04', '2022-12-28 11:19:04'),
(7, 'JONATHAN', 'ESAU', 'GUZMAN', 'PARADA', '', '03392182-9', '', '62005385', 'BARRIO SAN NICOLAS, CALLE FRENTE A ALTOS DEL TEJAR, CASA 4', '', 'San Salvador', 'Tonacatepeque', '01101', 'SV', '', NULL, '0.0', 'img/ESAU.png', 'Pe', NULL, '2022-12-28 11:19:04', '2022-12-28 11:19:04'),
(8, 'EDWIN', 'ORLANDO', 'LEON', 'RIVAS', '', '03732100-5', '', '71994036', 'APOPA', '', 'San Salvador', 'Apopa', '01101', 'SV', '', NULL, '0.0', 'img/3.png', 'Pe', NULL, '2022-12-28 11:19:04', '2022-12-28 11:19:04'),
(9, 'FRANCISCO', 'ALBERTO', 'ZALDIVAR', 'CORTEZ', '', '03653863-3', '', '0000-0000', 'REPARTO PORTICOS DE SAN RAMON, CALLE AL VOLCAN PASAJE 1, CASA 7-A', '', 'San Salvador', 'San salvador', '01101', 'SV', '', NULL, '0.0', 'img/4.png', 'Pe', NULL, '2022-12-28 11:19:04', '2022-12-28 11:19:04'),
(10, 'ROBERTO', 'CARLOS', 'SANDOVAL', 'RUANO', '', '01732077-0', '', '78516059', 'CALLE ZONA 1 VERACRUZ, CAS VERACRUZ', '', 'San Salvador', 'Tonacatepeque', '01101', 'SV', '', NULL, '0.0', 'img/5.png', 'Pe', NULL, '2023-01-13 11:19:04', '2023-01-13 11:19:04'),
(11, 'ANA', 'DORIS', 'BELTRAN', 'DE MARROQUIN', '', '03846741-4', '', '75576969', 'KILOMETRO 4 1/2 CALLE A TONACATEPEQUE FRENTE A ESCUELA VERACRUZ', '', 'San Salvador', 'Tonacatepeque', '01101', 'SV', '', NULL, '0.0', 'img/Doris.png', 'Pe', NULL, '2023-01-25 11:19:04', '2023-01-25 11:19:04'),
(12, 'JOHANNA', 'JAMILA', 'PARADA', 'MONTES', '', '032791912', '', '73162237', 'Canton La Union', '', 'San Salvador', 'Tonacatepeque', '01101', 'SV', '', NULL, '0.0', 'img/JJ.png', 'Pe', NULL, '2023-02-24 11:19:04', '2023-02-24 11:19:04'),
(13, 'ELISA', 'ANABEL', 'SERRANO', 'ELIAS', '', '027554656', '', '70694850', 'BARRIO LAS MERCEDES 2A CALLE PONIENTE Y 8A AVENIDA NORTE Nº 30', '', 'San Salvador', 'Tonacatepeque', '01101', 'SV', '', NULL, '0.0', 'img/perfil.jpg', 'Pe', NULL, '2023-02-24 11:19:04', '2023-02-24 11:19:04'),
(14, 'JOSE', 'LEONARDO', 'CRUZ', 'RODRIGUEZ', '', '04903620-0', '', '76832670', 'Urbanización Sierra Morena 1 pasaje 10, polígono 20, casa # 624', '', 'San Salvador', 'Soyapango', '01101', 'SV', '', NULL, '0.0', 'img/Leonardo.png', 'Pe', NULL, '2023-03-27 11:19:04', '2023-03-27 11:19:04'),
(15, 'RICARDO', 'ERNESTO', 'ELIAS', 'MOLINA', '', '02642110-1', '', '6113-4753', 'Bosques del Matazano 1, pasaje L 3 casa 518', '', 'San Salvador', 'Soyapango', '01101', 'SV', '', NULL, '0.0', 'img/Richard.png', 'Pe', NULL, '2023-03-27 11:19:04', '2023-03-27 11:19:04'),
(16, 'JUAN', 'ARNOLDO', 'MELARA', 'CRESPIN', '', '000300537-6', '', '7907-6851', 'COLONIA BERNAL, PASAJE SALAZAR #2', '', 'San Salvador', 'San Salvador', '01101', 'SV', '', NULL, '0.0', 'img/juan melara.JPG.png', 'Pe', NULL, '2023-03-27 11:19:04', '2023-03-27 11:19:04'),
(17, 'GIOVANNI', 'ANTONIO', 'VASQUEZ', 'HERNANDEZ', '', '05475180-7', '', '7441-1609', 'Calle a Toncatepeque', '', 'San Salvador', 'Tonacatepeque', '01101', 'SV', '', NULL, '0.0', 'img/Giovanni.png', 'Pe', NULL, '2023-03-27 11:19:04', '2023-03-27 11:19:04'),
(18, 'JEFFERSON', 'FRANCISCO', 'DURAN', 'GUILLEN', '', '06044125-8', '', '75646138', 'Autopista de oro # 9 frente a gasolinera texaco, caserío Los Guillen', '', 'San Salvador', 'Tonacatepeque', '01101', 'SV', '', NULL, '0.0', 'img/jefferson.png', 'Pe', NULL, '2023-03-28 11:19:04', '2023-03-28 11:19:04'),
(19, 'BLANCA', 'XIOMARA', 'SERRANO', 'DE SORTO', '', '02328595-4', '', '7908-1320', 'BARRIO MERCEDEZ 2ª AVENIDA NORTE, #206', '', 'San Salvador', 'Tonacatepeque', '01101', 'SV', '', NULL, '0.0', 'img/XIOMARA.png', 'Pe', NULL, '2023-04-12 11:19:04', '2023-04-12 11:19:04'),
(20, 'OSCAR', 'EDILBERTO', 'LOPEZ', 'ELIAS', '', '05642510-1', '', '0000-0000', 'SAN SALVADOR', '', 'San Salvador', 'Tonacatepeque', '01101', 'SV', '', NULL, '0.0', 'img/azul.png', 'Pe', NULL, '2023-05-03 11:19:04', '2023-05-03 11:19:04'),
(21, 'JOSE', 'LUIS', 'AGUILAR', 'MENA', '', '02280321-2', '', '7461-7907', 'RESIDENCIAL CUMBRES DE CUSCATLAN AVENIDA QUEZALTEPEQUE POLIGONO E NUMERO15', '', 'La Libertad', 'Antiguo Cuscatlan', '05001', 'SV', '', NULL, '0.0', 'img/arqui2.png', 'Pe', NULL, '2023-05-03 11:19:04', '2023-05-03 11:19:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicacions`
--

CREATE TABLE `publicacions` (
  `id` int(11) NOT NULL,
  `nombrePublicacion` char(255) NOT NULL,
  `descripcionPub` char(255) NOT NULL,
  `url` char(255) DEFAULT NULL,
  `imagen` char(255) DEFAULT NULL,
  `fechaInicio` date DEFAULT NULL,
  `fechaFin` date DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reinos`
--

CREATE TABLE `reinos` (
  `id` int(11) NOT NULL,
  `idDominio` int(11) DEFAULT NULL,
  `nombreReino` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `riesgos`
--

CREATE TABLE `riesgos` (
  `id` int(11) NOT NULL,
  `catRiesgo` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `guard_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2021-02-24 16:25:35', '2021-02-24 16:25:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rols`
--

CREATE TABLE `rols` (
  `id` int(11) NOT NULL,
  `idDetalleUsuario` int(11) DEFAULT NULL,
  `nombreRol` char(255) NOT NULL,
  `estado` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secuencias`
--

CREATE TABLE `secuencias` (
  `id` int(11) NOT NULL,
  `secuenciaObtenida` char(255) NOT NULL,
  `metodoSecuenciacion` char(255) NOT NULL,
  `lugarSec` char(255) NOT NULL,
  `horaSec` time NOT NULL,
  `fechaSec` date NOT NULL,
  `responsableSec` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `se_realizans`
--

CREATE TABLE `se_realizans` (
  `id` int(11) NOT NULL,
  `idInv` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `taxonomias`
--

CREATE TABLE `taxonomias` (
  `id` int(11) NOT NULL,
  `idEspecie` int(11) DEFAULT NULL,
  `idColeccion` int(11) DEFAULT NULL,
  `idEspecimen` int(11) DEFAULT NULL,
  `NumVoucher` char(255) NOT NULL,
  `nombreComun` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoinvestigacions`
--

CREATE TABLE `tipoinvestigacions` (
  `id` int(11) NOT NULL,
  `idInv` int(11) DEFAULT NULL,
  `nombreTipo` char(255) NOT NULL,
  `descripcionTipo` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Diego', 'diego@gmail.com', NULL, '$2y$10$.N3WdI85CXw4D6C1HGUKreI39lDsHF7id1OKdZUDqwIBPuhmFOHQa', NULL, '2021-02-24 16:28:46', '2021-02-24 16:28:46');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `idDetalleUsuario` int(11) DEFAULT NULL,
  `nombreUsuario` char(255) NOT NULL,
  `correoElectronico1` char(255) NOT NULL,
  `password` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zonas`
--

CREATE TABLE `zonas` (
  `id` int(11) NOT NULL,
  `nombreZona` char(255) NOT NULL,
  `descripcionZona1` char(255) NOT NULL,
  `lugarZona` char(255) NOT NULL,
  `idDepto` int(11) DEFAULT NULL,
  `idMunicipio` int(11) DEFAULT NULL,
  `latitudZona` float NOT NULL,
  `longitudZona` float NOT NULL,
  `habitatZona` char(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_log_log_name_index` (`log_name`),
  ADD KEY `subject` (`subject_id`,`subject_type`),
  ADD KEY `causer` (`causer_id`,`causer_type`);

--
-- Indices de la tabla `aprobacions`
--
ALTER TABLE `aprobacions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_APROBACION_PERTENECE_A_UNA_INVESTIGACION` (`idInvestigacion`);

--
-- Indices de la tabla `catcuentas`
--
ALTER TABLE `catcuentas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `clases`
--
ALTER TABLE `clases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_CLASE_POSEE_UN_FILUM` (`idFilum`);

--
-- Indices de la tabla `coleccions`
--
ALTER TABLE `coleccions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contribuyentes`
--
ALTER TABLE `contribuyentes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalleusuarios`
--
ALTER TABLE `detalleusuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_DETALLEU_OBTIENE_ROL` (`idRol`),
  ADD KEY `FK_DETALLEU_POSEE2_USUARIO` (`idUsuario`);

--
-- Indices de la tabla `dominios`
--
ALTER TABLE `dominios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_DOMINIO_PERTENECE_REINO` (`idReino`);

--
-- Indices de la tabla `especieamenazadas`
--
ALTER TABLE `especieamenazadas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_ESPECIEA_TIENE_UNA_RIESGO` (`idRiesgo`);

--
-- Indices de la tabla `especies`
--
ALTER TABLE `especies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_ESPECIE_POSEE_UNA_TAXONOMI` (`idTaxonomia`),
  ADD KEY `FK_ESPECIE_PUEDE_SER_ESPECIEA` (`idEspamen`),
  ADD KEY `FK_ESPECIE_TIENE_____GENERO` (`idGenero`);

--
-- Indices de la tabla `especimens`
--
ALTER TABLE `especimens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_ESPECIME_POSEE_UNA_TAXONOMI` (`idTaxonomia`),
  ADD KEY `FK_SECUENCI_TIENE_____SECUENCIA` (`idSecuencia`),
  ADD KEY `FK_ESPECIMEN_PERTENECE_INVESTIGACION` (`idInvestigacion`);

--
-- Indices de la tabla `familias`
--
ALTER TABLE `familias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_FAMILIA_TIENE_UNA_ORDEN` (`idOrden`);

--
-- Indices de la tabla `filums`
--
ALTER TABLE `filums`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_FILUM_TIENE_____REINO` (`idReino`);

--
-- Indices de la tabla `generos`
--
ALTER TABLE `generos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_GENERO_PEERTENEC_FAMILIA` (`idFamilia`);

--
-- Indices de la tabla `investigacions`
--
ALTER TABLE `investigacions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_INVESTIG_PERTENECE_ZONA` (`idZona`),
  ADD KEY `FK_INVESTIG_RELATIONS_USUARIO` (`idUsuario`),
  ADD KEY `FK_INVESTIG_TIENE_UN_TIPOINVE` (`idTipo`);

--
-- Indices de la tabla `libcompras`
--
ALTER TABLE `libcompras`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indices de la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indices de la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_MUNICIPI_COMPUESTO_DEPARTAM` (`idDepto`);

--
-- Indices de la tabla `obtenido_ens`
--
ALTER TABLE `obtenido_ens`
  ADD PRIMARY KEY (`id`,`idInv`),
  ADD KEY `FK_OBTENIDO_OBTENIDO__INVESTIG` (`idInv`);

--
-- Indices de la tabla `ordens`
--
ALTER TABLE `ordens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_ORDEN_RELATIONS_CLASE` (`idClase`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `partidacs`
--
ALTER TABLE `partidacs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `prestamos`
--
ALTER TABLE `prestamos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `prestatarios`
--
ALTER TABLE `prestatarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `publicacions`
--
ALTER TABLE `publicacions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reinos`
--
ALTER TABLE `reinos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_REINO_PERTENECE_DOMINIO` (`idDominio`);

--
-- Indices de la tabla `riesgos`
--
ALTER TABLE `riesgos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `rols`
--
ALTER TABLE `rols`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_ROL_OBTIENE2_DETALLEU` (`idDetalleUsuario`);

--
-- Indices de la tabla `secuencias`
--
ALTER TABLE `secuencias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `se_realizans`
--
ALTER TABLE `se_realizans`
  ADD PRIMARY KEY (`id`,`idInv`),
  ADD KEY `FK_SE_REALI_SE_REALIZ_INVESTIG` (`idInv`);

--
-- Indices de la tabla `taxonomias`
--
ALTER TABLE `taxonomias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_TAXONOMI_POSEE_UNA_ESPECIME` (`idEspecimen`),
  ADD KEY `FK_TAXONOMI_POSEE_UNA_ESPECIE` (`idEspecie`),
  ADD KEY `FK_TAXONOMI_SE_CONFOR_COLECCIO` (`idColeccion`);

--
-- Indices de la tabla `tipoinvestigacions`
--
ALTER TABLE `tipoinvestigacions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_TIPOINVE_TIENE_UN2_INVESTIG` (`idInv`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_USUARIO_POSEE_DETALLEU` (`idDetalleUsuario`);

--
-- Indices de la tabla `zonas`
--
ALTER TABLE `zonas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_ZONA_PERTENECE_A_UN_MUNICIPIO` (`idMunicipio`),
  ADD KEY `FK_ZONA_PERTENECE_A_UN_DEPARTAMENTO` (`idDepto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52463;

--
-- AUTO_INCREMENT de la tabla `aprobacions`
--
ALTER TABLE `aprobacions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `catcuentas`
--
ALTER TABLE `catcuentas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=537;

--
-- AUTO_INCREMENT de la tabla `clases`
--
ALTER TABLE `clases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `coleccions`
--
ALTER TABLE `coleccions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `contribuyentes`
--
ALTER TABLE `contribuyentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalleusuarios`
--
ALTER TABLE `detalleusuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `dominios`
--
ALTER TABLE `dominios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `especieamenazadas`
--
ALTER TABLE `especieamenazadas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `especies`
--
ALTER TABLE `especies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `especimens`
--
ALTER TABLE `especimens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `familias`
--
ALTER TABLE `familias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `filums`
--
ALTER TABLE `filums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `generos`
--
ALTER TABLE `generos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `investigacions`
--
ALTER TABLE `investigacions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `libcompras`
--
ALTER TABLE `libcompras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `municipios`
--
ALTER TABLE `municipios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `obtenido_ens`
--
ALTER TABLE `obtenido_ens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `ordens`
--
ALTER TABLE `ordens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT de la tabla `partidacs`
--
ALTER TABLE `partidacs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT de la tabla `prestamos`
--
ALTER TABLE `prestamos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `prestatarios`
--
ALTER TABLE `prestatarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `publicacions`
--
ALTER TABLE `publicacions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reinos`
--
ALTER TABLE `reinos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `riesgos`
--
ALTER TABLE `riesgos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `rols`
--
ALTER TABLE `rols`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `secuencias`
--
ALTER TABLE `secuencias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `se_realizans`
--
ALTER TABLE `se_realizans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `taxonomias`
--
ALTER TABLE `taxonomias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipoinvestigacions`
--
ALTER TABLE `tipoinvestigacions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `zonas`
--
ALTER TABLE `zonas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `aprobacions`
--
ALTER TABLE `aprobacions`
  ADD CONSTRAINT `FK_APROBACION_PERTENECE_A_UNA_INVESTIGACION` FOREIGN KEY (`idInvestigacion`) REFERENCES `investigacions` (`id`);

--
-- Filtros para la tabla `clases`
--
ALTER TABLE `clases`
  ADD CONSTRAINT `FK_CLASE_POSEE_UN_FILUM` FOREIGN KEY (`idFilum`) REFERENCES `filums` (`id`);

--
-- Filtros para la tabla `detalleusuarios`
--
ALTER TABLE `detalleusuarios`
  ADD CONSTRAINT `FK_DETALLEU_OBTIENE_ROL` FOREIGN KEY (`idRol`) REFERENCES `rols` (`id`),
  ADD CONSTRAINT `FK_DETALLEU_POSEE2_USUARIO` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`id`);

--
-- Filtros para la tabla `dominios`
--
ALTER TABLE `dominios`
  ADD CONSTRAINT `FK_DOMINIO_PERTENECE_REINO` FOREIGN KEY (`idReino`) REFERENCES `reinos` (`id`);

--
-- Filtros para la tabla `especieamenazadas`
--
ALTER TABLE `especieamenazadas`
  ADD CONSTRAINT `FK_ESPECIEA_TIENE_UNA_RIESGO` FOREIGN KEY (`idRiesgo`) REFERENCES `riesgos` (`id`);

--
-- Filtros para la tabla `especies`
--
ALTER TABLE `especies`
  ADD CONSTRAINT `FK_ESPECIE_POSEE_UNA_TAXONOMI` FOREIGN KEY (`idTaxonomia`) REFERENCES `taxonomias` (`id`),
  ADD CONSTRAINT `FK_ESPECIE_PUEDE_SER_ESPECIEA` FOREIGN KEY (`idEspamen`) REFERENCES `especieamenazadas` (`id`),
  ADD CONSTRAINT `FK_ESPECIE_TIENE_____GENERO` FOREIGN KEY (`idGenero`) REFERENCES `generos` (`id`);

--
-- Filtros para la tabla `especimens`
--
ALTER TABLE `especimens`
  ADD CONSTRAINT `FK_ESPECIMEN_PERTENECE_INVESTIGACION` FOREIGN KEY (`idInvestigacion`) REFERENCES `investigacions` (`id`),
  ADD CONSTRAINT `FK_ESPECIME_POSEE_UNA_TAXONOMI` FOREIGN KEY (`idTaxonomia`) REFERENCES `taxonomias` (`id`),
  ADD CONSTRAINT `FK_SECUENCI_TIENE_____SECUENCIA` FOREIGN KEY (`idSecuencia`) REFERENCES `secuencias` (`id`);

--
-- Filtros para la tabla `familias`
--
ALTER TABLE `familias`
  ADD CONSTRAINT `FK_FAMILIA_TIENE_UNA_ORDEN` FOREIGN KEY (`idOrden`) REFERENCES `ordens` (`id`);

--
-- Filtros para la tabla `filums`
--
ALTER TABLE `filums`
  ADD CONSTRAINT `FK_FILUM_TIENE_____REINO` FOREIGN KEY (`idReino`) REFERENCES `reinos` (`id`);

--
-- Filtros para la tabla `generos`
--
ALTER TABLE `generos`
  ADD CONSTRAINT `FK_GENERO_PEERTENEC_FAMILIA` FOREIGN KEY (`idFamilia`) REFERENCES `familias` (`id`);

--
-- Filtros para la tabla `investigacions`
--
ALTER TABLE `investigacions`
  ADD CONSTRAINT `FK_INVESTIG_PERTENECE_ZONA` FOREIGN KEY (`idZona`) REFERENCES `zonas` (`id`),
  ADD CONSTRAINT `FK_INVESTIG_RELATIONS_USUARIO` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `FK_INVESTIG_TIENE_UN_TIPOINVE` FOREIGN KEY (`idTipo`) REFERENCES `tipoinvestigacions` (`id`);

--
-- Filtros para la tabla `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD CONSTRAINT `FK_MUNICIPI_COMPUESTO_DEPARTAM` FOREIGN KEY (`idDepto`) REFERENCES `departamentos` (`id`);

--
-- Filtros para la tabla `obtenido_ens`
--
ALTER TABLE `obtenido_ens`
  ADD CONSTRAINT `FK_OBTENIDO_OBTENIDO__ESPECIME` FOREIGN KEY (`id`) REFERENCES `especimens` (`id`),
  ADD CONSTRAINT `FK_OBTENIDO_OBTENIDO__INVESTIG` FOREIGN KEY (`idInv`) REFERENCES `investigacions` (`id`);

--
-- Filtros para la tabla `ordens`
--
ALTER TABLE `ordens`
  ADD CONSTRAINT `FK_ORDEN_RELATIONS_CLASE` FOREIGN KEY (`idClase`) REFERENCES `clases` (`id`);

--
-- Filtros para la tabla `reinos`
--
ALTER TABLE `reinos`
  ADD CONSTRAINT `FK_REINO_PERTENECE_DOMINIO` FOREIGN KEY (`idDominio`) REFERENCES `dominios` (`id`);

--
-- Filtros para la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `rols`
--
ALTER TABLE `rols`
  ADD CONSTRAINT `FK_ROL_OBTIENE2_DETALLEU` FOREIGN KEY (`idDetalleUsuario`) REFERENCES `detalleusuarios` (`id`);

--
-- Filtros para la tabla `se_realizans`
--
ALTER TABLE `se_realizans`
  ADD CONSTRAINT `FK_SE_REALI_SE_REALIZ_INVESTIG` FOREIGN KEY (`idInv`) REFERENCES `investigacions` (`id`),
  ADD CONSTRAINT `FK_SE_REALI_SE_REALIZ_MUNICIPI` FOREIGN KEY (`id`) REFERENCES `municipios` (`id`);

--
-- Filtros para la tabla `taxonomias`
--
ALTER TABLE `taxonomias`
  ADD CONSTRAINT `FK_TAXONOMI_POSEE_UNA_ESPECIE` FOREIGN KEY (`idEspecie`) REFERENCES `especies` (`id`),
  ADD CONSTRAINT `FK_TAXONOMI_POSEE_UNA_ESPECIME` FOREIGN KEY (`idEspecimen`) REFERENCES `especimens` (`id`),
  ADD CONSTRAINT `FK_TAXONOMI_SE_CONFOR_COLECCIO` FOREIGN KEY (`idColeccion`) REFERENCES `coleccions` (`id`);

--
-- Filtros para la tabla `tipoinvestigacions`
--
ALTER TABLE `tipoinvestigacions`
  ADD CONSTRAINT `FK_TIPOINVE_TIENE_UN2_INVESTIG` FOREIGN KEY (`idInv`) REFERENCES `investigacions` (`id`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `FK_USUARIO_POSEE_DETALLEU` FOREIGN KEY (`idDetalleUsuario`) REFERENCES `detalleusuarios` (`id`);

--
-- Filtros para la tabla `zonas`
--
ALTER TABLE `zonas`
  ADD CONSTRAINT `FK_ZONA_PERTENECE_A_UN_DEPARTAMENTO` FOREIGN KEY (`idDepto`) REFERENCES `departamentos` (`id`),
  ADD CONSTRAINT `FK_ZONA_PERTENECE_A_UN_MUNICIPIO` FOREIGN KEY (`idMunicipio`) REFERENCES `municipios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
