<?php



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('home');
});


 Route::get('map', 'MapController@index')->name('map');
 Route::get('consulta', 'MapController@consulta')->name('consulta');
 Route::get('bitacoras', 'MapController@bitacoras')->name('bitacoras');
 Route::get('bold/{taxon}/{geo}', 'MapController@bold')->name('bold');

 Route::get('bold/{taxon}/{geo}', [
      'uses'=> 'MapController@bold',
      'as'  => 'bold'
  ]);
 Route::get('gestion', function()
  {
   return view('gestion');
  });
  Route::get('quienes_somos', function()
  {
   return view('somos');
  });

Auth::routes();

Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');

Route::get('google', function () {
    return view('googleAuth');
});
    
Route::get('auth/google', 'Auth\LoginController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\LoginController@handleGoogleCallback');

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles','RoleController');
    Route::resource('users','UserController');
    Route::resource('departamento','DepartamentoController');
    Route::resource('municipio','MunicipioController');
    Route::resource('coleccion','ColeccionController');
    Route::resource('prestatario','PrestatarioController');
    Route::resource('publicacion','PublicacionController');
    Route::resource('aprobacion','AprobacionController');
    Route::resource('zona','ZonaController');
    Route::resource('secuencia','SecuenciaController');
     Route::resource('coleccion','ColeccionController');
 Route::resource('zona','ZonaController');
 Route::resource('dominio','DominioController');
 
 Route::resource('pago','PagoController');
 Route::resource('partidac','PartidacController');
 Route::get('gestionp','PartidacController@gestionp')->name('gestionCierres');
 Route::get('partidas/{id}','PartidacController@partidas')->name('partidasCierres');
 Route::get('libroMayor/{id}','PartidacController@libroMayor')->name('libroMayor');
 Route::get('detallepartida/{id}/{status}','PartidacController@detallepartida')->name('detallePartidas');
 Route::get('balance/{id}','PartidacController@balance')->name('balanceCierres');
 Route::get('balancec','PartidacController@estadoc')->name('reporteBalance');
 Route::get('balancecr','PartidacController@estadocr')->name('reporteBalancer');
 Route::resource('prestamo','PrestamoController');
 Route::resource('prestatario','PrestatarioController');
 Route::resource('contribuyente','ContribuyenteController');
 Route::resource('catcuenta','CatcuentaController');
 Route::resource('contcuenta','ContCuentaController');
 Route::resource('libcompras','LibcomprasController');
 Route::resource('libventascs','LibventascsController');
 Route::resource('libventas','LibventasController');
 Route::resource('contelemento','contElementoController');
 Route::resource('contrubro','contRubroController');
 
 Route::resource('secuencia','SecuenciaController');
 Route::resource('riesgo','RiesgoController');
 Route::resource('tipoInvestigacion','TipoInvestigacionController');
 Route::resource('reino','ReinoController');
 Route::resource('filum','FilumController');
 Route::resource('clase','ClaseController');
 Route::resource('orden','OrdenController');
 Route::resource('especieAmenazada','EspecieAmenazadaController');
 Route::resource('municipio','MunicipioController');
 Route::resource('familia','FamiliaController');
  Route::resource('genero','GeneroController');
  Route::resource('especie','EspecieController');
  Route::resource('especimen','EspecimenController');
  Route::resource('taxonomia','TaxonomiaController');
Route::resource('investigacion','InvestigacionController');

Route::get('facturacion','PagoController@facturacion')->name('pago.facturacion');
Route::get('facturacionccf','PagoController@facturacionccf')->name('pago.facturacionccf');
Route::get('facturacionfe','PagoController@facturacionfe')->name('pago.facturacionfe');
Route::get('facturacionse','PagoController@facturacionse')->name('pago.facturacionse');
Route::get('facturacionnc','PagoController@facturacionnc')->name('pago.facturacionnc');

Route::get('index2','PrestamoController@index2')->name('prestamo.index2');
Route::get('create2','ContCuentaController@create2')->name('contcuenta.create2');
Route::get('create3','ContCuentaController@create3')->name('contcuenta.create3');
Route::get('create4','ContCuentaController@create4')->name('contcuenta.create4');
Route::get('create5','ContCuentaController@create5')->name('contcuenta.create5');
Route::post('store2','ContCuentaController@store2')->name('contcuenta.store2');
Route::post('store3','ContCuentaController@store3')->name('contcuenta.store3');
Route::post('store4','ContCuentaController@store4')->name('contcuenta.store4');
Route::post('store5','ContCuentaController@store5')->name('contcuenta.store5');
Route::get('edit2/{id}','ContCuentaController@edit2')->name('contcuenta.edit2');
Route::get('edit3/{id}','ContCuentaController@edit3')->name('contcuenta.edit3');
Route::get('edit4/{id}','ContCuentaController@edit4')->name('contcuenta.edit4');
Route::get('edit5/{id}','ContCuentaController@edit5')->name('contcuenta.edit5');
Route::post('update2','ContCuentaController@update2')->name('contcuenta.update2');
Route::post('update3','ContCuentaController@update3')->name('contcuenta.update3');
Route::post('update4','ContCuentaController@update4')->name('contcuenta.update4');
Route::post('update5','ContCuentaController@update5')->name('contcuenta.update5');
Route::delete('destroy2/{id}','ContCuentaController@destroy2')->name('contcuenta.destroy2');
Route::delete('destroy3/{id}','ContCuentaController@destroy3')->name('contcuenta.destroy3');
Route::delete('destroy4/{id}','ContCuentaController@destroy4')->name('contcuenta.destroy4');
Route::delete('destroy5/{id}','ContCuentaController@destroy5')->name('contcuenta.destroy5');

Route::get('/get-prestatarios', 'DropdownController@getPrestatarios');
Route::get('/get-prestamos/{prestatario}', 'DropdownController@getPrestamos');


//RUTAS PARA REPORTERIA
Route::get('dominioPrueba.pdf', 'DominioController@generatePDF');

Route::get('partidacPrueba.pdf', 'PartidacController@generatePDF')->name('reportePartidac');
Route::get('partidacPrueba.pdf2', 'PartidacController@generatePDF2')->name('reportePartidacr');
Route::get('partidacPrueba.pdf3/{id}', 'PartidacController@generatePDF3')->name('reportePartidac3');
Route::get('partidacPrueba.pdf4/{id}/{status}', 'PartidacController@generatePDF4')->name('reportePartidac4');
Route::get('libroMayor.pdf5/{id}', 'PartidacController@generatePDF5')->name('libroMayorpdf');
Route::get('partidac.pdf6/{fechai}/{fechaf}/{cuenta}', 'PartidacController@generatePDF6')->name('partidashpdf');
Route::get('/pagoPrueba.pdf/{idprest}/{fechadepago}', 'PagoController@generatePDF')->name('reportePago');
Route::get('/pagoPrueba2.pdf/{idprest}/{fechadepago}', 'PagoController@generatePDF2')->name('reportePago2');
Route::get('/pagoPrueba3.pdf/{idprest}/{fechadepago}', 'PagoController@generatePDF3')->name('reportePago3');
Route::get('/pagoPrueba4.pdf/{idprest}/{fechadepago}', 'PagoController@generatePDF4')->name('reportePago4');
Route::get('/pagoPrueba5.pdf/{idprest}/{fechadepago}', 'PagoController@generatePDF5')->name('reportePago5');
Route::get('/jsonpago/{idprest}/{fechadepago}', 'PagoController@jsonp')->name('jsonpago');
Route::get('contribuyentePrueba.pdf', 'ContribuyenteController@generatePDF');
Route::get('catcuentaPrueba.pdf', 'CatcuentaController@generatePDF');
Route::get('contcuentaPrueba.pdf', 'ContCuentaController@generatePDF');
Route::get('libcomprasPrueba.pdf', 'LibcomprasController@generatePDF');
Route::get('/prestamoPrueba.pdf/{id}', 'PrestamoController@generatePDF')->name('reportePre');
Route::get('/prestamoPrueba2.pdf/{id}', 'PrestamoController@generatePDF2')->name('reportePre2');

Route::get('/exportPrestamo/{id}', 'PrestamoController@exportExcel')->name('excelPrestamo');
Route::get('/exportLibcompras/{ccf}', 'LibcomprasController@exportExcel')->name('excelLibcompras');
Route::get('/exportLibventascs/{ccf}', 'LibventascsController@exportExcel')->name('excelLibventascs');
Route::get('/detalle/{periodo}', 'LibventasController@detalle')->name('detalleVentas');
Route::get('/exportLibrocuenta/{fechai}/{fechaf}/{cuenta}', 'PartidacController@exportExcelp')->name('excelLibroCuenta');
Route::get('exportPartidac', 'PartidacController@exportExcel')->name('excelPartidac');
Route::get('exportPartidac2', 'PartidacController@exportExcel2')->name('excelPartidacr');
Route::get('cierre', 'PartidacController@cierre')->name('cierre');
Route::get('procesoCierre', 'PartidacController@procesoCierre')->name('procesoCierre');
Route::get('reproceso', 'PartidacController@Reproceso')->name('reproceso');
Route::get('vreproceso', 'PartidacController@Vreproceso')->name('vreproceso');
Route::get('partidash', 'PartidacController@Partidash')->name('historialdePartidas');

Route::get('reportes', 'ReporteController@Index')->name('reportes.index');
Route::get('reportePrestamo.pdf', 'ReporteController@generatePDF')->name('reportePrestamop');
Route::get('exportPrestamo', 'ReporteController@exportExcel')->name('reportePrestamoe');

Route::get('especieAmenazada.pdf', 'EspecieAmenazadaController@generatePDF');
Route::get('reporteTaxonomia.pdf', 'TaxonomiaController@generatePDF');
Route::get('/reportePDF.pdf/{id}', 'EspecimenController@generatePDF');
Route::get('/reporteEspecie/{idReino}', 'EspecieController@reportePDF')->name('reporte2');
Route::get('index_filter', 'EspecieController@index');
//Route::get('reporte2.pdf', 'EspecieController@reportePDF');


//Rutas para Facebook y google



});



