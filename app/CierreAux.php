<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class CierreAux extends Model
{
    use LogsActivity;

    protected $fillable = ['periodo', 'anio', 'estatus', 'estatus2', 'fechaCierre', 'fechaUltCierre'];
    protected $dates = ['created_at','updated_at'];

    public function scopeNombre($query, $nombre)
    {
        return $query->where('rubroDesc', 'LIKE', "%$nombre%");
    }
 }