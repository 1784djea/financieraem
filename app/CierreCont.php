<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class CierreCont extends Model
{
    use LogsActivity;

    protected $fillable = ['idaux', 'cuenta', 'tipo', 'rubroDesc', 'estatus', 'estatus2', 'saldoInicial', 'debe', 'haber', 'saldo', 'periodo', 'fechaCierre', 'fechaUltCierre'];
    protected $dates = ['created_at','updated_at'];

    public function scopeNombre($query, $nombre)
    {
        return $query->where('rubroDesc', 'LIKE', "%$nombre%");
    }
 }