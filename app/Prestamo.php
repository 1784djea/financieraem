<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Prestamo extends Model
{

  use LogsActivity;
  
    protected $fillable = ['idprest', 'codigoPrestamo', 'periodicidad', 'tasa', 'tiempo', 'cantidad', 'descrip', 'pago', 'fechadePago', 'pendpago', 'fechapago', 'intereses'];
    protected $dates = ['created_at','updated_at'];

    public function scopeNombre($query, $nombre)
	{
		return $query
                 ->join('prestatarios', 'prestamos.idprest', '=', 'prestatarios.id')
                 ->select('prestamos.*', 'prestatarios.pnombre', 'prestatarios.snombre', 'prestatarios.papellido', 'prestatarios.sapellido')
                 ->where('prestatarios.pnombre', 'LIKE', "%$nombre%")
                 ->orWhere('prestatarios.snombre', 'LIKE', "%$nombre%")
                 ->orWhere('prestatarios.papellido', 'LIKE', "%$nombre%")
                 ->orWhere('prestatarios.dui', 'LIKE', "%$nombre%")
                 ->orWhere('prestamos.notas3', 'LIKE', "%$nombre%");
	}
}