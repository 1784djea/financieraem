<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Catcuenta;
use App\Contcuenta;
use App\ContSubcuenta;
use App\ContCuentaDetalle;
use App\ContElemento;
use App\ContRubro;
use Contcuenta1\http\Request\ContcuentaRequest;
use RealRashid\SweetAlert\Facades\Alert;
use PDF;
use Carbon\Carbon;

class ContcuentaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:Dominios|Crear Dominio|Editar Dominio|Eliminar Dominio', ['only' => ['index','store']]);
         $this->middleware('permission:Dominios', ['only' => ['index']]);
         $this->middleware('permission:Crear Dominio', ['only' => ['create','store']]);
         $this->middleware('permission:Editar Dominio', ['only' => ['edit','update']]);
         $this->middleware('permission:Eliminar Dominio', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nombre = $request->get('rubroDesc');
        $contcuentas = Contcuenta::orderBy('cuenta','ASC')->nombre($nombre)->paginate(100);
        $contsubcuentas = ContSubcuenta::all();
        $contcuentadetalles = ContCuentaDetalle::all();
        $contelementos = ContElemento::all();
        $contrubros = ContRubro::all();

        $prueba = ContSubcuenta::join('ContCuentaDetalle', 'ContCuentaDetalle.padre', '=', 'ContSubcuenta.id')
                                ->orderBy('ContSubcuenta.id');

        return view('contcuenta.index',compact('contcuentas','contsubcuentas','contcuentadetalles','contelementos','contrubros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $contcuentas = Contcuenta::all();
        $contrubros = ContRubro::all();

        return view('contcuenta.create', compact('contcuentas', 'contrubros'));
    }

    public function create2()
    {
        $contcuentas = Contcuenta::all();
        return view('contcuenta.create2', compact('contcuentas'));
    }

    public function create3()
    {
        //$contcuentas = Contcuenta::all();
        $contsubcuentas = ContSubcuenta::all();
        return view('contcuenta.create3', compact('contsubcuentas'));
    }

    public function create4()
    {
        //$contcuentas = Contcuenta::all();
        $contelementos = ContElemento::all();
        return view('contcuenta.create4', compact('contelementos'));
    }

    public function create5()
    {
        //$contcuentas = Contcuenta::all();
        $contelementos = ContElemento::all();
        return view('contcuenta.create5', compact('contelementos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
          'cuenta', 
          'subcuenta', 
          'cuentaDetalle', 
          'rubroDesc', 
          'estatus',
          'saldoInicial',
          'debe',
          'haber',
          'saldo'
        ]);
        Contcuenta::create($request->all());
        Catcuenta::create($request->all());
        return redirect()->route('contcuenta.index');
    }

    public function store2(Request $request)
    {
       $this->validate($request,[
          'subcuenta', 
          'cuentaDetalle', 
          'rubroDesc', 
          'estatus',
          'saldoInicial',
          'debe',
          'haber',
          'saldo'
        ]);
        ContSubcuenta::create($request->all());
        Catcuenta::create($request->all());
        return redirect()->route('contcuenta.index');
    }

    public function store3(Request $request)
    {
       $this->validate($request,[
          'subcuenta', 
          'cuentaDetalle', 
          'rubroDesc', 
          'estatus',
          'saldoInicial',
          'debe',
          'haber',
          'saldo'
        ]);
        ContCuentaDetalle::create($request->all());
        Catcuenta::create($request->all());
        return redirect()->route('contcuenta.index');
    }

    public function store4(Request $request)
    {
       $this->validate($request,[
          'subcuenta', 
          'cuentaDetalle', 
          'rubroDesc', 
          'estatus',
          'saldoInicial',
          'debe',
          'haber',
          'saldo'
        ]);
        ContElemento::create($request->all());
        return redirect()->route('contcuenta.index');
    }

    public function store5(Request $request)
    {
       $this->validate($request,[
          'subcuenta', 
          'cuentaDetalle', 
          'rubroDesc', 
          'estatus',
          'saldoInicial',
          'debe',
          'haber',
          'saldo'
        ]);
        ContRubro::create($request->all());
        return redirect()->route('contcuenta.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contcuentas = Contcuenta::find($id);
      return view('contcuenta.show',compact('contcuentas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contcuentas = Contcuenta::find($id);
        return view('contcuenta.edit',compact('contcuentas'));
    }

    public function edit2($id)
    {
        $contcuentas = ContSubcuenta::find($id);
        return view('contcuenta.edit2',compact('contcuentas'));
    }

    public function edit3($id)
    {
        $contcuentas = ContCuentaDetalle::find($id);
        return view('contcuenta.edit3',compact('contcuentas'));
    }

    public function edit4($id)
    {
        $contelementos = ContElemento::find($id);
        return view('contcuenta.edit4',compact('contelementos'));
    }

    public function edit5($id)
    {
        $contrubros = ContRubro::find($id);
        return view('contcuenta.edit5',compact('contrubros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
          'cuenta', 
          'subcuenta', 
          'cuentaDetalle', 
          'rubroDesc', 
          'estatus',
          'saldoInicial',
          'debe',
          'haber',
          'saldo'
        ]);
        Contcuenta::find($id)->update($request->all());
        return redirect()->route('contcuenta.index');
    }

    public function update2(Request $request, $id)
    {
        $this->validate($request,[
          'cuenta', 
          'subcuenta', 
          'cuentaDetalle', 
          'rubroDesc', 
          'estatus',
          'saldoInicial',
          'debe',
          'haber',
          'saldo'
        ]);
        ContSubcuenta::find($id)->update($request->all());
        return redirect()->route('contcuenta.index');
    }

    public function update3(Request $request, $id)
    {
        $this->validate($request,[
          'cuenta', 
          'subcuenta', 
          'cuentaDetalle', 
          'rubroDesc', 
          'estatus',
          'saldoInicial',
          'debe',
          'haber',
          'saldo'
        ]);
        ContCuentaDetalle::find($id)->update($request->all());
        return redirect()->route('contcuenta.index');
    }

    public function update4(Request $request, $id)
    {
        $this->validate($request,[
          'cuenta', 
          'subcuenta', 
          'cuentaDetalle', 
          'rubroDesc', 
          'estatus',
          'saldoInicial',
          'debe',
          'haber',
          'saldo'
        ]);
        ContElemento::find($id)->update($request->all());
        return redirect()->route('contcuenta.index');
    }

    public function update5(Request $request, $id)
    {
        $this->validate($request,[
          'cuenta', 
          'subcuenta', 
          'cuentaDetalle', 
          'rubroDesc', 
          'estatus',
          'saldoInicial',
          'debe',
          'haber',
          'saldo'
        ]);
        ContRubro::find($id)->update($request->all());
        return redirect()->route('contcuenta.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       try{
            $cuenta_eliminar = Contcuenta::find($id);
            $catcuenta_eliminar = $cuenta_eliminar->cuenta;
            Catcuenta::where('cuenta', $catcuenta_eliminar)->first()->delete();
            Contcuenta::find($id)->delete();
            

            Alert::success('Cuenta eliminado con exito');
        return redirect()->route('contcuenta.index');
            } catch  (\Illuminate\Database\QueryException $e){
                 Alert::danger('No se Puede eliminar este registro porque esta asociado con otros datos');
        return redirect()->route('contcuenta.index');
        }
    }

    public function destroy2($id)
    {
       try{
            $cuenta_eliminar = ContSubcuenta::find($id);
            $catcuenta_eliminar = $cuenta_eliminar->subcuenta;
            Catcuenta::where('subcuenta', $catcuenta_eliminar)->first()->delete();
            ContSubcuenta::find($id)->delete();
            

            Alert::success('Subcuenta eliminado con exito');
        return redirect()->route('contcuenta.index');
            } catch  (\Illuminate\Database\QueryException $e){
                 Alert::danger('No se Puede eliminar este registro porque esta asociado con otros datos');
        return redirect()->route('contcuenta.index');
        }
    }

    public function destroy3($id)
    {
       try{
            $cuenta_eliminar = ContCuentaDetalle::find($id);
            $catcuenta_eliminar = $cuenta_eliminar->cuentaDetalle;
            Catcuenta::where('cuentaDetalle', $catcuenta_eliminar)->first()->delete();
            ContCuentaDetalle::find($id)->delete();
            

            Alert::success('Cuenta detalle eliminada con exito');
        return redirect()->route('contcuenta.index');
            } catch  (\Illuminate\Database\QueryException $e){
                 Alert::danger('No se Puede eliminar este registro porque esta asociado con otros datos');
        return redirect()->route('contcuenta.index');
        }
    }

    public function destroy4($id)
    {
       try{
            ContElemento::find($id)->delete();
            

            Alert::success('Elemento eliminada con exito');
        return redirect()->route('contcuenta.index');
            } catch  (\Illuminate\Database\QueryException $e){
                 Alert::danger('No se Puede eliminar este registro porque esta asociado con otros datos');
        return redirect()->route('contcuenta.index');
        }
    }

    public function destroy5($id)
    {
       try{
            ContRubro::find($id)->delete();
            

            Alert::success('Rubro eliminada con exito');
        return redirect()->route('contcuenta.index');
            } catch  (\Illuminate\Database\QueryException $e){
                 Alert::danger('No se Puede eliminar este registro porque esta asociado con otros datos');
        return redirect()->route('contcuenta.index');
        }
    }

    public function generatePDF(Request $request)

    {
        $nombre = $request->get('cuenta');
        $contcuentas = Contcuenta::orderBy('id','ASC')->nombre($nombre)->paginate(100);
        //$data = ['title' => 'Esta es una página de Prueba'];
        $date=new Carbon();
        $fecha = $date->format('d-m-Y');

        $pdf = PDF::loadView('contcuenta.reporteContcuenta',compact('contcuentas','fecha'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('contcuenta.pdf');

    }


}
