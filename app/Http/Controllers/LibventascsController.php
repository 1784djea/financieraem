<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contribuyentes;
use App\Libventascs;
use App\Catcuenta;
use Libventascs1\http\Request\LibventascsRequest;
use RealRashid\SweetAlert\Facades\Alert;
use PDF;
use Carbon\Carbon;
use App\Exports\LibventascsExport;
use Maatwebsite\Excel\Facades\Excel;

class LibventascsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:Dominios|Crear Dominio|Editar Dominio|Eliminar Dominio', ['only' => ['index','store']]);
         $this->middleware('permission:Dominios', ['only' => ['index']]);
         $this->middleware('permission:Crear Dominio', ['only' => ['create','store']]);
         $this->middleware('permission:Editar Dominio', ['only' => ['edit','update']]);
         $this->middleware('permission:Eliminar Dominio', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nombre =$request->get('nombre');
        //$libventasc = Partidac::orderBy('correlativo','desc')->groupBy('correlativo')->having('correlativo', '>', 0)->get();
        //$partidac = Partidac::select('correlativo')
        //                ->distinct()
        //                ->get();
        $libventasc = Libventascs::orderBy('id','ASC')->nombre($nombre)->get();
        $libventasc2 = Libventascs::select('correlativo')->distinct()->paginate(10);
        //$partidac = Partidac::orderBy('id','ASC')->nombre($nombre)->get();
        $libventascs = Libventascs::all();
        return view('libventascs.index',compact('libventasc', 'libventascs', 'libventasc2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         //$personas = TableCliente::all();
        //$productos = TableProductos::all();
        //$factura = TableFacturas::all();
        //$venta = TableVentas::all();
        $catcuentas = Catcuenta::all();
        $contribuyentes = Contribuyentes::all();
        $libventasc = Libventascs::select('correlativo')->orderBy('id', 'desc')->first();
        return view('libventascs.create',compact('contribuyentes', 'libventasc'));
    }

    public function estadoc()
    {
        //$personas = TableCliente::all();
        //$productos = TableProductos::all();
        //$factura = TableFacturas::all();
        //$venta = TableVentas::all();
        $catcuentas = Catcuenta::all();
        $libventasc = Libventascs::all();
        $libventascs = Libventascs::all()->unique('correlativo');
        
        return view('libventascs.estadoc',compact('catcuentas', 'libventasc', 'libventascs'));
    }

    public function estadoco()
    {
        //$personas = TableCliente::all();
        //$productos = TableProductos::all();
        //$factura = TableFacturas::all();
        //$venta = TableVentas::all();
        $catcuentas = Catcuenta::all();
        $libventasc = Libventascs::all();
        $libventascs = Libventascs::all()->unique('correlativo');
        
        return view('libventascs.estadoco',compact('catcuentas', 'libventasc', 'libventascs'));
    }

    public function reporte(Request $request)
    {
        //$personas = TableCliente::all();
        //$productos = TableProductos::all();
        //$factura = TableFacturas::all();
        //$venta = TableVentas::all();
        $mes = $request->get('mes');
        $año = $request->get('año');
        $libventascs2 = DB::table('libventascs')
        ->select('*')
        ->where('mes', $mes)
        ->orWhere('año', $año)
        ->get();
        $contribuyentes = Contribuyentes::all();
        $catcuentas = Catcuenta::all();
        $libventasc = Libventascs::all();
        //$libventascs2 = DB::select('select * from Libventascs where mes = :mes1',  $mes1);;
        $libventascs = Libventascs::all()->unique('correlativo');
        
        return view('libventascs.reporte',compact('catcuentas', 'libventasc', 'libventascs', 'libventascs2', 'contribuyentes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       /**$this->validate($request,[
            'No_Facturas',
            'Productos',
            'cantidad'
            factura= cliente, fecha, total
            Cliente= id
        ]);
        
        TableVentas::create($request->all());
        return redirect()->route('tableVentas.create')->with('success','Venta guardado con éxito');*/
        $total = 0;
        $estatus = 1;
        $totalf = 0;
        $cant = 0;
        $cont = 0;
        $tipoDoc = $request->get('tipo2');
        $correlativo = $request->get('correlativo');
        $tipo2 = $request->get('tipo2');
        $ccf = $request->get('ccf');
        $fechaccf = $request->get('fechaccf');
        $fecha = $request->get('año');
        $montoGravado = $request->get('montot');
        $montoGravado2 = $request->get('montot2');
        $montoGravado3 = $request->get('montot3');
        $idContribuyente = $request->get('idContribuyente');
        $siva2 = $request->get('siva');
        $sriva2 = $request->get('sriva');
        $sneto2 = $request->get('sneto2');
        $srenta = $request->get('srenta');
        while ($cont < count($montoGravado)) {
        $año= substr($fecha, 0, 4);
        $mes= substr($fecha, 5, 2);
        $libventasc = new Libventascs;
        $libventasc->correlativo = $correlativo; 
        $libventasc->tipoDoc = $tipoDoc[$cont]; 
        $libventasc->tipo2 = $tipo2[$cont]; 
        $libventasc->ccf = $ccf[$cont]; 
        $libventasc->fechaccf = $fechaccf[$cont]; 
        $libventasc->año = $año; 
        $libventasc->mes = $mes; 
        $libventasc->idContribuyente = $idContribuyente[$cont]; 
        $libventasc->iva = $siva2[$cont]; 
        $libventasc->riva = $sriva2[$cont]; 
        $libventasc->renta = $srenta[$cont]; 
        $libventasc->montoNoSujeto = $montoGravado2[$cont]; 
        if ($tipoDoc=="Sujeto Excl.") {
            $libventasc->montoGravado = $montoGravado3[$cont]; 
            $libventasc->sneto = $montoGravado[$cont]; 
        }else{
            $libventasc->montoGravado = $montoGravado[$cont]; 
        $libventasc->sneto = $sneto2[$cont]; 
        }
        $libventasc->estatus = '1';
        $libventasc->save();


        $cont = $cont + 1;
        }


        /**$Productos = $request->get('Productos');
        $cantidad = $request->get('cantidad');

        $total = 0;
        $totalf = 0;
        $cant = 0;
        $cont = 0;
        while ($cont < count($Productos)) {
            $venta = new TableVentas;
            $venta->id_facturas = $request->get('id_facturas');
            $venta->id_productos = $Productos[$cont];
            $venta->cantidad = $cantidad[$cont];
            $venta->notaEnvio = $request->get('notaEnvio');
            $venta->save();

            $pro = TableProductos::find($Productos[$cont]);
            $total = ($pro->preciosProductos*$cantidad[$cont]);
            $totalf = ($totalf + $total);

            $cant = ($pro->cantidadProductos - $cantidad[$cont]);
            $pro->cantidadProductos = $cant;
            $pro->save();

            $cont = $cont + 1;
        }

        //$factura->totals = $totalf;*/
        
        return redirect()->route('libventascs.index')->with('success','Registro guardado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $libventasc = Libventascs::find($id);
      return view('libventascs.show',compact('libventasc'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $libventasc = Libventascs::find($id);
        return view('libventascs.edit',compact('libventasc'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
          'id_facturas',
            'id_productos',
            'cantidad'
        ]);
        Libventascs::find($id)->update($request->all());
        return redirect()->route('libventascs.index')->with('success','Partida actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       try{
            Libventascs::find($id)->delete();
            Alert::success('compras eliminada con exito');
        return redirect()->route('libventascs.index');
            } catch  (\Illuminate\Database\QueryException $e){
                 Alert::danger('No se Puede eliminar este registro porque esta asociado con otros datos');
        return redirect()->route('libventascs.index');
        }
    }

    public function generatePDF(Request $request)

    {
        $nombre = $request->get('tipoDoc');
        //$libventascs2 = libventascs::orderBy('id','DESC')->nombre($nombre)->paginate(10);
        $libventascs2 = Libventascs::all();
        $contribuyentes = Contribuyentes::all();
        $catcuentas = Catcuenta::all();
        
        $date=new Carbon();
        $fecha = $date->format('d-m-Y');

        $pdf = PDF::loadView('libventascs.reportelibventascs',compact('libventascs2','fecha','contribuyentes','catcuentas'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('libventascs.pdf');

    }


    public function exportExcel($ccf)

    {
        
        $libventasc2 = Libventascs::where('correlativo', $ccf)
                                    ->get();

        $export = new LibventascsExport($libventasc2);

        return Excel::download($export, 'Ventas.xlsx');
        //return view('export.prestamo',compact('prestamo', 'pagos'));

    }


}
