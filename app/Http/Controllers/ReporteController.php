<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partidac;
use App\Partidacierre;
use App\Catcuenta;
use App\Prestatario;
use App\Contcuenta;
use App\ContSubcuenta;
use App\ContCuentaDetalle;
use App\ContElemento;
use App\ContRubro;
use App\CierreCont;
use App\CierreAux;
use App\Pago;
use App\Prestamo;
use Partidac1\http\Request\PartidacRequest;
use RealRashid\SweetAlert\Facades\Alert;
use PDF;
use Carbon\Carbon;
use App\Exports\prestamoExport;
use App\Exports\partidacExport;
use App\Exports\partidacrExport;
use App\Exports\librocuentaExport;
use Maatwebsite\Excel\Facades\Excel;

class ReporteController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:Dominios|Crear Dominio|Editar Dominio|Eliminar Dominio', ['only' => ['index','store']]);
         $this->middleware('permission:Dominios', ['only' => ['index']]);
         $this->middleware('permission:Crear Dominio', ['only' => ['create','store']]);
         $this->middleware('permission:Editar Dominio', ['only' => ['edit','update']]);
         $this->middleware('permission:Eliminar Dominio', ['only' => ['destroy']]);
    }

    public function Index(Request $request)
    {
        $nombre =$request->get('nombre');
        //$partidac = Partidac::orderBy('correlativo','desc')->groupBy('correlativo')->having('correlativo', '>', 0)->get();
        //$partidac2 = Partidac::select('correlativo')->distinct()->paginate(10);
        //$partidac = Partidac::orderBy('id','ASC')->nombre($nombre)->get();
        //$partidacs = Partidac::all();
        $prestamos = Prestamo::all();
        $pagos = Pago::all();
        return view('reportes.index',compact('prestamos', 'pagos'));
    }

    public function generatePDF(Request $request)

    {
        $prestamos = Prestamo::all();
        $pagos = Pago::all();
        $prestatarios = Prestatario::all();
        
        $date=new Carbon();
        $fecha = $date->format('d-m-Y');

        $pdf = PDF::loadView('reportes.reportePrestamopdf',compact('prestamos', 'pagos', 'prestatarios', 'fecha'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('partidac.pdf');

    }

    public function exportExcel()

    {
        
        $fecha=date("dmy");

        return Excel::download(new prestamoExport, 'Reporte Prestamos'.$fecha.'.xlsx');
    }


    public function gestionp(Request $request)
    {
        $nombre =$request->get('nombre');
        //$partidac = Partidac::orderBy('correlativo','desc')->groupBy('correlativo')->having('correlativo', '>', 0)->get();
        $partidac2 = Partidacierre::select('estatus2')->distinct()->get();
        $partidac = Partidac::orderBy('id','ASC')->nombre($nombre)->paginate(10);
        $partidacs = Partidac::all();
        $cierreconts = CierreCont::all();
        $cierreauxs = CierreAux::orderBy('id','DESC')->paginate(10);
        return view('partidac.gestionp',compact('cierreauxs', 'partidac2'));
    }

    public function partidas($id)
    {
        $partidac2 = Partidacierre::select('correlativo')
                                    ->distinct()
                                    ->where('estatus2', $id)
                                    ->get();
        $partidac = Partidacierre::where('estatus2', $id)
                              ->get();
      return view('partidac.partidas_old',compact('partidac', 'partidac2'));
    }

    public function balance($id)
    {
        
        $partidac = CierreCont::where('idaux', $id)
                              ->get();
      return view('partidac.cierres_old',compact('partidac'));
    }

    public function detallepartida($id, $status)
    {
        $first = ContCuentaDetalle::select('cuentaDetalle', 'rubroDesc');
        $contcuentasd = ContSubcuenta::select('subcuenta', 'rubroDesc')
                                       ->whereNull('hijo')
                                       ->union($first)
                                       ->get();


        $contcuentadetalles = ContCuentaDetalle::all();
        $partidac2 = Partidacierre::where('estatus2', $status)
                                    ->where('correlativo', $id)
                                    ->get();
        $partidac = Partidacierre::where('correlativo', $id)
                              ->get();
      return view('partidac.detalle_partida_old',compact('partidac', 'contcuentadetalles', 'contcuentasd', 'partidac2'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $catcuentas = Catcuenta::all();
        //$contcuentasd = ContCuentaDetalle::all();
        $first = ContCuentaDetalle::select('cuentaDetalle', 'rubroDesc');
        $contcuentasd = ContSubcuenta::select('subcuenta', 'rubroDesc')
                                       ->whereNull('hijo')
                                       ->union($first)
                                       ->get();
        $partidacs = Partidac::select('correlativo')->orderBy('id', 'desc')->first();
        return view('partidac.create',compact('contcuentasd', 'partidacs'));
    }

    public function estadoc()
    {
        //$personas = TableCliente::all();
        //$productos = TableProductos::all();
        //$factura = TableFacturas::all();
        //$venta = TableVentas::all();
        $catcuentas = Catcuenta::all();
        $partidac = Partidac::all();
        $contelementos = ContElemento::all();
        $contrubros = ContRubro::all();
        $contcuentas = ContCuenta::all();
        $contsubcuentas = ContSubcuenta::all();
        $contcuentadetalles = ContCuentaDetalle::all();
        
        return view('partidac.estadoc',compact('catcuentas', 'partidac', 'contelementos', 'contrubros', 'contcuentas', 'contsubcuentas', 'contcuentadetalles'));
    }

    public function estadocr()
    {
        //$personas = TableCliente::all();
        //$productos = TableProductos::all();
        //$factura = TableFacturas::all();
        //$venta = TableVentas::all();
        $catcuentas = Catcuenta::all();
        $partidac = Partidac::all();
        $contelementos = ContElemento::all();
        $contrubros = ContRubro::all();
        $contcuentas = ContCuenta::all();
        $contsubcuentas = ContSubcuenta::all();
        $contcuentadetalles = ContCuentaDetalle::all();
        
        return view('partidac.estadocr',compact('catcuentas', 'partidac', 'contelementos', 'contrubros', 'contcuentas', 'contsubcuentas', 'contcuentadetalles'));
    }

    public function estadoco()
    {
        //$personas = TableCliente::all();
        //$productos = TableProductos::all();
        //$factura = TableFacturas::all();
        //$venta = TableVentas::all();
        $catcuentas = Catcuenta::all();
        $partidac = Partidac::all();
        $partidacs = Partidac::all()->unique('correlativo');
        
        return view('partidac.estadoco',compact('catcuentas', 'partidac', 'partidacs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $first = ContCuentaDetalle::select('cuentaDetalle', 'rubroDesc');
        $contcuentasd = ContSubcuenta::select('subcuenta', 'rubroDesc')
                                       ->whereNull('hijo')
                                       ->union($first)
                                       ->get();


        $contcuentadetalles = ContCuentaDetalle::all();
        $partidac = Partidac::where('correlativo', $id)
                              ->get();
      return view('partidac.show',compact('partidac', 'contcuentadetalles', 'contcuentasd'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partidac2 = Partidac::find($id);
        $partidac = Partidac::where('correlativo', $id)
                                    ->get();
        return view('partidac.edit',compact('partidac'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
          'id_facturas',
            'id_productos',
            'cantidad'
        ]);
        Partidac::find($id)->update($request->all());
        return redirect()->route('partidac.index')->with('success','Partida actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    

    public function generatePDF2(Request $request)

    {
        //$nombre = $request->get('cuenta');
        //$catcuentas = Catcuenta::orderBy('id','ASC')->nombre($nombre)->paginate(100);
        //$data = ['title' => 'Esta es una página de Prueba'];
        $catcuentas = Catcuenta::all();
        $partidac = Partidac::all();
        $contelementos = ContElemento::all();
        $contrubros = ContRubro::all();
        $contcuentas = ContCuenta::all();
        $contsubcuentas = ContSubcuenta::all();
        $contcuentadetalles = ContCuentaDetalle::all();
        
        //return view('partidac.estadocr',compact('catcuentas', 'partidac', 'contelementos', 'contrubros', 'contcuentas', 'contsubcuentas', 'contcuentadetalles'));

        //$catcuentas = Catcuenta::all();
        //$partidac = Partidac::all();
        //$partidacs = Partidac::all()->unique('correlativo');
        
        //$partidacs = Partidac::find($id);
        $date=new Carbon();
        $fecha = $date->format('d-m-Y');

        $pdf = PDF::loadView('partidac.reportePartidacr',compact('catcuentas', 'partidac', 'contelementos', 'contrubros', 'contcuentas', 'contsubcuentas', 'contcuentadetalles','fecha'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('partidac.pdf');
        }

        public function generatePDF3($id)

    {
        $first = ContCuentaDetalle::select('cuentaDetalle', 'rubroDesc');
        $contcuentasd = ContSubcuenta::select('subcuenta', 'rubroDesc')
                                       ->whereNull('hijo')
                                       ->union($first)
                                       ->get();
        $num = $id;

        $contcuentadetalles = ContCuentaDetalle::all();
        $partidac = Partidac::where('correlativo', $id)
                              ->get();
        $date=new Carbon();
        $fecha = $date->format('d/m/Y');

        $pdf = PDF::loadView('partidac.reportePartidacShow',compact('contcuentasd', 'contcuentadetalles', 'partidac','fecha', 'num'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('partidac.pdf');
        }

   public function generatePDF5($id)

    {
        $partidac2 = Partidacierre::select('idcatalogo')
                                    ->distinct()
                                    ->where('estatus2', $id)
                                    ->orderBy('idcatalogo','ASC')
                                    ->get();
        $partidac = Partidacierre::where('estatus2', $id)
                              ->get();
        $balance = cierrecont::where('idaux', $id)
                              ->get();
        

        $pdf = PDF::loadView('partidac.reporteLibroMayorCierre',compact('partidac', 'partidac2', 'balance'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('partidac.pdf');
        }

        public function libroMayor($id)
    {
        $partidac2 = Partidacierre::select('idcatalogo')
                                    ->distinct()
                                    ->where('estatus2', $id)
                                    ->orderBy('idcatalogo','ASC')
                                    ->get();
        $partidac = Partidacierre::where('estatus2', $id)
                              ->get();
        $balance = cierrecont::where('idaux', $id)
                              ->get();
        $num = $id;
      return view('partidac.libroMayorCierre',compact('partidac', 'partidac2', 'balance','num'));
    }

    
    public function exportExcel2()

    {
        
        $fecha=date("dmy");

        return Excel::download(new partidacrExport, 'Balance'.$fecha.'.xlsx');
    }

    public function exportExcelp($fechai, $fechaf, $cuenta)

    {
        $first = Partidac::select('*')
                            ->whereBetween('fecha', [$fechai, $fechaf])
                            ->where('idcatalogo', $cuenta);
        $partidas = Partidacierre::select('*')
                                       ->whereBetween('fecha', [$fechai, $fechaf])
                                       ->where('idcatalogo', $cuenta)
                                       ->union($first)
                                       ->get();

        $partidas2 = Partidacierre::select('*')
                                       ->where('fecha','<', $fechai)
                                       ->where('idcatalogo', $cuenta)
                                       ->get();
        
        $cuentan = $cuenta;
        //$apellidop = $prestatario->papellido;
        //$date=new Carbon();
        //$fecha = $date->format('d-m-Y');

        //$excel = Excel::loadView('prestamo.reportePrestamo',compact('prestamos','fecha','pagos'));
        //$pdf->getDomPDF()->set_option("enable_php", TRUE);
        //return $pdf->stream('prestamo.pdf');
        $export = new librocuentaExport($partidas, $partidas2);

        return Excel::download($export, 'Cuenta'.'_'.$cuentan.'.xlsx');
        //return view('export.prestamo',compact('prestamo', 'pagos'));

    }

    public function cierre()
    {

        //$personas = TableCliente::all();
        //$productos = TableProductos::all();
        //$factura = TableFacturas::all();
        //$venta = TableVentas::all();
        $catcuentas = Catcuenta::all();
        $partidac = Partidac::all();
        $contelementos = ContElemento::all();
        $contrubros = ContRubro::all();
        $contcuentas = ContCuenta::all();
        $contsubcuentas = ContSubcuenta::all();
        $contcuentadetalles = ContCuentaDetalle::all();
        
        return view('partidac.cierre',compact('catcuentas', 'partidac', 'contelementos', 'contrubros', 'contcuentas', 'contsubcuentas', 'contcuentadetalles'));

    }

     public function Partidash(Request $request) {
        $fechai = $request->get('fechainicial');
        $fechaf = $request->get('fechafinal');
        $cuenta = $request->get('cuenta');
        $first = Partidac::select('*')
                            ->whereBetween('fecha', [$fechai, $fechaf])
                            ->where('idcatalogo', $cuenta);
        $partidas = Partidacierre::select('*')
                                       ->whereBetween('fecha', [$fechai, $fechaf])
                                       ->where('idcatalogo', $cuenta)
                                       ->union($first)
                                       ->get();
        if (is_null($fechai)) {
            $fechai = '2022-01-01';
        }
        $partidas2 = Partidacierre::select('*')
                                       ->where('fecha','<', $fechai)
                                       ->where('idcatalogo', $cuenta)
                                       ->get();
      return view('partidac.partidash',compact('partidas', 'partidas2', 'fechai', 'fechaf', 'cuenta'));
    }

    public function generatePDF6($fechai, $fechaf, $cuenta)

    {
        $first = Partidac::select('*')
                            ->whereBetween('fecha', [$fechai, $fechaf])
                            ->where('idcatalogo', $cuenta);
        $partidas = Partidacierre::select('*')
                                       ->whereBetween('fecha', [$fechai, $fechaf])
                                       ->where('idcatalogo', $cuenta)
                                       ->union($first)
                                       ->get();

        $partidas2 = Partidacierre::select('*')
                                       ->where('fecha','<', $fechai)
                                       ->where('idcatalogo', $cuenta)
                                       ->get();
        

        $pdf = PDF::loadView('partidac.reportePartidash',compact('partidas', 'partidas2'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('partidac.pdf');
        }

    public function Vreproceso()
    {
        $partidacs = Partidac::all();
      return view('partidac.reproceso',compact('partidacs'));
    }



    public function Reproceso() {

        $partidas = Partidacierre::where('estatus2', 5)
                                   ->get();

        foreach ($partidas as $key => $value) {
            $partidac = new Partidac;
            $partidac->idcatalogo = $value->idcatalogo;
            $cuenta2 = $value->idcatalogo;
            $partidac->tipo = $value->tipo;
            $partidac->tipo2 = $value->tipo2;
            $partidac->correlativo = $value->correlativo;
            $partidac->fecha = $value->fecha;
            $partidac->descripcion = $value->descripcion;
            $partidac->concepto = $value->concepto;
            $partidac->saldoInicial = $value->saldoInicial;
            $partidac->debe = $value->debe;
            $debe = $value->debe;
            $partidac->haber = $value->haber;
            $haber = $value->haber;
            $partidac->saldo = $value->saldo;
            $partidac->estatus = $value->estatus;
            $partidac->save();
            

        $cuentaDetalleAfectadav = ContCuentaDetalle::whereCuentadetalle($cuenta2)->first();
        if (empty($cuentaDetalleAfectadav)) {
            $subcuentaAfectada = ContSubcuenta::whereSubcuenta($cuenta2)->first();
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $subcuentaAfectada->debe;
            $shaber = $subcuentaAfectada->haber;
            $subcuentaAfectada->debe = $sdebe + $debe;
            $subcuentaAfectada->haber = $shaber + $haber;
            $subcuentaAfectada->save();

            $subcuentaAfectadas = ContSubcuenta::whereSubcuenta($cuenta2)->first();
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $subcuentaAfectadas->saldoInicial;
            $ssaldos = $subcuentaAfectadas->saldo;
            $sdebes = $subcuentaAfectadas->debe;
            $shabers = $subcuentaAfectadas->haber;
            $concuenta = $subcuentaAfectadas->padre;
            $subcuentaAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $subcuentaAfectadas->save();
        } else {
            $sdebe = 0;
            $shaber = 0;
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            //$cuentaDetalleAfectada = ContCuentaDetalle::find($cuenta2);
            $cuentaDetalleAfectada = ContCuentaDetalle::whereCuentadetalle($cuenta2)->first();
            $sdebe = $cuentaDetalleAfectada->debe;
            $shaber = $cuentaDetalleAfectada->haber;
            $cuentaDetalleAfectada->debe = $sdebe + $debe;
            $cuentaDetalleAfectada->haber = $shaber + $haber;
            $cuentaDetalleAfectada->save();

            $cuentaDetalleAfectadaS = ContCuentaDetalle::whereCuentadetalle($cuenta2)->first();
            $ssaldois = $cuentaDetalleAfectadaS->saldoInicial;
            $ssaldos = $cuentaDetalleAfectadaS->saldo;
            $sdebes = $cuentaDetalleAfectadaS->debe;
            $shabers = $cuentaDetalleAfectadaS->haber;
            $subcuenta = $cuentaDetalleAfectadaS->padre;
            $cuentaDetalleAfectadaS->saldo = ($ssaldois + $sdebes - $shabers);
            $cuentaDetalleAfectadaS->save();

            $subcuentaAfectada = ContSubcuenta::find($subcuenta);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $subcuentaAfectada->debe;
            $shaber = $subcuentaAfectada->haber;
            $subcuentaAfectada->debe = $sdebe + $debe;
            $subcuentaAfectada->haber = $shaber + $haber;
            $subcuentaAfectada->save();

            $subcuentaAfectadas = ContSubcuenta::find($subcuenta);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $subcuentaAfectadas->saldoInicial;
            $ssaldos = $subcuentaAfectadas->saldo;
            $sdebes = $subcuentaAfectadas->debe;
            $shabers = $subcuentaAfectadas->haber;
            $concuenta = $subcuentaAfectadas->padre;
            $subcuentaAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $subcuentaAfectadas->save();
        }
        
            $cuentaAfectada = ContCuenta::find($concuenta);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $cuentaAfectada->debe;
            $shaber = $cuentaAfectada->haber;
            $cuentaAfectada->debe = $sdebe + $debe;
            $cuentaAfectada->haber = $shaber + $haber;
            $cuentaAfectada->save();

            $cuentaAfectadas = ContCuenta::find($concuenta);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $cuentaAfectadas->saldoInicial;
            $ssaldos = $cuentaAfectadas->saldo;
            $sdebes = $cuentaAfectadas->debe;
            $shabers = $cuentaAfectadas->haber;
            $rubro = $cuentaAfectadas->padre;
            $cuentaAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $cuentaAfectadas->save();

            $rubroAfectada = ContRubro::find($rubro);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $rubroAfectada->debe;
            $shaber = $rubroAfectada->haber;
            $rubroAfectada->debe = $sdebe + $debe;
            $rubroAfectada->haber = $shaber + $haber;
            $rubroAfectada->save();

            $rubroAfectadas = ContRubro::find($rubro);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $rubroAfectadas->saldoInicial;
            $ssaldos = $rubroAfectadas->saldo;
            $sdebes = $rubroAfectadas->debe;
            $shabers = $rubroAfectadas->haber;
            $elemento = $rubroAfectadas->padre;
            $rubroAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $rubroAfectadas->save();

            $elementoAfectada = ContElemento::find($elemento);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $elementoAfectada->debe;
            $shaber = $elementoAfectada->haber;
            $elementoAfectada->debe = $sdebe + $debe;
            $elementoAfectada->haber = $shaber + $haber;
            $elementoAfectada->save();

            $elementoAfectadas = ContElemento::find($elemento);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $elementoAfectadas->saldoInicial;
            $ssaldos = $elementoAfectadas->saldo;
            $sdebes = $elementoAfectadas->debe;
            $shabers = $elementoAfectadas->haber;
            $elementoAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $elementoAfectadas->save();

            $value->delete();
        }

        $partidacs = Partidac::all();
        return view('partidac.reproceso',compact('partidacs'));
    }

}


