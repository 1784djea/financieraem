<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pago;
use App\Prestamo;
use App\Prestatario;
use App\Empresa;
use App\NumerosEnLetras;
use Pago1\http\Request\PagoRequest;
use RealRashid\SweetAlert\Facades\Alert;
use PDF;
use Carbon\Carbon;

use App\Exports\prestamoExport;
use Maatwebsite\Excel\Facades\Excel;

class PagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:Dominios|Crear Dominio|Editar Dominio|Eliminar Dominio', ['only' => ['index','store']]);
         $this->middleware('permission:Dominios', ['only' => ['index']]);
         $this->middleware('permission:Crear Dominio', ['only' => ['create','store']]);
         $this->middleware('permission:Editar Dominio', ['only' => ['edit','update']]);
         $this->middleware('permission:Eliminar Dominio', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        //$pagos = Pago::all();
        $prestatarios = Prestatario::all();
        $prestamos = Prestamo::all();
        $nombre =$request->get('nombre');
        $pagos = Pago::orderBy('id','DESC')->nombre($nombre)->paginate(20);
               return view('pago.index',compact('pagos', 'prestatarios', 'prestamos'));
    }

    public function facturacion(Request $request)
    {
        //$pagos = Pago::all();
        $prestatarios = Prestatario::all();
        $prestamos = Prestamo::all();
        $nombre =$request->get('nombre');
        $pagos2 = Pago::select('idprest','fechadepago')->orderBy('id','DESC')->groupBy('idprest', 'fechadepago')->paginate(20);

        return view('pago.facturacion',compact('prestatarios', 'prestamos', 'pagos2'));
    }

    public function facturacionccf(Request $request)
    {
        //$pagos = Pago::all();
        $prestatarios = Prestatario::all();
        $prestamos = Prestamo::all();
        $nombre =$request->get('nombre');
        $pagos2 = Pago::select('idprest','fechadepago')->orderBy('id','DESC')->groupBy('idprest', 'fechadepago')->paginate(20);

        return view('pago.facturacionccf',compact('prestatarios', 'prestamos', 'pagos2'));
    }

    public function facturacionfe(Request $request)
    {
        //$pagos = Pago::all();
        $prestatarios = Prestatario::all();
        $prestamos = Prestamo::all();
        $nombre =$request->get('nombre');
        $pagos2 = Pago::select('idprest','fechadepago')->orderBy('id','DESC')->groupBy('idprest', 'fechadepago')->paginate(20);

        return view('pago.facturacionfe',compact('prestatarios', 'prestamos', 'pagos2'));
    }

    public function facturacionse(Request $request)
    {
        //$pagos = Pago::all();
        $prestatarios = Prestatario::all();
        $prestamos = Prestamo::all();
        $nombre =$request->get('nombre');
        $pagos2 = Pago::select('idprest','fechadepago')->orderBy('id','DESC')->groupBy('idprest', 'fechadepago')->paginate(20);

        return view('pago.facturacionse',compact('prestatarios', 'prestamos', 'pagos2'));
    }

    public function facturacionnc(Request $request)
    {
        //$pagos = Pago::all();
        $prestatarios = Prestatario::all();
        $prestamos = Prestamo::all();
        $nombre =$request->get('nombre');
        $pagos2 = Pago::select('idprest','fechadepago')->orderBy('id','DESC')->groupBy('idprest', 'fechadepago')->paginate(20);

        return view('pago.facturacionnc',compact('prestatarios', 'prestamos', 'pagos2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $pagos = Pago::all();
        $prestatarios = Prestatario::all();
        $prestamos = Prestamo::all();
        return view('pago.create', compact('pagos', 'prestatarios', 'prestamos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

          'idprest', 'idprest2', 'referencia', 'fechadepago', 'cantidad', 'diasintereses', 'comentario'
        ]);

        $idprestamo = $request->get('idprest2');
        $pcantidad = $request->get('cantidad');
        $dintereses = $request->get('diasIntereses');

        $prestamos = Prestamo::find($idprestamo);
        $remante = $prestamos->pendpago;
        $ptasa = $prestamos->tasa;
        $ppago = $prestamos->pago;

        
            $pintereses = (((($ptasa/100)/30)*$remante)*$dintereses);
            $cappagado = ($pcantidad-(((($ptasa/100)/30)*$remante)*$dintereses));
            $cappagado = round($cappagado, 2);
            $nuevadeuda = ($remante-$cappagado);

            if ($nuevadeuda<0.25) {
                $nuevadeuda = 0.00;
                $nota3 = "Cancelado";
            }else{
                $nuevadeuda = $nuevadeuda;
                $nota3 = "Pendiente";
            }


            $prestamos->pendpago = $nuevadeuda;
            $prestamos->notas3 = $nota3;
        
       
        

        $prestamos->save();


        $pago = new Pago;
        $pago->idprest = $request->get('idprest');
        $pago->idprest2 = $request->get('idprest2');
        $pago->referencia = $request->get('referencia');
        $pago->fechadepago = $request->get('fechadepago');
        $pago->cantidad = $request->get('cantidad');
        $pago->intereses = $pintereses;
        $pago->capitalpagado = $cappagado;
        $pago->diasIntereses = $request->get('diasIntereses');
        $pago->comentario = $request->get('comentario');
        $pago->estatus = '1';
        $pago->save();
        //Pago::create($request->all());
        Alert::success('pago agregada con éxito');
        return redirect()->route('pago.index');
    }

    public function create2()
    {
        $pagos = Pago::all();
        $prestatarios = Prestatario::all();
        $prestamos = Prestamo::all();
        $empresa = Empresa::all();
        return view('pago.create2', compact('pagos', 'prestatarios', 'prestamos', 'empresa'));
    }

    public function store2(Request $request)
    {
       $this->validate($request,[
          'subcuenta', 
          'cuentaDetalle', 
          'rubroDesc', 
          'estatus',
          'saldoInicial',
          'debe',
          'haber',
          'saldo'
        ]);
        ContSubcuenta::create($request->all());
        Catcuenta::create($request->all());
        return redirect()->route('contcuenta.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pago = Pago::find($id);
      return view('pago.show',compact('pago'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pago = Pago::find($id);
        $prestamos = Prestamo::all();
        $prestatarios = Prestatario::all();
        return view('pago.edit',compact('pago','prestatarios','prestamos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
          'idprest', 'idprest2', 'fechadepago', 'cantidad', 'intereses', 'comentario'
        ]);
        Pago::find($id)->update($request->all());
        return redirect()->route('pago.index')->with('success','pago actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $pago_eliminar = Pago::find($id);
            $capital_eliminar = $pago_eliminar->capitalpagado;
            $id_prestamo_eliminar = $pago_eliminar->idprest2;
            $prestamo_afectado = Prestamo::find($id_prestamo_eliminar);
            $capital_pendiente = $prestamo_afectado->pendpago;
            $prestamo_afectado->pendpago = ($capital_pendiente + $capital_eliminar);
            $prestamo_afectado->save();
            //Pago::find($id)->delete();
            $pago_eliminar->estatus = '0';
            $pago_eliminar->save();
            Alert::success('pago eliminada con exito');
        return redirect()->route('pago.index');
    		} catch  (\Illuminate\Database\QueryException $e){
                 Alert::danger('No se Puede eliminar este registro porque esta asociado con otra asignación');
        return redirect()->route('pago.index');
        }
    }

     public function generatePDF(Request $request, $idprest, $fechadepago)

    {
        //$nombre = $request->get('cuenta');
        //$catcuentas = Catcuenta::orderBy('id','ASC')->nombre($nombre)->paginate(100);
        //$data = ['title' => 'Esta es una página de Prueba'];
        $pagos = Pago::where('idprest', $idprest)
                        ->where('fechadepago', $fechadepago)
                        ->get();
        //$idprest = Pago::select('idprest')
        //                            ->distinct()
        //                            ->where('id', $id)
        //                            ->get();
        //$idprest2 = $idprest->idprest;
        $venta = 0;
        $ventat = 0;
        $ventati = 0;
        foreach ($pagos as $key => $value) {
            $idprest = $value->idprest;
            $venta = $value->intereses;
            $venta=round($venta, 2);
            $ventat = $venta + $ventat;
        }
        $ventati = $ventat + 0;
        $ventati = round($ventati, 2);

        $prestatario = Prestatario::where('id', $idprest)
                              ->get();
        $date=new Carbon();
        $fecha = $date->format('d-m-Y');
        $tipo = "FACTURA CONSUMIDOR FINAL";
        $tipo2 = 0;

        $numl=NumerosEnLetras::convertir($ventati,'Dolares','centavos');
        $numl = strtoupper($numl);

        $pdf = PDF::loadView('pago.reportePago',compact('pagos','fecha','prestatario','idprest', 'tipo', 'tipo2', 'numl'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('pago.pdf');

    }

    public function generatePDF2(Request $request, $idprest, $fechadepago)

    {
        //$nombre = $request->get('cuenta');
        //$catcuentas = Catcuenta::orderBy('id','ASC')->nombre($nombre)->paginate(100);
        //$data = ['title' => 'Esta es una página de Prueba'];
        $pagos = Pago::where('idprest', $idprest)
                        ->where('fechadepago', $fechadepago)
                        ->get();
        //$idprest = Pago::select('idprest')
        //                            ->distinct()
        //                            ->where('id', $id)
        //                            ->get();
        //$idprest2 = $idprest->idprest;
        $venta = 0;
        $ventat = 0;
        $ventati = 0;
        foreach ($pagos as $key => $value) {
            $idprest = $value->idprest;
            $venta = $value->intereses;
            $venta=round($venta, 2);
            $ventat = $venta + $ventat;
        }
        $ventati = $ventat + ($ventat * 0.13);
        $ventati = round($ventati, 2);

        $prestatario = Prestatario::where('id', $idprest)
                              ->get();
        $date=new Carbon();
        $fecha = $date->format('d-m-Y');
        $tipo = "COMPROBANTE DE CRÉDITO FISCAL";
        $tipo2 = 1;

        $numl=NumerosEnLetras::convertir($ventati,'Dolares','centavos');
        $numl = strtoupper($numl);

        $pdf = PDF::loadView('pago.reportePago',compact('pagos','fecha','prestatario','idprest', 'tipo', 'tipo2', 'numl'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('pago.pdf');

    }

    public function generatePDF3(Request $request, $idprest, $fechadepago)

    {
        //$nombre = $request->get('cuenta');
        //$catcuentas = Catcuenta::orderBy('id','ASC')->nombre($nombre)->paginate(100);
        //$data = ['title' => 'Esta es una página de Prueba'];
        $pagos = Pago::where('idprest', $idprest)
                        ->where('fechadepago', $fechadepago)
                        ->get();
        //$idprest = Pago::select('idprest')
        //                            ->distinct()
        //                            ->where('id', $id)
        //                            ->get();
        //$idprest2 = $idprest->idprest;
        $venta = 0;
        $ventat = 0;
        $ventati = 0;
        foreach ($pagos as $key => $value) {
            $idprest = $value->idprest;
            $venta = $value->intereses;
            $venta=round($venta, 2);
            $ventat = $venta + $ventat;
        }
        $ventati = $ventat + 0;
        $ventati = round($ventati, 2);

        $prestatario = Prestatario::where('id', $idprest)
                              ->get();
        $date=new Carbon();
        $fecha = $date->format('d-m-Y');
        $tipo = "FACTURA EXPORTACIÓN";
        $tipo2 = 3;

        $numl=NumerosEnLetras::convertir($ventati,'Dolares','centavos');
        $numl = strtoupper($numl);

        $pdf = PDF::loadView('pago.reportePago',compact('pagos','fecha','prestatario','idprest', 'tipo', 'tipo2', 'numl'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('pago.pdf');

    }

    public function generatePDF4(Request $request, $idprest, $fechadepago)

    {
        //$nombre = $request->get('cuenta');
        //$catcuentas = Catcuenta::orderBy('id','ASC')->nombre($nombre)->paginate(100);
        //$data = ['title' => 'Esta es una página de Prueba'];
        $pagos = Pago::where('idprest', $idprest)
                        ->where('fechadepago', $fechadepago)
                        ->get();
        //$idprest = Pago::select('idprest')
        //                            ->distinct()
        //                            ->where('id', $id)
        //                            ->get();
        //$idprest2 = $idprest->idprest;
        $venta = 0;
        $ventat = 0;
        $ventati = 0;
        foreach ($pagos as $key => $value) {
            $idprest = $value->idprest;
            $venta = $value->intereses;
            $venta=round($venta, 2);
            $ventat = $venta + $ventat;
        }
        $ventati = $ventat + 0;
        $ventati = round($ventati, 2);

        $prestatario = Prestatario::where('id', $idprest)
                              ->get();
        $date=new Carbon();
        $fecha = $date->format('d-m-Y');
        $tipo = "FACTURA DE SUJETO EXCLUIDO";
        $tipo2 = 4;

        $numl=NumerosEnLetras::convertir($ventati,'Dolares','centavos');
        $numl = strtoupper($numl);

        $pdf = PDF::loadView('pago.reportePago',compact('pagos','fecha','prestatario','idprest', 'tipo', 'tipo2', 'numl'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('pago.pdf');

    }

    public function generatePDF5(Request $request, $idprest, $fechadepago)

    {
        //$nombre = $request->get('cuenta');
        //$catcuentas = Catcuenta::orderBy('id','ASC')->nombre($nombre)->paginate(100);
        //$data = ['title' => 'Esta es una página de Prueba'];
        $pagos = Pago::where('idprest', $idprest)
                        ->where('fechadepago', $fechadepago)
                        ->get();
        //$idprest = Pago::select('idprest')
        //                            ->distinct()
        //                            ->where('id', $id)
        //                            ->get();
        //$idprest2 = $idprest->idprest;
        $venta = 0;
        $ventat = 0;
        $ventati = 0;
        foreach ($pagos as $key => $value) {
            $idprest = $value->idprest;
            $venta = $value->intereses;
            $venta=round($venta, 2);
            $ventat = $venta + $ventat;
        }
        $ventati = $ventat + 0;
        $ventati = round($ventati, 2);

        $prestatario = Prestatario::where('id', $idprest)
                              ->get();
        $date=new Carbon();
        $fecha = $date->format('d-m-Y');
        $tipo = "NOTA DE CRÉDITO";
        $tipo2 = 5;

        $numl=NumerosEnLetras::convertir($ventati,'Dolares','centavos');
        $numl = strtoupper($numl);

        $pdf = PDF::loadView('pago.reportePago',compact('pagos','fecha','prestatario','idprest', 'tipo', 'tipo2', 'numl'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('pago.pdf');

    }


    public function jsonp(Request $request, $idprest, $fechadepago)

    {

      $pagos = Pago::where('idprest', $idprest)
                        ->where('fechadepago', $fechadepago)
                        ->get();
        //$idprest = Pago::select('idprest')
        //                            ->distinct()
        //                            ->where('id', $id)
        //                            ->get();
        //$idprest2 = $idprest->idprest;
        $cont = 0;
        foreach ($pagos as $key => $value) {
            $idprest = $value->idprest;
            $fechacreacion = $value->created_at;
            $cont++;
        }

        $fechac = substr($fechacreacion, 0, 10);
        $newDate = date("d/m/Y", strtotime($fechac));
        $horac = substr($fechacreacion, 11, 18);
        $prestatario = Prestatario::where('id', $idprest)
                              ->get();

        foreach ($prestatario as $key => $value2) {
            $nombre = $value2->pnombre . ' ' . $value2->snombre . ' ' . $value2->papellido . ' ' . $value2->sapellido . $value2->capellido;
            $tel = $value2->tel;
            $email = $value2->email;
            $dui = $value2->dui;
            $dir = $value2->direccion;
        }
        

        //$pago = Pago::find($id);
        $json1 = ('
                    {
                        "identification":{
                         "version": 3,
                         "ambiente": "00",
                         "tipoDte": "03",
                         "numeroControl": "DTE-03-M001P001-00000000000002",
                         "codigoGeneracion": "516413E3-12A2-78D5-7A79-6B6DFE861395",
                         "tipoModelo": 1,
                         "tipoOperacion": 1,
                         "tipoContingencia": null,
                         "motivoContin": null,
                         "fecEmi": "'. $newDate .'",
                         "horEmi": "'. $horac .'",
                         "tipoMoneda": "USD"
                        },
                        "documentoRelacionado": null,
                        "emisor": {
                            "nit": "0618-160722-101-0",
                            "nrc": " ",
                            "nombre": "EM & ASOCIADOS S.A. DE C.V",
                            "codActividad": "62020",
                            "descActividad": " ",
                            "nombreComercial": " ",
                            "tipoEstablecimiento": " ",
                            "direccion": {
                                "departamento": "06",
                                "municipio": "14",
                                "complemento": " "
                            },
                            "telefono": " ",
                            "correo": " ",
                            "codEstable": null,
                            "codPuntoVenta": null,
                            "codEstableMH": "4577",
                            "codPuntoVentaMH": "4578"
                        },
                        "receptor": {
                            "nrc": " ",
                            "nombre": "'. $nombre .'",
                            "codActividad": "46495",
                            "descActividad": " ",
                            "direccion": {
                                "departamento": "05",
                                "municipio": "06",
                                "complemento": "'. $dir .'"
                            },
                            "telefono": "'. $tel .'",
                            "correo": "'. $email .'",
                            "nombreComercial": " ",
                            "nit": "'. $dui .'"
                        },
                        "cuerpoDocumento": [
                        '); 

                        if ($cont > 1) {
                            $cont2=0;
                            $json2='';
                            foreach ($pagos as $key => $value3) {
                                $idprest = $value3->idprest;
                                $fechacreacion = $value3->created_at;
                                $cont2++;

                                $jsonaux = ('
                                {
                                    "numItem": '. $cont2 .',
                                    "tipoItem": 2,
                                    "cantidad": 1,
                                    "codigo": " ",
                                    "uniMedida": 99,
                                    "descripcion": "Intereses sobre prestamo",
                                    "precioUni": '. $value3->intereses .',
                                    "montoDescu": 0,
                                    "codTributo": null,
                                    "ventaNoSuj": 0,
                                    "ventaExenta": 0,
                                    "ventaGravada": 0,
                                    "tributos": [
                                        "20"
                                    ],
                                    "psv": 0,
                                    "noGravado": 0
                                }
                                ');
                            $json2 = $json2 . ',' . $jsonaux;
                            }

                        } else {
                            $cont2=0;
                        foreach ($pagos as $key => $value3) {
                            $idprest = $value->idprest;
                            $fechacreacion = $value->created_at;
                            $cont2++;
                        
                        $json2 = ('
                        {
                            "numItem": '. $cont2 .',
                            "tipoItem": 2,
                            "cantidad": 1,
                            "codigo": " ",
                            "uniMedida": 99,
                            "descripcion": "Intereses sobre prestamo",
                            "precioUni": '. $value->intereses .',
                            "montoDescu": 0,
                            "codTributo": null,
                            "ventaNoSuj": 0,
                            "ventaExenta": 0,
                            "ventaGravada": 0,
                            "tributos": [
                                "20"
                            ],
                            "psv": 0,
                            "noGravado": 0
                        }
                        ');
                        }
                        }
                        
                        
                         
                        
                        $json3 = ('
                        ],
                        "resumen": {
                            "totalNoSuj": 0,
                            "totalExenta": 0,
                            "totalGravada": 0,
                            "subTotalVentas": 0,
                            "descuNoSuj": 0,
                            "descuExenta": 0,
                            "descuGravada": 0,
                            "porcentajeDescuento": 0,
                            "totalDescu": 0,
                            "tributos": [
                            {
                                "codigo": " ",
                                "descripcion": "IVA",
                                "valor": 0
                            }
                            ],
                            "subTotal": 0,
                            "ivaPerci1": 0,
                            "ivaRete1": 0,
                            "reteRenta": 0,
                            "montoTotalOperacion": 0,
                            "totalNoGravado": 0,
                            "totalPagar": 0,
                            "totalLetras": " ",
                            "saldoFavor": 0,
                            "condicionOperacion": 1,
                            "pagos": null,
                            "numPagoElectronico": null
                            },
                            "extension": {
                                "nombEntrega": " ",
                                "docuEntrega": " ",
                                "nombRecibe": " ",
                                "docuRecibe": " ",
                                "placaVehiculo": " ",
                                "observaciones": " ",
                            },
                            "apendice": null,
                         ');
    $json = $json1 . $json2 . $json3;
    return view('pago.json',compact('json'));

    }

    

}
