<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libventas;
use Libventas1\http\Request\LibventasRequest;
use RealRashid\SweetAlert\Facades\Alert;
use PDF;
use Carbon\Carbon;

class LibventasController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:Dominios|Crear Dominio|Editar Dominio|Eliminar Dominio', ['only' => ['index','store']]);
         $this->middleware('permission:Dominios', ['only' => ['index']]);
         $this->middleware('permission:Crear Dominio', ['only' => ['create','store']]);
         $this->middleware('permission:Editar Dominio', ['only' => ['edit','update']]);
         $this->middleware('permission:Eliminar Dominio', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nombre = $request->get('nombreDominio');
        $libventas2 = Libventas::select('periodo')->distinct()->paginate(10);
        $libventas = Libventas::orderBy('id','DESC')->get();
               return view('libventas.index',compact('libventas', 'libventas2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $libventas = Libventas::all();
        $libventasc = Libventas::select('correlativo','fechaccf')->orderBy('correlativo', 'desc')->first();
        $ultfecha = Libventas::select('fechaccf')->orderBy('fechaccf', 'desc')->first();
        return view('libventas.create', compact('libventasc', 'ultfecha'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $correlativo = $request->get('correlativo');
        $fechaccf = $request->get('fechaccf');
        $montoGravado = $request->get('montoGravado');
        $año = substr($fechaccf, 0, 4);
        $mes = substr($fechaccf, 5, 2);
        $dia = substr($fechaccf, 8, 2);
        switch ($mes) {
            case '01':
                $periodo = 'Enero-'.$año;
                break;
            case '02':
                $periodo = 'Febrero-'.$año;
                break;
            case '03':
                $periodo = 'Marzo-'.$año;
                break;
            case '04':
                $periodo = 'Abril-'.$año;
                break;
            case '05':
                $periodo = 'Mayo-'.$año;
                break;
            case '06':
                $periodo = 'Junio-'.$año;
                break;
            case '07':
                $periodo = 'Julio-'.$año;
                break;
            case '08':
                $periodo = 'Agosto-'.$año;
                break;
            case '09':
                $periodo = 'Septiembre-'.$año;
                break;
            case '10':
                $periodo = 'Octubre-'.$año;
                break;
            case '11':
                $periodo = 'Noviembre-'.$año;
                break;
            case '12':
                $periodo = 'Diciembre-'.$año;
                break;
            
            default:
                $periodo = $mes.$año;
                break;
        }
        $libventa = new Libventas;
        $libventa->correlativo = $correlativo; 
        $libventa->fechaccf = $fechaccf; 
        $libventa->año = $año; 
        $libventa->mes = $mes; 
        $libventa->dia = $dia; 
        $libventa->periodo = $periodo; 
        $libventa->montoGravado = $montoGravado; 
        $libventa->estatus = '1';
        $libventa->save();
        Alert::success('Venta agregada con éxito');
        return redirect()->route('libventas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($mes)
    {
        $libventas = Libventas::find($id);
      return view('libventas.show',compact('libventas'));
    }

    public function detalle($periodo)

    {
        $libventas = Libventas::where('periodo', $periodo)->get();
        $libventas2 = Libventas::select('periodo')->distinct()->get();
        $libventas3 = Libventas::select('fechaccf')->distinct()->where('periodo', $periodo)->orderBy('fechaccf', 'asc')->get();

        return view('libventas.detalle',compact('libventas','libventas2','libventas3'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idDominio)
    {
        $libventas = Libventas::find($idDominio);
        return view('libventas.edit',compact('libventas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idDominio)
    {
        $this->validate($request,[
          'idDominio',
          'nombreDominio'
        ]);
        Libventas::find($idDominio)->update($request->all());
        Alert::success('Dominio actualizado con éxito');
        return redirect()->route('libventas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idDominio)
    {
       try{
            Libventas::find($idDominio)->delete();
            Alert::success('Dominio eliminado con exito');
        return redirect()->route('libventas.index');
            } catch  (\Illuminate\Database\QueryException $e){
                 Alert::danger('No se Puede eliminar este registro porque esta asociado con otros datos');
        return redirect()->route('libventas.index');
        }
    }

    public function exportExcel($ccf)

    {

        $libcompra2 = Libcompras::where('correlativo', $ccf)
                                    ->get();

        $export = new libcomprasExport($libcompra2);

        return Excel::download($export, 'Compras.xlsx');
        //return view('export.prestamo',compact('prestamo', 'pagos'));

    }

}
