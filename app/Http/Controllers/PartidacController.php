<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partidac;
use App\Partidacierre;
use App\Catcuenta;
use App\Prestatario;
use App\Contcuenta;
use App\ContSubcuenta;
use App\ContCuentaDetalle;
use App\ContElemento;
use App\ContRubro;
use App\CierreCont;
use App\CierreAux;
use Partidac1\http\Request\PartidacRequest;
use RealRashid\SweetAlert\Facades\Alert;
use PDF;
use Carbon\Carbon;
use App\Exports\partidacExport;
use App\Exports\partidacrExport;
use App\Exports\librocuentaExport;
use Maatwebsite\Excel\Facades\Excel;

class PartidacController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:Dominios|Crear Dominio|Editar Dominio|Eliminar Dominio', ['only' => ['index','store']]);
         $this->middleware('permission:Dominios', ['only' => ['index']]);
         $this->middleware('permission:Crear Dominio', ['only' => ['create','store']]);
         $this->middleware('permission:Editar Dominio', ['only' => ['edit','update']]);
         $this->middleware('permission:Eliminar Dominio', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $nombre =$request->get('nombre');
        //$partidac = Partidac::orderBy('correlativo','desc')->groupBy('correlativo')->having('correlativo', '>', 0)->get();
        $partidac2 = Partidac::select('correlativo')->distinct()->paginate(10);
        $partidac = Partidac::orderBy('id','ASC')->nombre($nombre)->get();
        $partidacs = Partidac::all();
        return view('partidac.index',compact('partidac', 'partidacs', 'partidac2'));
    }

    public function gestionp(Request $request)
    {
        $nombre =$request->get('nombre');
        //$partidac = Partidac::orderBy('correlativo','desc')->groupBy('correlativo')->having('correlativo', '>', 0)->get();
        $partidac2 = Partidacierre::select('estatus2')->distinct()->get();
        $partidac = Partidac::orderBy('id','ASC')->nombre($nombre)->paginate(10);
        $partidacs = Partidac::all();
        $cierreconts = CierreCont::all();
        $cierreauxs = CierreAux::orderBy('id','DESC')->paginate(10);
        return view('partidac.gestionp',compact('cierreauxs', 'partidac2'));
    }

    public function partidas($id)
    {
        $partidac2 = Partidacierre::select('correlativo')
                                    ->distinct()
                                    ->where('estatus2', $id)
                                    ->get();
        $partidac = Partidacierre::where('estatus2', $id)
                              ->get();
      return view('partidac.partidas_old',compact('partidac', 'partidac2'));
    }

    public function balance($id)
    {
        
        $partidac = CierreCont::where('idaux', $id)
                              ->get();
      return view('partidac.cierres_old',compact('partidac'));
    }

    public function detallepartida($id, $status)
    {
        $first = ContCuentaDetalle::select('cuentaDetalle', 'rubroDesc');
        $contcuentasd = ContSubcuenta::select('subcuenta', 'rubroDesc')
                                       ->whereNull('hijo')
                                       ->union($first)
                                       ->get();


        $contcuentadetalles = ContCuentaDetalle::all();
        $partidac2 = Partidacierre::where('estatus2', $status)
                                    ->where('correlativo', $id)
                                    ->get();
        $partidac = Partidacierre::where('correlativo', $id)
                              ->get();
      return view('partidac.detalle_partida_old',compact('partidac', 'contcuentadetalles', 'contcuentasd', 'partidac2'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $catcuentas = Catcuenta::all();
        //$contcuentasd = ContCuentaDetalle::all();
        $first = ContCuentaDetalle::select('cuentaDetalle', 'rubroDesc');
        $contcuentasd = ContSubcuenta::select('subcuenta', 'rubroDesc')
                                       ->whereNull('hijo')
                                       ->union($first)
                                       ->get();
        $partidacs = Partidac::select('correlativo')->orderBy('id', 'desc')->first();
        return view('partidac.create',compact('contcuentasd', 'partidacs'));
    }

    public function estadoc()
    {
        //$personas = TableCliente::all();
        //$productos = TableProductos::all();
        //$factura = TableFacturas::all();
        //$venta = TableVentas::all();
        $catcuentas = Catcuenta::all();
        $partidac = Partidac::all();
        $contelementos = ContElemento::all();
        $contrubros = ContRubro::all();
        $contcuentas = ContCuenta::all();
        $contsubcuentas = ContSubcuenta::all();
        $contcuentadetalles = ContCuentaDetalle::all();
        
        return view('partidac.estadoc',compact('catcuentas', 'partidac', 'contelementos', 'contrubros', 'contcuentas', 'contsubcuentas', 'contcuentadetalles'));
    }

    public function estadocr()
    {
        //$personas = TableCliente::all();
        //$productos = TableProductos::all();
        //$factura = TableFacturas::all();
        //$venta = TableVentas::all();
        $catcuentas = Catcuenta::all();
        $partidac = Partidac::all();
        $contelementos = ContElemento::all();
        $contrubros = ContRubro::all();
        $contcuentas = ContCuenta::all();
        $contsubcuentas = ContSubcuenta::all();
        $contcuentadetalles = ContCuentaDetalle::all();
        
        return view('partidac.estadocr',compact('catcuentas', 'partidac', 'contelementos', 'contrubros', 'contcuentas', 'contsubcuentas', 'contcuentadetalles'));
    }

    public function estadoco()
    {
        //$personas = TableCliente::all();
        //$productos = TableProductos::all();
        //$factura = TableFacturas::all();
        //$venta = TableVentas::all();
        $catcuentas = Catcuenta::all();
        $partidac = Partidac::all();
        $partidacs = Partidac::all()->unique('correlativo');
        
        return view('partidac.estadoco',compact('catcuentas', 'partidac', 'partidacs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**$this->validate($request,[
            'No_Facturas',
            'Productos',
            'cantidad'
            factura= cliente, fecha, total
            Cliente= id
        ]);
        
        TableVentas::create($request->all());
        return redirect()->route('tableVentas.create')->with('success','Venta guardado con éxito');*/
        $total = 0;
        $estatus = 1;
        $totalf = 0;
        $cant = 0;
        $cont = 0;
        $cuenta = $request->get('idcuenta');
        $desc = $request->get('desc');
        $cantidad = $request->get('cantidad');
        $tipo2 = $request->get('tipo2');
        $debe = $request->get('debe');
        $haber = $request->get('haber');
        while ($cont < count($cuenta)) {
        $cuenta2= trim($cuenta[$cont]);
        $partidac = new Partidac;
        $partidac->idcatalogo = $cuenta2; 
        $partidac->tipo = $request->get('tipo'); 
        $partidac->correlativo = $request->get('correlativo');
        $partidac->concepto = $request->get('concepto');
        $partidac->fecha = $request->get('fecha');
        $partidac->descripcion = $desc[$cont];
        $partidac->debe = $debe[$cont];
        $partidac->haber = $haber[$cont];
        $partidac->tipo2 = $tipo2[$cont];
        $partidac->estatus = $estatus;
        $partidac->save();
        $cantCaracteres= strlen($cuenta2);

        $cuentaDetalleAfectadav = ContCuentaDetalle::whereCuentadetalle($cuenta2)->first();
        if (empty($cuentaDetalleAfectadav)) {
            $subcuentaAfectada = ContSubcuenta::whereSubcuenta($cuenta2)->first();
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $subcuentaAfectada->debe;
            $shaber = $subcuentaAfectada->haber;
            $subcuentaAfectada->debe = $sdebe + $debe[$cont];
            $subcuentaAfectada->haber = $shaber + $haber[$cont];
            $subcuentaAfectada->save();

            $subcuentaAfectadas = ContSubcuenta::whereSubcuenta($cuenta2)->first();
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $subcuentaAfectadas->saldoInicial;
            $ssaldos = $subcuentaAfectadas->saldo;
            $sdebes = $subcuentaAfectadas->debe;
            $shabers = $subcuentaAfectadas->haber;
            $concuenta = $subcuentaAfectadas->padre;
            $subcuentaAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $subcuentaAfectadas->save();
        } else {
            $sdebe = 0;
            $shaber = 0;
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            //$cuentaDetalleAfectada = ContCuentaDetalle::find($cuenta2);
            $cuentaDetalleAfectada = ContCuentaDetalle::whereCuentadetalle($cuenta2)->first();
            $sdebe = $cuentaDetalleAfectada->debe;
            $shaber = $cuentaDetalleAfectada->haber;
            $cuentaDetalleAfectada->debe = $sdebe + $debe[$cont];
            $cuentaDetalleAfectada->haber = $shaber + $haber[$cont];
            $cuentaDetalleAfectada->save();

            $cuentaDetalleAfectadaS = ContCuentaDetalle::whereCuentadetalle($cuenta2)->first();
            $ssaldois = $cuentaDetalleAfectadaS->saldoInicial;
            $ssaldos = $cuentaDetalleAfectadaS->saldo;
            $sdebes = $cuentaDetalleAfectadaS->debe;
            $shabers = $cuentaDetalleAfectadaS->haber;
            $subcuenta = $cuentaDetalleAfectadaS->padre;
            $cuentaDetalleAfectadaS->saldo = ($ssaldois + $sdebes - $shabers);
            $cuentaDetalleAfectadaS->save();

            $subcuentaAfectada = ContSubcuenta::find($subcuenta);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $subcuentaAfectada->debe;
            $shaber = $subcuentaAfectada->haber;
            $subcuentaAfectada->debe = $sdebe + $debe[$cont];
            $subcuentaAfectada->haber = $shaber + $haber[$cont];
            $subcuentaAfectada->save();

            $subcuentaAfectadas = ContSubcuenta::find($subcuenta);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $subcuentaAfectadas->saldoInicial;
            $ssaldos = $subcuentaAfectadas->saldo;
            $sdebes = $subcuentaAfectadas->debe;
            $shabers = $subcuentaAfectadas->haber;
            $concuenta = $subcuentaAfectadas->padre;
            $subcuentaAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $subcuentaAfectadas->save();
        }
        
            $cuentaAfectada = ContCuenta::find($concuenta);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $cuentaAfectada->debe;
            $shaber = $cuentaAfectada->haber;
            $cuentaAfectada->debe = $sdebe + $debe[$cont];
            $cuentaAfectada->haber = $shaber + $haber[$cont];
            $cuentaAfectada->save();

            $cuentaAfectadas = ContCuenta::find($concuenta);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $cuentaAfectadas->saldoInicial;
            $ssaldos = $cuentaAfectadas->saldo;
            $sdebes = $cuentaAfectadas->debe;
            $shabers = $cuentaAfectadas->haber;
            $rubro = $cuentaAfectadas->padre;
            $cuentaAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $cuentaAfectadas->save();

            $rubroAfectada = ContRubro::find($rubro);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $rubroAfectada->debe;
            $shaber = $rubroAfectada->haber;
            $rubroAfectada->debe = $sdebe + $debe[$cont];
            $rubroAfectada->haber = $shaber + $haber[$cont];
            $rubroAfectada->save();

            $rubroAfectadas = ContRubro::find($rubro);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $rubroAfectadas->saldoInicial;
            $ssaldos = $rubroAfectadas->saldo;
            $sdebes = $rubroAfectadas->debe;
            $shabers = $rubroAfectadas->haber;
            $elemento = $rubroAfectadas->padre;
            $rubroAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $rubroAfectadas->save();

            $elementoAfectada = ContElemento::find($elemento);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $elementoAfectada->debe;
            $shaber = $elementoAfectada->haber;
            $elementoAfectada->debe = $sdebe + $debe[$cont];
            $elementoAfectada->haber = $shaber + $haber[$cont];
            $elementoAfectada->save();

            $elementoAfectadas = ContElemento::find($elemento);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $elementoAfectadas->saldoInicial;
            $ssaldos = $elementoAfectadas->saldo;
            $sdebes = $elementoAfectadas->debe;
            $shabers = $elementoAfectadas->haber;
            $elementoAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $elementoAfectadas->save();

        
        //$cuentaAfectada = ;

        $cont = $cont + 1;

  }
        return redirect()->route('partidac.index')->with('success','Partida actualizado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $first = ContCuentaDetalle::select('cuentaDetalle', 'rubroDesc');
        $contcuentasd = ContSubcuenta::select('subcuenta', 'rubroDesc')
                                       ->whereNull('hijo')
                                       ->union($first)
                                       ->get();


        $contcuentadetalles = ContCuentaDetalle::all();
        $partidac = Partidac::where('correlativo', $id)
                              ->get();
      return view('partidac.show',compact('partidac', 'contcuentadetalles', 'contcuentasd'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partidac2 = Partidac::find($id);
        $partidac = Partidac::where('correlativo', $id)
                                    ->get();
        return view('partidac.edit',compact('partidac'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
          'id_facturas',
            'id_productos',
            'cantidad'
        ]);
        Partidac::find($id)->update($request->all());
        return redirect()->route('partidac.index')->with('success','Partida actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        /**
        try{
            Partidac::find($id)->delete();
            Alert::success('Partida eliminada con exito');
        return redirect()->route('partidac.index');
    		} catch  (\Illuminate\Database\QueryException $e){
                 Alert::danger('No se Puede eliminar este registro porque esta asociado con otra asignación');
        return redirect()->route('partidac.index');
        }**/

        $partidac = Partidac::where('correlativo', $id)
                              ->where('estatus', 1)
                              ->get();
        foreach ($partidac as $key => $value) {

            $cantCaracteres= strlen($value->idcatalogo);

        $cuentaDetalleAfectadav = ContCuentaDetalle::whereCuentadetalle($value->idcatalogo)->first();


        if (empty($cuentaDetalleAfectadav)) {
            $sdebe = 0;
            $shaber = 0;
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $rdebes = $value->debe;
            $rhabers = $value->haber;
            $subcuentaAfectada = ContSubcuenta::whereSubcuenta($value->idcatalogo)->first();
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $subcuentaAfectada->debe;
            $shaber = $subcuentaAfectada->haber;
            $subcuentaAfectada->debe = $sdebe - $rdebes;
            $subcuentaAfectada->haber = $shaber - abs($rhabers);
            $subcuentaAfectada->save();

            $subcuentaAfectadas = ContSubcuenta::whereSubcuenta($value->idcatalogo)->first();
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $subcuentaAfectadas->saldoInicial;
            $ssaldos = $subcuentaAfectadas->saldo;
            $sdebes = $subcuentaAfectadas->debe;
            $shabers = $subcuentaAfectadas->haber;
            $concuenta = $subcuentaAfectadas->padre;
            $subcuentaAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $subcuentaAfectadas->save();
        } else {
            $sdebe = 0;
            $shaber = 0;
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $rdebes = 0;
            $rhabers = 0;
            $rdebes = $value->debe;
            $rhabers = $value->haber;
            //$cuentaDetalleAfectada = ContCuentaDetalle::find($cuenta2);
            $cuentaDetalleAfectada = ContCuentaDetalle::whereCuentadetalle($value->idcatalogo)->first();
            $sdebe = $cuentaDetalleAfectada->debe;
            $shaber = $cuentaDetalleAfectada->haber;
            $cuentaDetalleAfectada->debe = $sdebe - $rdebes;
            $cuentaDetalleAfectada->haber = $shaber - abs($rhabers);
            $cuentaDetalleAfectada->save();

            $cuentaDetalleAfectadaS = ContCuentaDetalle::whereCuentadetalle($value->idcatalogo)->first();
            $ssaldois = $cuentaDetalleAfectadaS->saldoInicial;
            $ssaldos = $cuentaDetalleAfectadaS->saldo;
            $sdebes = $cuentaDetalleAfectadaS->debe;
            $shabers = $cuentaDetalleAfectadaS->haber;
            $subcuenta = $cuentaDetalleAfectadaS->padre;
            $cuentaDetalleAfectadaS->saldo = ($ssaldois + $sdebes - $shabers);
            $cuentaDetalleAfectadaS->save();

            $subcuentaAfectada = ContSubcuenta::find($subcuenta);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $subcuentaAfectada->debe;
            $shaber = $subcuentaAfectada->haber;
            $subcuentaAfectada->debe = $sdebe - $rdebes;
            $subcuentaAfectada->haber = $shaber - abs($rhabers);
            $subcuentaAfectada->save();

            $subcuentaAfectadas = ContSubcuenta::find($subcuenta);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $subcuentaAfectadas->saldoInicial;
            $ssaldos = $subcuentaAfectadas->saldo;
            $sdebes = $subcuentaAfectadas->debe;
            $shabers = $subcuentaAfectadas->haber;
            $concuenta = $subcuentaAfectadas->padre;
            $subcuentaAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $subcuentaAfectadas->save();
        } 
        

        

        
            $cuentaAfectada = ContCuenta::find($concuenta);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $cuentaAfectada->debe;
            $shaber = $cuentaAfectada->haber;
            $cuentaAfectada->debe = $sdebe - $rdebes;
            $cuentaAfectada->haber = $shaber - abs($rhabers);
            $cuentaAfectada->save();

            $cuentaAfectadas = ContCuenta::find($concuenta);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $cuentaAfectadas->saldoInicial;
            $ssaldos = $cuentaAfectadas->saldo;
            $sdebes = $cuentaAfectadas->debe;
            $shabers = $cuentaAfectadas->haber;
            $rubro = $cuentaAfectadas->padre;
            $cuentaAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $cuentaAfectadas->save();

            $rubroAfectada = ContRubro::find($rubro);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $rubroAfectada->debe;
            $shaber = $rubroAfectada->haber;
            $rubroAfectada->debe = $sdebe - $rdebes;
            $rubroAfectada->haber = $shaber - abs($rhabers);
            $rubroAfectada->save();

            $rubroAfectadas = ContRubro::find($rubro);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $rubroAfectadas->saldoInicial;
            $ssaldos = $rubroAfectadas->saldo;
            $sdebes = $rubroAfectadas->debe;
            $shabers = $rubroAfectadas->haber;
            $elemento = $rubroAfectadas->padre;
            $rubroAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $rubroAfectadas->save();

            $elementoAfectada = ContElemento::find($elemento);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $elementoAfectada->debe;
            $shaber = $elementoAfectada->haber;
            $elementoAfectada->debe = $sdebe - $rdebes;
            $elementoAfectada->haber = $shaber - abs($rhabers);
            $elementoAfectada->save();

            $elementoAfectadas = ContElemento::find($elemento);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $elementoAfectadas->saldoInicial;
            $ssaldos = $elementoAfectadas->saldo;
            $sdebes = $elementoAfectadas->debe;
            $shabers = $elementoAfectadas->haber;
            $elementoAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $elementoAfectadas->save();

        
        //$cuentaAfectada = ;

        $value->delete();

  }
            Alert::success('Partida eliminada con exito');
        return redirect()->route('partidac.index');
            

    }

     public function generatePDF(Request $request)

    {
        //$nombre = $request->get('cuenta');
        //$catcuentas = Catcuenta::orderBy('id','ASC')->nombre($nombre)->paginate(100);
        //$data = ['title' => 'Esta es una página de Prueba'];
        $catcuentas = Catcuenta::all();
        $partidac = Partidac::all();
        $contelementos = ContElemento::all();
        $contrubros = ContRubro::all();
        $contcuentas = ContCuenta::all();
        $contsubcuentas = ContSubcuenta::all();
        $contcuentadetalles = ContCuentaDetalle::all();
        
        //return view('partidac.estadocr',compact('catcuentas', 'partidac', 'contelementos', 'contrubros', 'contcuentas', 'contsubcuentas', 'contcuentadetalles'));

        //$catcuentas = Catcuenta::all();
        //$partidac = Partidac::all();
        //$partidacs = Partidac::all()->unique('correlativo');
        
        //$partidacs = Partidac::find($id);
        $date=new Carbon();
        $fecha = $date->format('d-m-Y');

        $pdf = PDF::loadView('partidac.reportePartidac',compact('catcuentas', 'partidac', 'contelementos', 'contrubros', 'contcuentas', 'contsubcuentas', 'contcuentadetalles','fecha'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('partidac.pdf');

    }

    public function generatePDF2(Request $request)

    {
        //$nombre = $request->get('cuenta');
        //$catcuentas = Catcuenta::orderBy('id','ASC')->nombre($nombre)->paginate(100);
        //$data = ['title' => 'Esta es una página de Prueba'];
        $catcuentas = Catcuenta::all();
        $partidac = Partidac::all();
        $contelementos = ContElemento::all();
        $contrubros = ContRubro::all();
        $contcuentas = ContCuenta::all();
        $contsubcuentas = ContSubcuenta::all();
        $contcuentadetalles = ContCuentaDetalle::all();
        
        //return view('partidac.estadocr',compact('catcuentas', 'partidac', 'contelementos', 'contrubros', 'contcuentas', 'contsubcuentas', 'contcuentadetalles'));

        //$catcuentas = Catcuenta::all();
        //$partidac = Partidac::all();
        //$partidacs = Partidac::all()->unique('correlativo');
        
        //$partidacs = Partidac::find($id);
        $date=new Carbon();
        $fecha = $date->format('d-m-Y');

        $pdf = PDF::loadView('partidac.reportePartidacr',compact('catcuentas', 'partidac', 'contelementos', 'contrubros', 'contcuentas', 'contsubcuentas', 'contcuentadetalles','fecha'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('partidac.pdf');
        }

        public function generatePDF3($id)

    {
        $first = ContCuentaDetalle::select('cuentaDetalle', 'rubroDesc');
        $contcuentasd = ContSubcuenta::select('subcuenta', 'rubroDesc')
                                       ->whereNull('hijo')
                                       ->union($first)
                                       ->get();
        $num = $id;

        $contcuentadetalles = ContCuentaDetalle::all();
        $partidac = Partidac::where('correlativo', $id)
                              ->get();
        $date=new Carbon();
        $fecha = $date->format('d/m/Y');

        $pdf = PDF::loadView('partidac.reportePartidacShow',compact('contcuentasd', 'contcuentadetalles', 'partidac','fecha', 'num'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('partidac.pdf');
        }

        public function generatePDF4($id, $status)

    {
        $first = ContCuentaDetalle::select('cuentaDetalle', 'rubroDesc');
        $contcuentasd = ContSubcuenta::select('subcuenta', 'rubroDesc')
                                       ->whereNull('hijo')
                                       ->union($first)
                                       ->get();
        $num = $id;
        $partidac2 = Partidacierre::where('estatus2', $status)
                                    ->where('correlativo', $id)
                                    ->get();
        $contcuentadetalles = ContCuentaDetalle::all();
        $partidac = Partidacierre::where('estatus2', $id)
                              ->get();
        $date=new Carbon();
        $fecha = $date->format('d/m/Y');

        $fechaCierre = Partidacierre::select('fecha')
                                      ->where('estatus2', $status)
                                      ->where('correlativo', $id)
                                      ->distinct()
                                      ->first();

        $conceptoCierre = Partidacierre::select('concepto')
                                      ->where('estatus2', $status)
                                      ->where('correlativo', $id)
                                      ->distinct()
                                      ->first();

        $tipoCierre = Partidacierre::select('tipo')
                                      ->where('estatus2', $status)
                                      ->where('correlativo', $id)
                                      ->distinct()
                                      ->first();

        if ($tipoCierre->tipo == '1') {
            $vartipo = 'INGRESO';
        } else { 
            if ($tipoCierre->tipo == '2') {
            $vartipo = 'EGRESO';
        } else {
            if ($tipoCierre->tipo == '3') {
            $vartipo = 'DIARIO';
        } else {
            if ($tipoCierre->tipo == '4') {
            $vartipo = 'CXC';
        } else {
            $vartipo = 'CXP';
            
        }
        }
        }
            
        }
        

        $pdf = PDF::loadView('partidac.reportePartidacShow',compact('contcuentasd', 'contcuentadetalles', 'partidac2', 'partidac','fecha', 'num', 'fechaCierre', 'vartipo', 'conceptoCierre'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('partidac.pdf');
        }

        public function generatePDF5($id)

    {
        $partidac2 = Partidacierre::select('idcatalogo')
                                    ->distinct()
                                    ->where('estatus2', $id)
                                    ->orderBy('idcatalogo','ASC')
                                    ->get();
        $partidac = Partidacierre::where('estatus2', $id)
                              ->get();
        $balance = cierrecont::where('idaux', $id)
                              ->get();
        

        $pdf = PDF::loadView('partidac.reporteLibroMayorCierre',compact('partidac', 'partidac2', 'balance'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('partidac.pdf');
        }

        public function libroMayor($id)
    {
        $partidac2 = Partidacierre::select('idcatalogo')
                                    ->distinct()
                                    ->where('estatus2', $id)
                                    ->orderBy('idcatalogo','ASC')
                                    ->get();
        $partidac = Partidacierre::where('estatus2', $id)
                              ->get();
        $balance = cierrecont::where('idaux', $id)
                              ->get();
        $num = $id;
      return view('partidac.libroMayorCierre',compact('partidac', 'partidac2', 'balance','num'));
    }

    public function exportExcel()

    {
        
        $fecha=date("dmy");

        return Excel::download(new partidacExport, 'Balance'.$fecha.'.xlsx');
    }

    public function exportExcel2()

    {
        
        $fecha=date("dmy");

        return Excel::download(new partidacrExport, 'Balance'.$fecha.'.xlsx');
    }

    public function exportExcelp($fechai, $fechaf, $cuenta)

    {
        $first = Partidac::select('*')
                            ->whereBetween('fecha', [$fechai, $fechaf])
                            ->where('idcatalogo', $cuenta);
        $partidas = Partidacierre::select('*')
                                       ->whereBetween('fecha', [$fechai, $fechaf])
                                       ->where('idcatalogo', $cuenta)
                                       ->union($first)
                                       ->get();

        $partidas2 = Partidacierre::select('*')
                                       ->where('fecha','<', $fechai)
                                       ->where('idcatalogo', $cuenta)
                                       ->get();
        
        $cuentan = $cuenta;
        //$apellidop = $prestatario->papellido;
        //$date=new Carbon();
        //$fecha = $date->format('d-m-Y');

        //$excel = Excel::loadView('prestamo.reportePrestamo',compact('prestamos','fecha','pagos'));
        //$pdf->getDomPDF()->set_option("enable_php", TRUE);
        //return $pdf->stream('prestamo.pdf');
        $export = new librocuentaExport($partidas, $partidas2);

        return Excel::download($export, 'Cuenta'.'_'.$cuentan.'.xlsx');
        //return view('export.prestamo',compact('prestamo', 'pagos'));

    }

    public function cierre()
    {

        //$personas = TableCliente::all();
        //$productos = TableProductos::all();
        //$factura = TableFacturas::all();
        //$venta = TableVentas::all();
        $catcuentas = Catcuenta::all();
        $partidac = Partidac::all();
        $contelementos = ContElemento::all();
        $contrubros = ContRubro::all();
        $contcuentas = ContCuenta::all();
        $contsubcuentas = ContSubcuenta::all();
        $contcuentadetalles = ContCuentaDetalle::all();
        
        return view('partidac.cierre',compact('catcuentas', 'partidac', 'contelementos', 'contrubros', 'contcuentas', 'contsubcuentas', 'contcuentadetalles'));

    }

    public function procesoCierre(Request $request)
    {
        $contelementos = ContElemento::all();
        $contrubros = ContRubro::all();
        $contcuentas = ContCuenta::all();
        $contsubcuentas = ContSubcuenta::all();
        $contcuentadetalles = ContCuentaDetalle::all();

        $periodoIngresado = $request->get('periodo');
        $anioIngresado = $request->get('anio');
        $fechaIngresado = $request->get('fecha');


        $tipo2 = 'pre';
        $this->trasladoCierre($tipo2, $periodoIngresado, $anioIngresado, $fechaIngresado);

        $cierreaux2 = CierreAux::latest('id')->first();
        $idaux = $cierreaux2->id;

        foreach ($contcuentadetalles as $key => $value) {
            $id = $value->id;
            $saldo = $value->saldo;
            $debe = $value->debe;
            $haber = $value->haber;
            $elemento = ContCuentaDetalle::find($id);
            $elemento->saldoInicial = $saldo;
            $elemento->debe = 0;
            $elemento->haber = 0;
            //$elemento->saldo = 0;
            $elemento->save();
        }

        
        $this->cierreSubcuenta();
        $this->cierreCuenta();
        $this->cierreRubro();
        $this->cierreElemento();
        $this->trasladoPartidas($idaux);
        $tipo2 = 'post2';
        $this->trasladoCierre($tipo2, $periodoIngresado, $anioIngresado, $fechaIngresado);

        return redirect()->route('partidac.index')->with('success','Cierre realizado con exito');
        //return view('partidac.index')->with('success','Cierre realizado con exito');
    }

    public function cierreSubcuenta(){
        $contsubcuentas = ContSubcuenta::all();
        $contcuentadetalles = ContCuentaDetalle::all();


        foreach ($contsubcuentas as $key => $value2) {
            if ($value2->hijo == '1') {
            
            $ssaldoi = 0;
            $tsaldoi = 0;
            $id = $value2->id;
            $saldo = $value2->saldo;
            $debe = $value2->debe;
            $haber = $value2->haber;
            foreach ($contcuentadetalles as $key => $value3) {
                if ($value3->padre == $value2->id) {
                    $ssaldoi = $value3->saldoInicial;
                    $tsaldoi = $tsaldoi + $ssaldoi;
                }
            }
            $elemento = ContSubcuenta::find($id);
            $elemento->saldoInicial = $tsaldoi;
            $elemento->saldo = $tsaldoi;
            $elemento->debe = 0;
            $elemento->haber = 0;
            //$elemento->saldo = 0;
            $elemento->save();
            }else{

            $id = $value2->id;
            $saldo = $value2->saldo;
            $debe = $value2->debe;
            $haber = $value2->haber;
            $elemento = ContSubcuenta::find($id);
            $elemento->saldoInicial = $saldo;
            $elemento->debe = 0;
            $elemento->haber = 0;
            //$elemento->saldo = 0;
            $elemento->save();
            }
            
        }

    }

    public function cierreCuenta(){
        $contcuentas = ContCuenta::all();
        $contsubcuentas = ContSubcuenta::all();


        foreach ($contcuentas as $key => $value4) {
            $ssaldoi = 0;
            $tsaldoi = 0;
            $id = $value4->id;
            $saldo = $value4->saldo;
            $debe = $value4->debe;
            $haber = $value4->haber;
            foreach ($contsubcuentas as $key => $value5) {
                if ($value5->padre == $value4->id) {
                    $ssaldoi = $value5->saldoInicial;
                    $tsaldoi = $tsaldoi + $ssaldoi;
                }
            }
            $elemento = ContCuenta::find($id);
            $elemento->saldoInicial = $tsaldoi;
            $elemento->saldo = $tsaldoi;
            $elemento->debe = 0;
            $elemento->haber = 0;
            //$elemento->saldo = 0;
            $elemento->save();
        }

    }

    public function cierreRubro(){
        $contrubros = ContRubro::all();
        $contcuentas = ContCuenta::all();



        foreach ($contrubros as $key => $value6) {
            $ssaldoi = 0;
            $tsaldoi = 0;
            $id = $value6->id;
            $saldo = $value6->saldo;
            $debe = $value6->debe;
            $haber = $value6->haber;
            foreach ($contcuentas as $key => $value7) {
                if ($value7->padre == $value6->id) {
                    $ssaldoi = $value7->saldoInicial;
                    $tsaldoi = $tsaldoi + $ssaldoi;
                }
            }
            $elemento = ContRubro::find($id);
            $elemento->saldoInicial = $tsaldoi;
            $elemento->saldo = $tsaldoi;
            $elemento->debe = 0;
            $elemento->haber = 0;
            //$elemento->saldo = 0;
            $elemento->save();
        }
    }

    public function cierreElemento(){
        $contelementos = ContElemento::all();
        $contrubros = ContRubro::all();

        foreach ($contelementos as $key => $value8) {
            $ssaldoi = 0;
            $tsaldoi = 0;
            $id = $value8->id;
            $saldo = $value8->saldo;
            $debe = $value8->debe;
            $haber = $value8->haber;
            foreach ($contrubros as $key => $value9) {
                if ($value9->padre == $value8->id) {
                    $ssaldoi = $value9->saldoInicial;
                    $tsaldoi = $tsaldoi + $ssaldoi;
                }
            }
            $elemento = ContElemento::find($id);
            $elemento->saldoInicial = $tsaldoi;
            $elemento->saldo = $tsaldoi;
            $elemento->debe = 0;
            $elemento->haber = 0;
            //$elemento->saldo = 0;
            $elemento->save();
        }
    }

    public function trasladoPartidas($idaux){
        $partidas = Partidac::all();

        foreach ($partidas as $key => $value) {
            $partidacierre = new Partidacierre;
            $partidacierre->idcatalogo = $value->idcatalogo;
            $partidacierre->tipo = $value->tipo;
            $partidacierre->tipo2 = $value->tipo2;
            $partidacierre->correlativo = $value->correlativo;
            $partidacierre->fecha = $value->fecha;
            $partidacierre->descripcion = $value->descripcion;
            $partidacierre->concepto = $value->concepto;
            $partidacierre->saldoInicial = $value->saldoInicial;
            $partidacierre->debe = $value->debe;
            $partidacierre->haber = $value->haber;
            $partidacierre->saldo = $value->saldo;
            $partidacierre->estatus = $value->estatus;
            $partidacierre->estatus2 = $idaux;
            $partidacierre->save();
            $value->delete();
        }
    }

    public function trasladoCierre($tipo2, $periodoIngresado, $anioIngresado, $fechaIngresado){ 
        $contelementos = ContElemento::all();
        $contrubros = ContRubro::all();
        $contcuentas = ContCuenta::all();
        $contsubcuentas = ContSubcuenta::all();
        $contcuentadetalles = ContCuentaDetalle::all();

        $hoy = getdate();
        $date=new Carbon();
        $date->format('d-m-Y H:i:s');
        $fecha = $date->modify('-6 hours');
        



        $cierreaux = new CierreAux;
        $cierreaux->periodo = $periodoIngresado;
        $cierreaux->anio = $anioIngresado;
        $cierreaux->fechaCierre = $fecha;
        $cierreaux->fechaUltCierre = $fechaIngresado;
        $cierreaux->estatus = '1';
        $cierreaux->estatus2 = $tipo2;
        $cierreaux->save();

        $cierreaux2 = CierreAux::latest('id')->first();
        $idaux = $cierreaux2->id;


        foreach ($contelementos as $key => $value) {
            $cierrecont = new CierreCont;
            $cierrecont->idaux = $idaux;
            $cierrecont->cuenta = $value->elemento;
            $cierrecont->tipo = $value->tipo;
            $cierrecont->tipo2 = $tipo2;
            $cierrecont->padre = $value->padre;
            $cierrecont->hijo = $value->hijo;
            $cierrecont->rubroDesc = $value->rubroDesc;
            $cierrecont->saldoInicial = $value->saldoInicial;
            $cierrecont->debe = $value->debe;
            $cierrecont->haber = $value->haber;
            $cierrecont->saldo = $value->saldo;
            $cierrecont->periodo = $periodoIngresado;
            $cierrecont->estatus = $value->estatus;
            $cierrecont->estatus2 = 1;
            $cierrecont->fechaCierre = $fecha;
            $cierrecont->save();

            foreach ($contrubros as $key => $value2) {
                if ($value->id == $value2->padre) {
                    $cierrecont2 = new CierreCont;
                    $cierrecont2->idaux = $idaux;
                    $cierrecont2->cuenta = $value2->rubro;
                    $cierrecont2->tipo = $value2->tipo;
                    $cierrecont2->tipo2 = $tipo2;
                    $cierrecont2->padre = $value2->padre;
                    $cierrecont2->hijo = $value2->hijo;
                    $cierrecont2->rubroDesc = $value2->rubroDesc;
                    $cierrecont2->saldoInicial = $value2->saldoInicial;
                    $cierrecont2->debe = $value2->debe;
                    $cierrecont2->haber = $value2->haber;
                    $cierrecont2->saldo = $value2->saldo;
                    $cierrecont2->periodo = $periodoIngresado;
                    $cierrecont2->estatus = $value2->estatus;
                    $cierrecont2->estatus2 = 1;
                    $cierrecont2->fechaCierre = $fecha;
                    $cierrecont2->save();
                //}
                foreach ($contcuentas as $key => $value3) {
                    if ($value2->id == $value3->padre) {
                        $cierrecont3 = new CierreCont;
                        $cierrecont3->idaux = $idaux;
                        $cierrecont3->cuenta = $value3->cuenta;
                        $cierrecont3->tipo = $value3->tipo;
                        $cierrecont3->tipo2 = $tipo2;
                        $cierrecont3->padre = $value3->padre;
                        $cierrecont3->hijo = $value3->hijo;
                        $cierrecont3->rubroDesc = $value3->rubroDesc;
                        $cierrecont3->saldoInicial = $value3->saldoInicial;
                        $cierrecont3->debe = $value3->debe;
                        $cierrecont3->haber = $value3->haber;
                        $cierrecont3->saldo = $value3->saldo;
                        $cierrecont3->periodo = $periodoIngresado;
                        $cierrecont3->estatus = $value3->estatus;
                        $cierrecont3->estatus2 = 1;
                        $cierrecont3->fechaCierre = $fecha;
                        $cierrecont3->save();
        //}
                    foreach ($contsubcuentas as $key => $value4) {
                        if ($value3->id == $value4->padre) {
                            $cierrecont4 = new CierreCont;
                            $cierrecont4->idaux = $idaux;
                            $cierrecont4->cuenta = $value4->subcuenta;
                            $cierrecont4->tipo = $value4->tipo;
                            $cierrecont4->tipo2 = $tipo2;
                            $cierrecont4->padre = $value4->padre;
                            $cierrecont4->hijo = $value4->hijo;
                            $cierrecont4->rubroDesc = $value4->rubroDesc;
                            $cierrecont4->saldoInicial = $value4->saldoInicial;
                            $cierrecont4->debe = $value4->debe;
                            $cierrecont4->haber = $value4->haber;
                            $cierrecont4->saldo = $value4->saldo;
                            $cierrecont4->periodo = $periodoIngresado;
                            $cierrecont4->estatus = $value4->estatus;
                            $cierrecont4->estatus2 = 1;
                            $cierrecont4->fechaCierre = $fecha;
                            $cierrecont4->save();
        //}
                        foreach ($contcuentadetalles as $key => $value5) {
                            if ($value4->id == $value5->padre) {
                                $cierrecont5 = new CierreCont;
                                $cierrecont5->idaux = $idaux;
                                $cierrecont5->cuenta = $value5->cuentaDetalle;
                                $cierrecont5->tipo = $value5->tipo;
                                $cierrecont5->tipo2 = $tipo2;
                                $cierrecont5->padre = $value5->padre;
                                $cierrecont5->hijo = $value5->hijo;
                                $cierrecont5->rubroDesc = $value5->rubroDesc;
                                $cierrecont5->saldoInicial = $value5->saldoInicial;
                                $cierrecont5->debe = $value5->debe;
                                $cierrecont5->haber = $value5->haber;
                                $cierrecont5->saldo = $value5->saldo;
                                $cierrecont5->periodo = $periodoIngresado;
                                $cierrecont5->estatus = $value5->estatus;
                                $cierrecont5->estatus2 = 1;
                                $cierrecont5->fechaCierre = $fecha;
                                $cierrecont5->save();
                            }
                         }
                    }
                    }
                }
                }
            }
            }
        }
    }


    public function Partidash(Request $request) {
        $fechai = $request->get('fechainicial');
        $fechaf = $request->get('fechafinal');
        $cuenta = $request->get('cuenta');
        $first = Partidac::select('*')
                            ->whereBetween('fecha', [$fechai, $fechaf])
                            ->where('idcatalogo', $cuenta);
        $partidas = Partidacierre::select('*')
                                       ->whereBetween('fecha', [$fechai, $fechaf])
                                       ->where('idcatalogo', $cuenta)
                                       ->union($first)
                                       ->get();
        if (is_null($fechai)) {
            $fechai = '2022-01-01';
        }
        $partidas2 = Partidacierre::select('*')
                                       ->where('fecha','<', $fechai)
                                       ->where('idcatalogo', $cuenta)
                                       ->get();
      return view('partidac.partidash',compact('partidas', 'partidas2', 'fechai', 'fechaf', 'cuenta'));
    }

    public function generatePDF6($fechai, $fechaf, $cuenta)

    {
        $first = Partidac::select('*')
                            ->whereBetween('fecha', [$fechai, $fechaf])
                            ->where('idcatalogo', $cuenta);
        $partidas = Partidacierre::select('*')
                                       ->whereBetween('fecha', [$fechai, $fechaf])
                                       ->where('idcatalogo', $cuenta)
                                       ->union($first)
                                       ->get();

        $partidas2 = Partidacierre::select('*')
                                       ->where('fecha','<', $fechai)
                                       ->where('idcatalogo', $cuenta)
                                       ->get();
        

        $pdf = PDF::loadView('partidac.reportePartidash',compact('partidas', 'partidas2'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('partidac.pdf');
        }

    public function Vreproceso()
    {
        $partidacs = Partidac::all();
      return view('partidac.reproceso',compact('partidacs'));
    }



    public function Reproceso() {

        $partidas = Partidacierre::where('estatus2', 5)
                                   ->get();

        foreach ($partidas as $key => $value) {
            $partidac = new Partidac;
            $partidac->idcatalogo = $value->idcatalogo;
            $cuenta2 = $value->idcatalogo;
            $partidac->tipo = $value->tipo;
            $partidac->tipo2 = $value->tipo2;
            $partidac->correlativo = $value->correlativo;
            $partidac->fecha = $value->fecha;
            $partidac->descripcion = $value->descripcion;
            $partidac->concepto = $value->concepto;
            $partidac->saldoInicial = $value->saldoInicial;
            $partidac->debe = $value->debe;
            $debe = $value->debe;
            $partidac->haber = $value->haber;
            $haber = $value->haber;
            $partidac->saldo = $value->saldo;
            $partidac->estatus = $value->estatus;
            $partidac->save();
            

        $cuentaDetalleAfectadav = ContCuentaDetalle::whereCuentadetalle($cuenta2)->first();
        if (empty($cuentaDetalleAfectadav)) {
            $subcuentaAfectada = ContSubcuenta::whereSubcuenta($cuenta2)->first();
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $subcuentaAfectada->debe;
            $shaber = $subcuentaAfectada->haber;
            $subcuentaAfectada->debe = $sdebe + $debe;
            $subcuentaAfectada->haber = $shaber + $haber;
            $subcuentaAfectada->save();

            $subcuentaAfectadas = ContSubcuenta::whereSubcuenta($cuenta2)->first();
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $subcuentaAfectadas->saldoInicial;
            $ssaldos = $subcuentaAfectadas->saldo;
            $sdebes = $subcuentaAfectadas->debe;
            $shabers = $subcuentaAfectadas->haber;
            $concuenta = $subcuentaAfectadas->padre;
            $subcuentaAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $subcuentaAfectadas->save();
        } else {
            $sdebe = 0;
            $shaber = 0;
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            //$cuentaDetalleAfectada = ContCuentaDetalle::find($cuenta2);
            $cuentaDetalleAfectada = ContCuentaDetalle::whereCuentadetalle($cuenta2)->first();
            $sdebe = $cuentaDetalleAfectada->debe;
            $shaber = $cuentaDetalleAfectada->haber;
            $cuentaDetalleAfectada->debe = $sdebe + $debe;
            $cuentaDetalleAfectada->haber = $shaber + $haber;
            $cuentaDetalleAfectada->save();

            $cuentaDetalleAfectadaS = ContCuentaDetalle::whereCuentadetalle($cuenta2)->first();
            $ssaldois = $cuentaDetalleAfectadaS->saldoInicial;
            $ssaldos = $cuentaDetalleAfectadaS->saldo;
            $sdebes = $cuentaDetalleAfectadaS->debe;
            $shabers = $cuentaDetalleAfectadaS->haber;
            $subcuenta = $cuentaDetalleAfectadaS->padre;
            $cuentaDetalleAfectadaS->saldo = ($ssaldois + $sdebes - $shabers);
            $cuentaDetalleAfectadaS->save();

            $subcuentaAfectada = ContSubcuenta::find($subcuenta);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $subcuentaAfectada->debe;
            $shaber = $subcuentaAfectada->haber;
            $subcuentaAfectada->debe = $sdebe + $debe;
            $subcuentaAfectada->haber = $shaber + $haber;
            $subcuentaAfectada->save();

            $subcuentaAfectadas = ContSubcuenta::find($subcuenta);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $subcuentaAfectadas->saldoInicial;
            $ssaldos = $subcuentaAfectadas->saldo;
            $sdebes = $subcuentaAfectadas->debe;
            $shabers = $subcuentaAfectadas->haber;
            $concuenta = $subcuentaAfectadas->padre;
            $subcuentaAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $subcuentaAfectadas->save();
        }
        
            $cuentaAfectada = ContCuenta::find($concuenta);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $cuentaAfectada->debe;
            $shaber = $cuentaAfectada->haber;
            $cuentaAfectada->debe = $sdebe + $debe;
            $cuentaAfectada->haber = $shaber + $haber;
            $cuentaAfectada->save();

            $cuentaAfectadas = ContCuenta::find($concuenta);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $cuentaAfectadas->saldoInicial;
            $ssaldos = $cuentaAfectadas->saldo;
            $sdebes = $cuentaAfectadas->debe;
            $shabers = $cuentaAfectadas->haber;
            $rubro = $cuentaAfectadas->padre;
            $cuentaAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $cuentaAfectadas->save();

            $rubroAfectada = ContRubro::find($rubro);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $rubroAfectada->debe;
            $shaber = $rubroAfectada->haber;
            $rubroAfectada->debe = $sdebe + $debe;
            $rubroAfectada->haber = $shaber + $haber;
            $rubroAfectada->save();

            $rubroAfectadas = ContRubro::find($rubro);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $rubroAfectadas->saldoInicial;
            $ssaldos = $rubroAfectadas->saldo;
            $sdebes = $rubroAfectadas->debe;
            $shabers = $rubroAfectadas->haber;
            $elemento = $rubroAfectadas->padre;
            $rubroAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $rubroAfectadas->save();

            $elementoAfectada = ContElemento::find($elemento);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $elementoAfectada->debe;
            $shaber = $elementoAfectada->haber;
            $elementoAfectada->debe = $sdebe + $debe;
            $elementoAfectada->haber = $shaber + $haber;
            $elementoAfectada->save();

            $elementoAfectadas = ContElemento::find($elemento);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $elementoAfectadas->saldoInicial;
            $ssaldos = $elementoAfectadas->saldo;
            $sdebes = $elementoAfectadas->debe;
            $shabers = $elementoAfectadas->haber;
            $elementoAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $elementoAfectadas->save();

            $value->delete();
        }

        $partidacs = Partidac::all();
        return view('partidac.reproceso',compact('partidacs'));
    }

}


