<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partidac;
use App\Catcuenta;
use App\Prestatario;
use App\Contcuenta;
use App\ContSubcuenta;
use App\ContCuentaDetalle;
use App\ContElemento;
use App\ContRubro;
use Partidac1\http\Request\PartidacRequest;
use RealRashid\SweetAlert\Facades\Alert;
use PDF;
use Carbon\Carbon;
use App\Exports\partidacExport;
use Maatwebsite\Excel\Facades\Excel;

class PartidacController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:Dominios|Crear Dominio|Editar Dominio|Eliminar Dominio', ['only' => ['index','store']]);
         $this->middleware('permission:Dominios', ['only' => ['index']]);
         $this->middleware('permission:Crear Dominio', ['only' => ['create','store']]);
         $this->middleware('permission:Editar Dominio', ['only' => ['edit','update']]);
         $this->middleware('permission:Eliminar Dominio', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $nombre =$request->get('nombre');
        //$partidac = Partidac::orderBy('correlativo','desc')->groupBy('correlativo')->having('correlativo', '>', 0)->get();
        //$partidac = Partidac::select('correlativo')
        //                ->distinct()
        //                ->get();
        $partidac = Partidac::orderBy('id','ASC')->nombre($nombre)->paginate(10);
        $partidacs = Partidac::all();
        return view('partidac.index',compact('partidac', 'partidacs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $catcuentas = Catcuenta::all();
        $contcuentasd = ContCuentaDetalle::all();
        $partidacs = Partidac::select('correlativo')->orderBy('id', 'desc')->first();
        return view('partidac.create',compact('contcuentasd', 'partidacs'));
    }

    public function estadoc()
    {
        //$personas = TableCliente::all();
        //$productos = TableProductos::all();
        //$factura = TableFacturas::all();
        //$venta = TableVentas::all();
        $catcuentas = Catcuenta::all();
        $partidac = Partidac::all();
        $contelementos = ContElemento::all();
        $contrubros = ContRubro::all();
        $contcuentas = ContCuenta::all();
        $contsubcuentas = ContSubcuenta::all();
        $contcuentadetalles = ContCuentaDetalle::all();
        
        return view('partidac.estadoc',compact('catcuentas', 'partidac', 'contelementos', 'contrubros', 'contcuentas', 'contsubcuentas', 'contcuentadetalles'));
    }

    public function estadoco()
    {
        //$personas = TableCliente::all();
        //$productos = TableProductos::all();
        //$factura = TableFacturas::all();
        //$venta = TableVentas::all();
        $catcuentas = Catcuenta::all();
        $partidac = Partidac::all();
        $partidacs = Partidac::all()->unique('correlativo');
        
        return view('partidac.estadoco',compact('catcuentas', 'partidac', 'partidacs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**$this->validate($request,[
            'No_Facturas',
            'Productos',
            'cantidad'
            factura= cliente, fecha, total
            Cliente= id
        ]);
        
        TableVentas::create($request->all());
        return redirect()->route('tableVentas.create')->with('success','Venta guardado con éxito');*/
        $total = 0;
        $estatus = 1;
        $totalf = 0;
        $cant = 0;
        $cont = 0;
        $cuenta = $request->get('idcuenta');
        $desc = $request->get('desc');
        $cantidad = $request->get('cantidad');
        $tipo2 = $request->get('tipo2');
        $debe = $request->get('debe');
        $haber = $request->get('haber');
        while ($cont < count($cuenta)) {
        $cuenta2= trim($cuenta[$cont]);
        $partidac = new Partidac;
        $partidac->idcatalogo = $cuenta2; 
        $partidac->tipo = $request->get('tipo'); 
        $partidac->correlativo = $request->get('correlativo');
        $partidac->fecha = $request->get('fecha');
        $partidac->descripcion = $desc[$cont];
        $partidac->debe = $debe[$cont];
        $partidac->haber = $haber[$cont];
        $partidac->tipo2 = $tipo2[$cont];
        $partidac->estatus = $estatus;
        $partidac->save();
        

        
            $sdebe = 0;
            $shaber = 0;
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            //$cuentaDetalleAfectada = ContCuentaDetalle::find($cuenta2);
            $cuentaDetalleAfectada = ContCuentaDetalle::whereCuentadetalle($cuenta2)->first();
            $sdebe = $cuentaDetalleAfectada->debe;
            $shaber = $cuentaDetalleAfectada->haber;
            $cuentaDetalleAfectada->debe = $sdebe + $debe[$cont];
            $cuentaDetalleAfectada->haber = $shaber + $haber[$cont];
            $cuentaDetalleAfectada->save();

            $cuentaDetalleAfectadaS = ContCuentaDetalle::whereCuentadetalle($cuenta2)->first();
            $subcuenta = $cuentaDetalleAfectadaS->padre;
            $ssaldois = $cuentaDetalleAfectadaS->saldoInicial;
            $ssaldos = $cuentaDetalleAfectadaS->saldo;
            $sdebes = $cuentaDetalleAfectadaS->debe;
            $shabers = $cuentaDetalleAfectadaS->haber;
            $cuentaDetalleAfectadaS->saldo = ($ssaldois + $sdebes - $shabers);
            $cuentaDetalleAfectadaS->save();

            $subcuentaAfectada = ContSubcuenta::find($subcuenta);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $subcuentaAfectada->debe;
            $shaber = $subcuentaAfectada->haber;
            $subcuentaAfectada->debe = $sdebe + $debe[$cont];
            $subcuentaAfectada->haber = $shaber + $haber[$cont];
            $subcuentaAfectada->save();

            $subcuentaAfectadas = ContSubcuenta::find($subcuenta);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $subcuentaAfectadas->saldoInicial;
            $ssaldos = $subcuentaAfectadas->saldo;
            $sdebes = $subcuentaAfectadas->debe;
            $shabers = $subcuentaAfectadas->haber;
            $concuenta = $subcuentaAfectadas->padre;
            $subcuentaAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $subcuentaAfectadas->save();

            $cuentaAfectada = ContCuenta::find($concuenta);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $cuentaAfectada->debe;
            $shaber = $cuentaAfectada->haber;
            $cuentaAfectada->debe = $sdebe + $debe[$cont];
            $cuentaAfectada->haber = $shaber + $haber[$cont];
            $cuentaAfectada->save();

            $cuentaAfectadas = ContCuenta::find($concuenta);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $cuentaAfectadas->saldoInicial;
            $ssaldos = $cuentaAfectadas->saldo;
            $sdebes = $cuentaAfectadas->debe;
            $shabers = $cuentaAfectadas->haber;
            $rubro = $cuentaAfectadas->padre;
            $cuentaAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $cuentaAfectadas->save();

            $rubroAfectada = ContRubro::find($rubro);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $rubroAfectada->debe;
            $shaber = $rubroAfectada->haber;
            $rubroAfectada->debe = $sdebe + $debe[$cont];
            $rubroAfectada->haber = $shaber + $haber[$cont];
            $rubroAfectada->save();

            $rubroAfectadas = ContRubro::find($rubro);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $rubroAfectadas->saldoInicial;
            $ssaldos = $rubroAfectadas->saldo;
            $sdebes = $rubroAfectadas->debe;
            $shabers = $rubroAfectadas->haber;
            $elemento = $rubroAfectadas->padre;
            $rubroAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $rubroAfectadas->save();

            $elementoAfectada = ContElemento::find($elemento);
            $sdebe = 0;
            $shaber = 0;
            $sdebe = $elementoAfectada->debe;
            $shaber = $elementoAfectada->haber;
            $elementoAfectada->debe = $sdebe + $debe[$cont];
            $elementoAfectada->haber = $shaber + $haber[$cont];
            $elementoAfectada->save();

            $elementoAfectadas = ContRubro::find($elemento);
            $ssaldois = 0;
            $ssaldos = 0;
            $sdebes = 0;
            $shabers = 0;
            $ssaldois = $elementoAfectadas->saldoInicial;
            $ssaldos = $elementoAfectadas->saldo;
            $sdebes = $elementoAfectadas->debe;
            $shabers = $elementoAfectadas->haber;
            $elementoAfectadas->saldo = ($ssaldois + $sdebes - $shabers);
            $elementoAfectadas->save();

        
        //$cuentaAfectada = ;

        $cont = $cont + 1;

  }
        return redirect()->route('partidac.index')->with('success','Partida actualizado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $partidac = Partidac::find($id);
      return view('partidac.show',compact('partidac'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partidac = Partidac::find($id);
        return view('partidac.edit',compact('partidac'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
          'id_facturas',
            'id_productos',
            'cantidad'
        ]);
        Partidac::find($id)->update($request->all());
        return redirect()->route('partidac.index')->with('success','Partida actualizado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Partidac::find($id)->delete();
            Alert::success('Partida eliminada con exito');
        return redirect()->route('partidac.index');
    		} catch  (\Illuminate\Database\QueryException $e){
                 Alert::danger('No se Puede eliminar este registro porque esta asociado con otra asignación');
        return redirect()->route('partidac.index');
        }
    }

     public function generatePDF(Request $request)

    {
        //$nombre = $request->get('cuenta');
        //$catcuentas = Catcuenta::orderBy('id','ASC')->nombre($nombre)->paginate(100);
        //$data = ['title' => 'Esta es una página de Prueba'];
        $catcuentas = Catcuenta::all();
        $partidac = Partidac::all();
        $partidacs = Partidac::all()->unique('correlativo');
        
        //$partidacs = Partidac::find($id);
        $date=new Carbon();
        $fecha = $date->format('d-m-Y');

        $pdf = PDF::loadView('partidac.reportePartidac',compact('catcuentas', 'partidac', 'partidacs','fecha'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('partidac.pdf');

    }

    public function exportExcel()

    {
        
        //$nombre = $request->get('cuenta');
        //$catcuentas = Catcuenta::orderBy('id','ASC')->nombre($nombre)->paginate(100);
        //$data = ['title' => 'Esta es una página de Prueba'];
        //$pagos = Pago::all();
        //$prestamo = Prestamo::find($id);
        //$id_prestatario = $prestamo->idprest;
        //$codigoP = $prestamo->codigoPrestamo;
        //$prestatario = Prestatario::find($id_prestatario);
        //$nombrep = $prestatario->pnombre;
        //$apellidop = $prestatario->papellido;
        //$date=new Carbon();
        //$fecha = $date->format('d-m-Y');
        $fecha=date("dmy");

        //$excel = Excel::loadView('prestamo.reportePrestamo',compact('prestamos','fecha','pagos'));
        //$pdf->getDomPDF()->set_option("enable_php", TRUE);
        //return $pdf->stream('prestamo.pdf');
        //$export = ($prestamo, $prestatario);

        return Excel::download(new partidacExport, 'Balance'.$fecha.'.xlsx');
        //return view('export.prestamo',compact('prestamo', 'pagos'));

    }
}
