<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prestamo;
use App\Prestatario;

class DropdownController extends Controller
{
    public function getPrestatarios()
{
    $prestatarios = Prestatario::all();
    return response()->json($prestatarios);
}

public function getPrestamos($prestatario)
{
    $prestamos = Prestamo::where('idprest', $prestatario)->get();
    return response()->json($prestamos);
}
}
