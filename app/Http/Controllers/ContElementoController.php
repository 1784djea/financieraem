<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContElemento;
use App\Contcuenta;
use App\ContSubcuenta;
use App\ContCuentaDetalle;
use Contelemento1\http\Request\ContElementoRequest;
use RealRashid\SweetAlert\Facades\Alert;
use PDF;
use Carbon\Carbon;

class ContElementoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:Dominios|Crear Dominio|Editar Dominio|Eliminar Dominio', ['only' => ['index','store']]);
         $this->middleware('permission:Dominios', ['only' => ['index']]);
         $this->middleware('permission:Crear Dominio', ['only' => ['create','store']]);
         $this->middleware('permission:Editar Dominio', ['only' => ['edit','update']]);
         $this->middleware('permission:Eliminar Dominio', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nombre = $request->get('rubroDesc');
        $contelementoss = ContElemento::orderBy('id','ASC')->nombre($nombre)->paginate(10);

        return view('contcuenta.index',compact('contelementos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contelemento.create');
    }

       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
          'elemento', 
          'subcuenta', 
          'cuentaDetalle', 
          'rubroDesc', 
          'estatus',
          'saldoInicial',
          'debe',
          'haber',
          'saldo'
        ]);
        ContElemento::create($request->all());
        return redirect()->route('contelemento.index');
    }

       /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contelementos = ContElemento::find($id);
      return view('contelemento.show',compact('contelementos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contelementos = ContElemento::find($id);
        return view('contelemento.edit',compact('contelementos'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
          'elemento', 
          'subcuenta', 
          'cuentaDetalle', 
          'rubroDesc', 
          'estatus',
          'saldoInicial',
          'debe',
          'haber',
          'saldo'
        ]);
        ContElemento::find($id)->update($request->all());
        return redirect()->route('contelemento.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       try{
            ContElemento::find($id)->delete();
            

            Alert::success('Elemento eliminado con exito');
        return redirect()->route('contcuenta.index');
            } catch  (\Illuminate\Database\QueryException $e){
                 Alert::danger('No se Puede eliminar este registro porque esta asociado con otros datos');
        return redirect()->route('contcuenta.index');
        }
    }

 

    public function generatePDF(Request $request)

    {
        $nombre = $request->get('cuenta');
        $contcuentas = Contcuenta::orderBy('id','ASC')->nombre($nombre)->paginate(100);
        //$data = ['title' => 'Esta es una página de Prueba'];
        $date=new Carbon();
        $fecha = $date->format('d-m-Y');

        $pdf = PDF::loadView('contcuenta.reporteContcuenta',compact('contcuentas','fecha'));
        $pdf->getDomPDF()->set_option("enable_php", TRUE);
        return $pdf->stream('contcuenta.pdf');

    }


}
