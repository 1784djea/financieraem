<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Contrubro extends Model
{
    use LogsActivity;

    protected $fillable = ['rubro', 'padre', 'hijo', 'tipo', 'rubroDesc', 'estatus', 'estatus2', 'saldoInicial', 'debe', 'haber', 'saldo', 'fechaCierre', 'fechaUltCierre'];
    protected $dates = ['created_at','updated_at'];

    public function scopeNombre($query, $nombre)
    {
        return $query->where('rubroDesc', 'LIKE', "%$nombre%");
    }
 }