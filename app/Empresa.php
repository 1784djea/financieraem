<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Empresa extends Model
{
    use LogsActivity;

    protected $fillable = ['nit', 'nrc', 'nombre', 'descripcion', 'codActividad', 'descActividad', 'nombreComercial', 'direccion', 'departamento', 'municipio', 'telefono', 'correo', 'tipoEstablecimiento', 'codPuntoVenta', 'codEstable', 'estado'];
    protected $dates = ['created_at','updated_at'];

    public function scopeNombre($query, $nombre)
    {
        return $query->where('nombre', 'LIKE', "%$nombre%");
    }
 }