<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Pago extends Model
{

  use LogsActivity;
  
    protected $fillable = ['idprest', 'idprest2', 'referencia', 'fechadepago', 'cantidad', 'intereses', 'capitalpagado', 'diasintereses', 'comentario'];
    protected $dates = ['created_at','updated_at'];

    public function scopeNombre($query, $nombre)
	{
		return $query->join('prestatarios', 'pagos.idprest', '=', 'prestatarios.id')
                 ->select('pagos.*', 'prestatarios.pnombre', 'prestatarios.snombre', 'prestatarios.papellido', 'prestatarios.sapellido')
                 ->where('prestatarios.pnombre', 'LIKE', "%$nombre%")
                 ->orWhere('prestatarios.snombre', 'LIKE', "%$nombre%")
                 ->orWhere('prestatarios.papellido', 'LIKE', "%$nombre%");
	}
}
