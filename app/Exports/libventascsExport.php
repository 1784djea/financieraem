<?php

namespace App\Exports;

use App\Libventascs;
use App\Contribuyentes;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class LibventascsExport implements FromView
{
     private $libventascs;

     public function __construct($libventascs)
    {
        $this->libventascs = $libventascs;
    }
	
    public function view(): View
    {
        return view('libventascs.reporteLibventascsexcel', ['libventasc' => Libventascs::all(),'contribuyentes' => Contribuyentes::all()
    ])->with('libventascs', $this->libventascs);
    }
}
