<?php

namespace App\Exports;

use App\Prestamo;
use App\Pago;
use App\Prestatario;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class prestamoExport implements FromView
{
	
    public function view(): View
    {
        return view('reportes.reportePrestamoexcel', ['prestamos' => Prestamo::all(), 'pagos' => Pago::all(), 'prestatarios' => Prestatario::all()
    ]);
    }
}
