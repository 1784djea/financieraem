<?php

namespace App\Exports;

use App\Prestamo;
use App\Pago;
use App\Prestatario;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class prestamoExport implements FromView
{

 private $prestamo;
 private $prestatario;

    public function __construct($prestamo, $prestatario)
    {
        $this->prestamo = $prestamo;
        $this->prestatario = $prestatario;
    }
    public function view(): View
    {
        return view('prestamo.reportePrestamo3', ['pagos' => Pago::all(), 'prestatario' => Prestatario::all()
    ])->with('prestamo', $this->prestamo)->with('prestatario', $this->prestatario);
    }
}
