<?php

namespace App\Exports;

use App\LibCompras;
use App\Contribuyentes;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class libcomprasExport implements FromView
{
     private $libcompras;

     public function __construct($libcompras)
    {
        $this->libcompras = $libcompras;
    }
	
    public function view(): View
    {
        return view('libcompras.reporteLibcomprasexcel', ['libcompra' => Libcompras::all(),'contribuyentes' => Contribuyentes::all()
    ])->with('libcompras', $this->libcompras);
    }
}
