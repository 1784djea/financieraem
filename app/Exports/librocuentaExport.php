<?php

namespace App\Exports;

use App\Partidac;
use App\Catcuenta;
use App\ContElemento;
use App\ContRubro;
use App\Contcuenta;
use App\ContSubcuenta;
use App\ContCuentaDetalle;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class librocuentaExport implements FromView
{
    private $partidas;
    private $partidas2;

    public function __construct($partidas, $partidas2)
    {
        $this->partidas = $partidas;
        $this->partidas2 = $partidas2;
    }
	
    public function view(): View
    {
        return view('partidac.reportePartidashexcel')->with('partidas', $this->partidas)->with('partidas2', $this->partidas2);
    }
}
