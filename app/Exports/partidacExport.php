<?php

namespace App\Exports;

use App\Partidac;
use App\Catcuenta;
use App\ContElemento;
use App\ContRubro;
use App\Contcuenta;
use App\ContSubcuenta;
use App\ContCuentaDetalle;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class partidacExport implements FromView
{
	
    public function view(): View
    {
        return view('partidac.estadocexcel', ['contelementos' => ContElemento::all(), 'contrubros' => ContRubro::all(), 'contcuentas' => Contcuenta::all(), 'contsubcuentas' => ContSubcuenta::all(), 'contcuentadetalles' => ContCuentaDetalle::all()
    ]);
    }
}
