<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Factura extends Model
{
    use LogsActivity;

    protected $fillable = ['idprestatario', 'idprestamo', 'descripcion', 'fecha', 'json', 'factura', 'procesado',
            'facturado', 'estado'];
    protected $dates = ['created_at','updated_at'];

    public function scopeNombre($query, $nombre)
    {
        return $query->where('descripcion', 'LIKE', "%$nombre%");
    }
 }