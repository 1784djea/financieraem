@extends('layouts.app')
@section('content')
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
    	<h3 style="text-align:center"> Actualizacion de la Subcuenta</h3>
    	<br>
      {{ Form::model($contcuentas,['route'=>['contcuenta.update2',$contcuentas->id],'method'=>'PATCH']) }}
      @include('contcuenta.form_master_edit2')
      {{ Form::close() }}
    </div>
  </div>
@endsection