@extends('layout')
@section('container')
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <h3 style="text-align:center"> DATOS DE LA CUENTA DETALLE</h3>
      <br>
      {{ Form::model($contsubcuentas,['route'=>['contcuenta.store3'], 'method'=>'POST']) }}
        @include('contcuenta.form_master3')
      {{ form::close() }}
    </div>
  </div>
@endsection
