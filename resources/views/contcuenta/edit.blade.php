@extends('layouts.app')
@section('content')
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
    	<h3 style="text-align:center"> Actualizacion de la cuenta</h3>
    	<br>
      {{ Form::model($contcuentas,['route'=>['contcuenta.update',$contcuentas->id],'method'=>'PATCH']) }}
      @include('contcuenta.form_master_edit')
      {{ Form::close() }}
    </div>
  </div>
@endsection