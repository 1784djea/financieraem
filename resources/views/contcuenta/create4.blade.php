@extends('layout')
@section('container')
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <h3 style="text-align:center"> DATOS DEL ELEMENTO</h3>
      <br>
      {{ Form::model($contelementos,['route'=>['contcuenta.store4'], 'method'=>'POST']) }}
        @include('contcuenta.form_master4')
      {{ form::close() }}
    </div>
  </div>
@endsection
