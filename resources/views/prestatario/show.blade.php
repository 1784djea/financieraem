@extends('layout')

@section('container')
<div class="row"  >
    <div class="col-lg-12 margin-tb">
        <div class="pull-left ">
            <h3 > Datos del prestatario</h3>
            <br>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Nombre : </strong>
            {{$prestatarios->pnombre}} {{$prestatarios->snombre}} {{$prestatarios->papellido}} {{$prestatarios->sapellido}}
        </div>
    </div>
</div>

<table class="table table-striped" style="text-align:center" >
    <tr>
      <th with="80px">No</th>
      <th style="text-align:center">Codigo</th>
      <th style="text-align:center">Tasa</th>
      <th style="text-align:center">Tiempo</th>
      <th style="text-align:center">Cantidad</th>
      <th style="text-align:center">Cuota</th>
      <th style="text-align:center">Fecha inicio</th>
      <th style="text-align:center">Estado</th>
      <th style="text-align:center">Acciones</th>
    </tr>
    <?php $no=1; ?>
    @foreach ($prestamos as $key => $value)
    @if($prestatarios->id == $value->idprest)
    <tr>
        <td>{{$no++}}</td>
        <td>{{ $value->codigoPrestamo }}</td>
        <td>{{ $value->tasa }}</td>
        <td>{{ $value->tiempo }}</td>
        <td>{{ $value->cantidad }}</td>
        <td>{{ $value->pago }}</td>
        <td>{{ $value->fechaInicio }}</td>
        <td>{{ $value->estatus }}</td>
        <td>
          <a class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{route('prestatario.show',$value->id)}}">
              <i class="glyphicon glyphicon-list-alt">Detalles</i></a>

          @can('Editar Usuarios')
          <a class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Editar" href="{{route('prestatario.edit',$value->id)}}">
              <i class="glyphicon glyphicon-pencil">Editar</i></a>
          @endcan

          @can('Eliminar Usuarios')
            {!! Form::open(['method' => 'DELETE','route' => ['prestatario.destroy', $value->id],'style'=>'display:inline', 'class'=>'formulario-eliminar']) !!}
              <button type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar" style="display: inline;" class="btn btn-danger btn-sm">Eliminar<i class="glyphicon glyphicon-trash" ></i></button>
            {!! Form::close() !!}
          @endcan
        </td>
      </tr>
      @endif
    @endforeach
  </table>
            <br/>
            <a class="btn btn-primary" href="{{ route('dominio.index') }}"> <i class="glyphicon glyphicon-arrow-left"> Regresar</i></a>
    </div>
@endsection

