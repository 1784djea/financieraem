@extends ('layout')
@section('header')
<header ><h2 style="color: white; font-family: sans-serif; font-size: 58px; text-align: center;">Gestion de elementos</h2>
  </header>
@endsection
@section('container')
 <br>
  @if ($errors->any())
   <div class="alert alert-danger">
  
          <p>Debe ingresar datos válidos</p>
     
   </div>
  @endif

  <div class="row"> 
       <div class="col-md-8">
        @can('Dominios')
        <a href="{{route('contcuenta.create')}}" class="btn btn-success btn-lg">
            <i class="glyphicon glyphicon-plus"> Nueva Cuenta</i>
        </a>
        @endcan
        </div>
        <div class="col-md-4">
         {!! Form::open(['route'=>'especimen.index', 'method'=>'GET', 'class'=>'navbar-form pull-right', 'role'=>'search'])!!}
        <div class="input-group"> 
           {!! Form::text('codigoEspecimen', null, ['class'=>'form-control', 'placeholder'=>'Buscar'])!!}
           <button type="submit" class="btn-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="Buscar">Buscar</button>
        {!! Form::close()!!}
        </div>
        <br>
      
      </div>
      <div class="col-md-12" style="text-align:right;">
          <a href="contcuentaPrueba.pdf" class="btn-info btn-sm" target="_blank" >Generar PDF</a>
      </div> 
      </div>
 
      <br>
  <table class="table table-striped" style="text-align:center" >
    <tr>
      <th with="80px">No</th>
      <th style="text-align:center">Cuenta</th>
      <th style="text-align:center">Subcuenta</th>
      <th style="text-align:center">Cuenta Detalle</th>
      <th style="text-align:center">rubroDesc</th>
      <th style="text-align:center">Acciones</th>
    </tr>
    <?php $no=1;
     ?>
    @foreach ($contcuentas as $key => $value)
    <tr>
        <td>{{$no++}}</td>
        <td>{{ $value->cuenta }}</td>
        <td>{{ $value->subcuenta }}</td>
        <td>{{ $value->cuentaDetalle }}</td>
        <td>{{ $value->rubroDesc }}</td>
        <td>
          <a class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{route('contcuenta.show',$value->id)}}">
              <i class="glyphicon glyphicon-list-alt">Detalles</i></a>
          @can('Editar Dominio')
          <a class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Editar" href="{{route('contcuenta.edit',$value->id)}}">
              <i class="glyphicon glyphicon-pencil">Editar</i></a>
          @endcan
          @can('Eliminar Dominio')
            {!! Form::open(['method' => 'DELETE','route' => ['contcuenta.destroy', $value->id],'style'=>'display:inline', 'class'=>'formulario-eliminar']) !!}
              <button type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar" style="display: inline;" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash" >Eliminar</i></button>
            {!! Form::close() !!}
          @endcan
        </td>
      </tr>


      @foreach ($contsubcuentas as $key => $value2)
      @if($value->id == $value2->padre)
    <tr>
        <td>{{$no++}}</td>
        <td>{{ $value2->cuenta }}</td>
        <td>{{ $value2->subcuenta }}</td>
        <td>{{ $value2->cuentaDetalle }}</td>
        <td>{{ $value2->rubroDesc }}</td>
        <td>
          <a class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{route('contcuenta.show',$value2->id)}}">
              <i class="glyphicon glyphicon-list-alt">Detalles</i></a>
          @can('Editar Dominio')
          <a class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Editar" href="{{route('contcuenta.edit2',$value2->id)}}">
              <i class="glyphicon glyphicon-pencil">Editar</i></a>
          @endcan
          @can('Eliminar Dominio')
            {!! Form::open(['method' => 'DELETE','route' => ['contcuenta.destroy2', $value2->id],'style'=>'display:inline', 'class'=>'formulario-eliminar']) !!}
              <button type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar" style="display: inline;" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash" >Eliminar</i></button>
            {!! Form::close() !!}
          @endcan
        </td>
      </tr>
      


      @foreach ($contcuentadetalles as $key => $value3)
      @if($value2->id == $value3->padre)
    <tr>
        <td>{{$no++}}</td>
        <td>{{ $value3->cuenta }}</td>
        <td>{{ $value3->subcuenta }}</td>
        <td>{{ $value3->cuentaDetalle }}</td>
        <td>{{ $value3->rubroDesc }}</td>
        <td>
          <a class="btn btn-info btn-sm" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{route('contcuenta.show',$value3->id)}}">
              <i class="glyphicon glyphicon-list-alt">Detalles</i></a>
          @can('Editar Dominio')
          <a class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Editar" href="{{route('contcuenta.edit3',$value3->id)}}">
              <i class="glyphicon glyphicon-pencil">Editar</i></a>
          @endcan
          @can('Eliminar Dominio')
            {!! Form::open(['method' => 'DELETE','route' => ['contcuenta.destroy3', $value3->id],'style'=>'display:inline', 'class'=>'formulario-eliminar']) !!}
              <button type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar" style="display: inline;" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash" >Eliminar</i></button>
            {!! Form::close() !!}
          @endcan
        </td>
      </tr>
      @endif
    @endforeach
    @endif


    @endforeach


    @endforeach
  </table>
  {!!$contcuentas->render()!!}
 <div class="text-center">
    <a class="btn btn-primary" href="{{ url('/gestion') }}">Regresar</a>
  </div>

  <!--Script para mostrar formulario y Alerta confirmar Guardar con ajax-->
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script type="text/javascript">
      $('.agregar').hide();
       function mostrarFormulario(){
        $('.agregar').show();
       }
$('.agregar').submit(function(e){
     e.preventDefault();Swal.fire({
  title: '¿Está seguro de guardar esta cuenta?',
  showDenyButton: true,
  //showCancelButton: true,
  confirmButtonText: `Guardar`,
  denyButtonText: `Cancelar`,
})
     .then((result) => {
    if (result.isConfirmed) {
     this.submit();
    }
})
});
</script>
<!--Script para Alerta con ajax-->

 <script type="text/javascript">
$('.formulario-eliminar').submit(function(e){
     e.preventDefault();
       Swal.fire({
    title: '¿Está seguro de eliminar permanentemente esta cuenta?',
    /*text: "You won't be able to revert this!",*/
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Eliminar',
    cancelButtonText: 'Cancelar'

  }).then((result) => {
    if (result.isConfirmed) {
     this.submit();
    }
})
});

    </script>
@endsection
