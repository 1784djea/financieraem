@extends('layout')
@section('container')
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
    	<h3 style="text-align:center"> Actualizacion del prestamo</h3>
    	<br>
      {{ Form::model($prestamos,['route'=>['prestamo.update',$prestamos->id],'method'=>'PATCH']) }}
      @include('prestamo.form_master')
      {{ Form::close() }}
    </div>
  </div>
@endsection