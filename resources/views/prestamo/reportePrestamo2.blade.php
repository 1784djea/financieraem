<!DOCTYPE html>
<html>
<!-- Latest compiled and minified CSS -->
<link href="{{ public_path()}}/css/reportes.css" rel="stylesheet">


<head>

    <title>Prestamo{{ $prestamos->codigoPrestamo}}.pdf</title>

</head>

<body>

<div class="container-fluid">
    <div class="header">
                                <h5>Fecha: <?=date("d/m/y") ?></h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <br>
                                <h5>Reporte del prestamo: {{ $prestamos->codigoPrestamo}}</h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <br>
                                <h5>Nombre del prestatario: {{ $prestatario->pnombre}} {{ $prestatario->snombre}} {{ $prestatario->papellido}} {{ $prestatario->sapellido}}</h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <br>
                                <h5>Id del prestamo: {{$prestamos->id}}</h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>

<div class="row"  >
    <div class="col-lg-12 margin-tb">
        <div class="pull-left ">
            <h3 > Detalle del prestamo</h3>
            <br>
        </div>
    </div>
</div>

<?php 
$idP = $prestamos->codigoPrestamo;
$idprestamo = $prestamos->id;
$prestamocancelado = 0;
$prestamocancelado = (float)$prestamos->pendpago;
$importe = $prestamos->cantidad;
$interest = $prestamos->tasa;
$interest2 = $prestamos->tasa;
$anos = $prestamos->tiempo;
$variable = "Sin IVA";

if ($prestamos->coniva == '1') {
    $interest2 = $interest/1.13;
    $interest2 = round($interest2*100)/100;
    $variable = "Con IVA";

}

 ?>


    <div style="float:left; width:90%">

<?php


    $deuda=$importe;
    $deuda2=$importe;
    $anos=$anos;
    $anos2=$anos;
    $interes=$interest;
    $totalint=0;
 
    // hacemos los calculos...
    $interes=($interes/100);
    $m=($interes*(pow((1+$interes),($anos*12)))*$deuda)/((pow((1+$interes),($anos*12)))-1);
    //$m=($deuda*$interes*(pow((1+$interes),($anos*12))))/((pow((1+$interes),($anos*12)))-1);
    $m = round($m, 2);
 
    echo "Identificador: ".$idP." ";
    echo "<br>Capital Inicial: $".number_format($deuda,2,".",",")." ";
    echo "<br>Tasa de interes: ".number_format($interest2,2,".",",")." %" . " "."$variable";
    echo "<br>Duracion en años: ".number_format($anos,2,".",",")." ";
    echo "<br>Cuota a pagar mensualmente: $".number_format($m,2,".",",")." ";
    ?>

    <table border="1" cellpadding="7" cellspacing="0">
        <tr>
            <th>N°</th>
            <th>Referencia</th>
            <th>Fecha de pago</th>
            <th>Pago</th>
            <th>Intereses</th>
            <th>Amortización</th>
            <th>Capital<br> Pendiente</th>
        </tr>
        <?php 
        $no=1;
        $totalint = 0;
        $tintereses = 0;
        $tcapitalpagado = 0;
        $capitalnuevo = 0;
        $capitalnuevo = ($prestamos->cantidad) * 1;
        //$capitalnuevo = floatval($capitalnuevos);


         ?>
    @foreach ($pagos as $key => $value)
    @if($value->estatus != '0')
    @if($value->idprest2 == $idprestamo)
    <?php 

    $tintereses = $value->intereses;
    $tintereses = round($tintereses, 2);
    $stintereses = number_format($value->intereses,2,".",",");
    $tcapitalpagado = $value->capitalpagado;
    $tcapitalpagado = round($tcapitalpagado, 2);
    $stcapitalpagado = number_format($tcapitalpagado,2,".",",");
    $totalint = $totalint + $tintereses;
    $capitalnuevo = $capitalnuevo - $tcapitalpagado;
    $capitalnuevo = round($capitalnuevo, 2);
    //$capitalnuevo = floatval($capitalnuevo);
    $scapitalnuevo = number_format($capitalnuevo,2,".",",");
    //$capitalnuevo = number_format((int)$capitalnuevo,2,".",",");
    $pago = $value->cantidad;
    $spago = number_format($pago,2,".",",");

    $fechapago = $value->fechadepago;
    $newDate = date("d/m/Y", strtotime($fechapago));
    ?>
    <tr>
        @if($capitalnuevo>0.05)
        <td>{{$no++}}</td>
        <td>{{ $value->referencia }}</td>
        <td>{{ $newDate }}</td>
        <td>${{ $spago }}</td>
        <td>${{ $stintereses }}</td>
        <td>${{ $stcapitalpagado }}</td>
        <td>${{ $scapitalnuevo }}</td>
        @endif
        @if($capitalnuevo<=0.05)
        <td>{{$no++}}</td>
        <td>{{ $value->referencia }}</td>
        <td>{{ $newDate }}</td>
        <td>${{ $spago }}</td>
        <td>${{ $stintereses }}</td>
        <td>${{ $stcapitalpagado }}</td>
        <td>${{ 0.00 }}</td>
        @endif
      </tr>
      @endif
      @endif
    @endforeach
    </table>
    Pago total de intereses : <?php echo number_format($totalint,2,".",",")?> $

    </div>

    <div class="footer">
                    <?php date_default_timezone_set('America/El_Salvador');?>
                    <p>EM&Asociados/<?=date("d/m/y") ?> <?=date('g:ia');?></p>
    </div>
    </div>

        <script type="text/php">
            if (isset($pdf)) {
                $text = "Página {PAGE_NUM} / {PAGE_COUNT}";
                $size = 10;
                $font = $fontMetrics->getFont("Verdana");
                $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
                $x = 250;
                $y = 10;;
                $pdf->page_text($x, $y, $text, $font, $size);
            }
        </script>

    </body>

</html>