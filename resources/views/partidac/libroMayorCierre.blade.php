@extends ('layout')
@section('container')
  <div class="row">
    <div class ="col-sm-12">
      <div class="full.right">
      <h2>EM Y ASOSCIADOS, S.A. DE C.V.</h2>
      <h2>** LIBRO MAYOR**</h2>
      <h2>N.I.T. 0618-160722-101-0</h2>
      <br>
      </div>
    </div>
  </div>
  @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
  @endif
   @if ($message = Session::get('danger'))
      <div class="alert alert-danger">
          <p>{{ $message }}</p>
      </div>
  @endif
  <div>
        <a class="btn btn-info btn-lg" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{route('libroMayorpdf',$num)}}">
              <i class="glyphicon glyphicon-list-alt">pdf</i></a>
        <a href="{{route('excelPartidacr')}}" class="btn btn-success btn-lg">
            <i class="glyphicon glyphicon-plus"> Excel</i>
        </a>
      </div>
      <br>
  <table class="table table-striped" style="text-align:center" >
    <tr>
      <th style="text-align:center">N° Partida</th>
      <th style="text-align:center">Fecha</th>
      <th style="text-align:center">Cuenta</th>
      <th style="text-align:center">Descripción</th>
      <th style="text-align:center">Saldo Inicial</th>
      <th style="text-align:center">Debe</th>
      <th style="text-align:center">Haber</th>
      <th style="text-align:center">Saldo Final</th>
    </tr>
    <?php $no=1;
    $debes = 0;
    $debet = 0;
    $habers = 0;
    $habert = 0;
    $aux = 0;
    $cor = "0";
     ?>

    @foreach ($partidac2 as $key => $value2)
    <?php
    $debet = 0;
    $habert = 0;
    $saldoinic = 0;
    $saldofina = 0;
     ?>

     @foreach ($balance as $key => $value3)
        @if($value3->cuenta==$value2->idcatalogo)
        <?php
          $saldoinic = $value3->saldoInicial;
          $saldoinicp = $saldoinic;
          $saldofina = $value3->saldoInicial;
          $saldofinap = $saldofina;

          $saldoinicp = number_format($saldoinicp,2,".",",");
          $saldofinap = number_format($saldofinap,2,".",",");
         ?>
        <tr>
          <td colspan="3">Cuenta {{ $value3->cuenta }} {{ $value3->rubroDesc }}</td>
          <td>Saldo Anterior</td>
          <td>{{ $saldoinicp }}</td>
          <td></td>
          <td></td>
          <td>{{ $saldofinap }}</td>
        </tr>
        @endif
        @endforeach

    @foreach ($partidac as $key => $value)

        @if($value2->idcatalogo==$value->idcatalogo)

        

        <?php 
            $debes = $value->debe;
            $debet = $debet + $debes;
            $debetp = $debet;
            $habers = $value->haber;
            $habert = $habers + $habert;
            $habertp = $habert;
            $status = $value->estatus2;
            $correlativo = $value->correlativo;
            $saldofina = $saldofina + $debes - $habers;
            $saldofinapp = $saldofina;

            $debes = number_format($debes,2,".",",");
            $habers = number_format($habers,2,".",",");
            $debetp = number_format($debetp,2,".",",");
            $habertp = number_format($habertp,2,".",",");
            $saldofinapp = number_format($saldofinapp,2,".",",");

            if ($debes == 0.00) {
              $debes = '';
            } 
            if ($habers == 0.00) {
              $habers = '';
            } 
            
     ?>
     <tr>
        <td>{{ $value->correlativo }}</td>
        <td>{{ $value->fecha }}</td>
        <td>{{ $value->idcatalogo }}</td>
        <td>{{ $value->descripcion }}</td>
        <td>{{ $value->saldoInicial }}</td>
        <td>{{ $debes }}</td>
        <td>{{ $habers }}</td>
        <td>{{ $saldofinapp }}</td>
      </tr>
        @endif
       @endforeach

    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td>Saldo Final</td>
        <td>{{ $saldoinicp }}</td>
        <td>{{ $debetp }}</td>
        <td>{{ $habertp }}</td>
        <td>{{ $saldofinapp }}</td>
      </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>



      
    @endforeach
  </table>
 <div class="text-center">
    <a class="btn btn-primary" href="{{ url('/gestion') }}">Regresar</a>
  </div>
@endsection
