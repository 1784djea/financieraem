@extends ('layout')
@section('container')
  <div class="row">
    <div class ="col-sm-12">
      <div class="full.right">
      <h2>BALANCE DE CUENTAS</h2>
      <br>
      </div>
    </div>
  </div>
  @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
  @endif
   @if ($message = Session::get('danger'))
      <div class="alert alert-danger">
          <p>{{ $message }}</p>
      </div>
  @endif
     
      <br>
  <table class="table table-striped" style="text-align:center" >
    <tr>
      <th style="text-align:center">Codigo</th>
      <th style="text-align:center">Nombre</th>
      <th style="text-align:center">Saldo inicial</th>
      <th style="text-align:center">Debe</th>
      <th style="text-align:center">Haber</th>
      <th style="text-align:center">Saldo</th>
      <th style="text-align:center">Periodo</th>
    </tr>
    <?php $no=1;
     ?>
    @foreach ($partidac as $key => $value)

    <?php 
    $saldoie = $value->saldoInicial;
    $debee = $value->debe;
    $habere = $value->haber;
    $saldofe = $value->saldo;
    $saldoie = number_format($saldoie,2,".",",");
    $debee = number_format($debee,2,".",",");
    $habere = number_format($habere,2,".",",");
    $saldofe = number_format($saldofe,2,".",",");
     ?>

    <tr>
        <td align="left">{{ $value->cuenta }}</td>
        <td align="left">{{ $value->rubroDesc }}</td>
        <td align="right">{{ $saldoie }}</td>
        <td align="right">{{ $debee }}</td>
        <td align="right">{{ $habere }}</td>
        <td align="right">{{ $saldofe }}</td>
        <td align="right">{{ $value->periodo }}</td>

      </tr>
      
    @endforeach
  </table>
 <div class="text-center">
    <a class="btn btn-primary" href="{{ url('/gestion') }}">Regresar</a>
  </div>
@endsection
