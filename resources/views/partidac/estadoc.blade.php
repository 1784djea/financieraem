@extends ('layout')
@section('container')
  <div class="row">
    <div class ="col-sm-12">
      <div class="full.right">
      <h2>BALANCE DE CUENTAS</h2>
      <br>
      </div>
    </div>
  </div>
  @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
  @endif
   @if ($message = Session::get('danger'))
      <div class="alert alert-danger">
          <p>{{ $message }}</p>
      </div>
  @endif
      <div>
        <a href="{{route('reportePartidac')}}" class="btn btn-success btn-lg">
            <i class="glyphicon glyphicon-plus"> PDF</i>
        </a>
        <a href="{{route('excelPartidac')}}" class="btn btn-success btn-lg">
            <i class="glyphicon glyphicon-plus"> Excel</i>
        </a>
      </div>
      <br>
  <table class="table table-striped" style="text-align:center" >
    <tr>
      <th style="text-align:center">Codigo</th>
      <th style="text-align:center">Nombre</th>
      <th style="text-align:center">Saldo inicial</th>
      <th style="text-align:center">Debe</th>
      <th style="text-align:center">Haber</th>
      <th style="text-align:center">Saldo final</th>
    </tr>
    <?php $no=1;
     ?>
    @foreach ($contelementos as $key => $value)
    
    <?php 
    $saldoie = $value->saldoInicial;
    $debee = $value->debe;
    $habere = $value->haber;
    $saldofe = $value->saldo;
    $saldoie = number_format($saldoie,2,".",",");
    $debee = number_format($debee,2,".",",");
    $habere = number_format($habere,2,".",",");
    $saldofe = number_format($saldofe,2,".",",");
     ?>
    <tr>
        <td align="left">{{ $value->elemento }}</td>
        <td align="left">{{ $value->rubroDesc }}</td>
        <td align="right">{{ $saldoie }}</td>
        <td align="right">{{ $debee }}</td>
        <td align="right">{{ $habere }}</td>
        <td align="right">{{ $saldofe }}</td>

      </tr>
      @foreach ($contrubros as $key => $value2)
      @if($value->id == $value2->padre)
      <?php 
    $saldoir = $value2->saldoInicial;
    $deber = $value2->debe;
    $haberr = $value2->haber;
    $saldofr = $value2->saldo;
    $saldoir = number_format($saldoir,2,".",",");
    $deber = number_format($deber,2,".",",");
    $haberr = number_format($haberr,2,".",",");
    $saldofr = number_format($saldofr,2,".",",");
     ?>
      <tr>
        <td align="left">{{ $value2->rubro }}</td>
        <td align="left">{{ $value2->rubroDesc }}</td>
        <td align="right">{{ $saldoir }}</td>
        <td align="right">{{ $deber }}</td>
        <td align="right">{{ $haberr }}</td>
        <td align="right">{{ $saldofr }}</td>
      </tr>
      @foreach ($contcuentas as $key => $value3)
      @if($value2->id == $value3->padre)
      <?php 
    $saldoic = $value->saldoInicial;
    $debec = $value3->debe;
    $haberc = $value3->haber;
    $saldofc = $value3->saldo;
    $saldoic = number_format($saldoic,2,".",",");
    $debec = number_format($debec,2,".",",");
    $haberc = number_format($haberc,2,".",",");
    $saldofc = number_format($saldofc,2,".",",");
     ?>
      <tr>
        <td align="left">{{ $value3->cuenta }}</td>
        <td align="left">{{ $value3->rubroDesc }}</td>
        <td align="right">{{ $saldoic }}</td>
        <td align="right">{{ $debec }}</td>
        <td align="right">{{ $haberc }}</td>
        <td align="right">{{ $saldofc }}</td>
      </tr>
      @foreach ($contsubcuentas as $key => $value4)
      @if($value3->id == $value4->padre)
      <?php 
    $saldois = $value4->saldoInicial;
    $debes = $value4->debe;
    $habers = $value4->haber;
    $saldofs = $value4->saldo;
    $saldois = number_format($saldois,2,".",",");
    $debes = number_format($debes,2,".",",");
    $habers = number_format($habers,2,".",",");
    $saldofs = number_format($saldofs,2,".",",");
     ?>
      <tr>
        <td align="left">{{ $value4->subcuenta }}</td>
        <td align="left">{{ $value4->rubroDesc }}</td>
        <td align="right">{{ $saldois }}</td>
        <td align="right">{{ $debes }}</td>
        <td align="right">{{ $habers }}</td>
        <td align="right">{{ $saldofs }}</td>
      </tr>
      @foreach ($contcuentadetalles as $key => $value5)
      @if($value4->id == $value5->padre)
      <?php 
    $saldoid = $value5->saldoInicial;
    $debed = $value5->debe;
    $haberd = $value5->haber;
    $saldofd = $value5->saldo;
    $saldoid = number_format($saldoid,2,".",",");
    $debed = number_format($debed,2,".",",");
    $haberd = number_format($haberd,2,".",",");
    $saldofd = number_format($saldofd,2,".",",");
     ?>
      <tr>
        <td align="left">{{ $value5->cuentaDetalle }}</td>
        <td align="left">{{ $value5->rubroDesc }}</td>
        <td align="right">{{ $saldoid }}</td>
        <td align="right">{{ $debed }}</td>
        <td align="right">{{ $haberd }}</td>
        <td align="right">{{ $saldofd }}</td>
      </tr>
      @endif
    @endforeach
    @endif
    @endforeach
    @endif
    @endforeach
    @endif
    @endforeach
    @endforeach
  </table>
 <div class="text-center">
    <a class="btn btn-primary" href="{{ url('/gestion') }}">Regresar</a>
  </div>
@endsection
