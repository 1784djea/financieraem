@extends ('layout')
@section('header')
<header style="background-image: url('startbootstrap-clean-blog-gh-pages/assets/img/titulo2.jpg'); opacity: 0.8;"><h2 style="color: black; font-family: sans-serif; font-size: 58px; text-align: center;">Gestion de partidas contables cerradas</h2>
  </header>
@endsection
@section('container')
 <br>
  
  <div class="row">
     <div>
        {!! Form::open(['route'=>'gestionCierres', 'method'=>'GET', 'class'=>'navbar-form pull-right', 'role'=>'search'])!!}
        <div class="input-group"> 
            {!! Form::text('nombre', null, ['class'=>'form-control', 'placeholder'=>'Buscar'])!!}
        </div>
         <button type="submit" class="glyphicon glyphicon-search btn-sm" data-toggle="tooltip" data-placement="top" title="Buscar"></button>
        {!! Form::close()!!}
      </div>
      <br>
  </div>
 
      <br>
 <table class="table table-striped" style="text-align:center" >
    <tr>
      <th style="text-align:center">No</th>
      <th style="text-align:center">Periodo</th>
      <th style="text-align:center">Año</th>
      <th style="text-align:center">Momento</th>
      <th style="text-align:center">Fecha</th>
      <th style="text-align:center">Accion</th>
    </tr>
    <?php $no=1;
    $debes = 0;
    $debet = 0;
    $habers = 0;
    $habert = 0;
    $aux = 0;
    $cor = "0";
     ?>

    @foreach ($cierreauxs as $key => $value)
    @if($value->estatus2=='pre')


    <tr>
        <td>{{ $no++ }}</td>
        <td>{{ $value->periodo }}</td>
        <td>{{ $value->anio }}</td>
        <td>{{ $value->estatus2 }}</td>
        <td>{{ $value->fechaCierre }}</td>
        <td>
    @foreach ($partidac2 as $key => $value2)
    @if($value->id == $value2->estatus2)

          <a class="btn btn-info btn-lg" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{route('partidasCierres',$value->id)}}">
              <i class="glyphicon glyphicon-list-alt">Partidas</i></a>
            <a class="btn btn-info btn-lg" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{route('libroMayor',$value->id)}}">
              <i class="glyphicon glyphicon-list-alt">Libro Mayor</i></a>
              @endif
              @endforeach
            <a class="btn btn-info btn-lg" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{route('balanceCierres',$value->id)}}">
              <i class="glyphicon glyphicon-list-alt">Balance</i></a>
          
        </td>
      </tr>
    @endif
    @endforeach
  </table>
 <div class="text-center">
    <a class="btn btn-primary" href="{{ url('/gestion') }}">Regresar</a>
  </div>

  <!--Script para mostrar formulario y Alerta confirmar Guardar con ajax-->
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script type="text/javascript">
      $('.agregar').hide();
       function mostrarFormulario(){
        $('.agregar').show();
       }
$('.agregar').submit(function(e){
     e.preventDefault();Swal.fire({
  title: '¿Está seguro de guardar esta partida?',
  showDenyButton: true,
  //showCancelButton: true,
  confirmButtonText: `Guardar`,
  denyButtonText: `Cancelar`,
})
     .then((result) => {
    if (result.isConfirmed) {
     this.submit();
    }
})
});
</script>
<!--Script para Alerta con ajax-->

 <script type="text/javascript">
$('.formulario-eliminar').submit(function(e){
     e.preventDefault();
       Swal.fire({
    title: '¿Está seguro de eliminar permanentemente esta partida?',
    /*text: "You won't be able to revert this!",*/
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Eliminar',
    cancelButtonText: 'Cancelar'

  }).then((result) => {
    if (result.isConfirmed) {
     this.submit();
    }
})
});

    </script>
@endsection
