
<table class="table table-striped tamletra" style="text-align:center" >
    <tr>
      <th style="text-align:center">N°</th>
      <th style="text-align:center">fecha</th>
      <th style="text-align:center">Partida</th>
      <th style="text-align:center">Descripcion</th>
      <th style="text-align:center">Saldo Inicial</th>
      <th style="text-align:center">Debe</th>
      <th style="text-align:center">Haber</th>
      <th style="text-align:center">Saldo</th>
      <th style="text-align:center">Estado</th>
    </tr>
    <?php $no=1;
    $debes = 0;
    $debet = 0;
    $habers = 0;
    $habert = 0;
    $aux = 0;
    $cor = "0";
    $saldost = 0;
    $saldost1 = 0;
    $saldofst = 0;
    $control = 1;
     ?>

     <?php

     if (is_null($partidas2)) {
         $saldost = 0;
         $saldost1 = 0;
     } else {
         foreach ($partidas2 as $key => $value2) {
         $saldost = $saldost + $value2->debe - $value2->haber;
         $saldost1 = $saldost;
         $saldofinap = number_format($saldost1,2,".",",");
         $cuenta = $value2->idcatalogo;
        }
     }
     
    

     ?>

     <tr>
          <td colspan="3">Cuenta {{ $cuenta }}</td>
          <td>Saldo Anterior</td>
          @if($control==1)
            <td align="right">{{ $saldofinap }}</td>
            @endif
            @if($control==0)
            <td></td>
            @endif
          <td></td>
          <td></td>
          <td align="right">{{ $saldofinap }}</td>
          <td></td>
        </tr>
    
    @foreach ($partidas as $key => $value)

    <?php
    $saldofst = $saldost;
    $saldofst = $saldofst + $value->debe - $value->haber;
    $saldost = $saldofst;
    $debep = $value->debe;
    $debet = $debet + $value->debe;
    $haberp = $value->haber;
    $habert = $habert + $value->haber;
    $debep = number_format($debep,2,".",",");
    $debetp = number_format($debet,2,".",",");
    $haberp = number_format($haberp,2,".",",");
    $habertp = number_format($habert,2,".",",");
    $saldofstfinap = number_format($saldofst,2,".",",");
     ?>

    <tr>
        <td>{{ $no++ }}</td>
        <td align="left" width="110px">{{ $value->fecha }}</td>
        <td align="left">{{ $value->idcatalogo }}</td>
        <td align="left">{{ $value->descripcion }}</td>
        <td></td>
        <td align="right">{{ $debep }}</td>
        <td align="right">{{ $haberp }}</td>
        <td align="right">{{ $saldofstfinap }}</td>
        @if($value->estatus2!=null)
        <td>Cerrada</td>
        @endif
        @if($value->estatus2==null)
        <td align="left">Abierta</td>
        @endif
    </tr>
    
    <?php
            $control = 0;
          ?>
      
    @endforeach
    <tr>
          <td colspan="4">Total</td>
          <td align="right">{{ $saldofinap }}</td>
          <td align="right">{{ $debetp }}</td>
          <td align="right">{{ $habertp }}</td>
          <td align="right">{{ $saldofstfinap }}</td>
          <td></td>
    </tr>
  </table>
