@extends ('layout')
@section('container')
<div class="row"  >
    <div class="col-lg-12 margin-tb">
        <div class="pull-left ">
            <h3 > Datos de las partidas</h3>
            <br>
        </div>
    </div>
</div>

{!! Form::open(['route'=>['historialdePartidas'], 'method'=>'GET', 'class'=>'navbar-form pull-right', 'role'=>'search'])!!}
        <div class="col-lg-12 margin-tb">
           <div class="row">
    <div>
      {!! form::label('Cuenta') !!}
      <i>{{ Form::text('cuenta',NULL, ['class'=>'form-control','id'=>'cuenta']) }} </i> 

    </div>
    <div>
      {!! form::label('Fecha inicial') !!}
      <i>{{ Form::date('fechainicial',NULL, ['class'=>'form-control','id'=>'fechainicial']) }} </i> 
  
    </div>
    <div>
      {!! form::label('Fecha final') !!}
      <i>{{ Form::date('fechafinal',NULL, ['class'=>'form-control','id'=>'fechafinal']) }} </i> 
      
    </div>
     <div>
      <button type="submit" class="glyphicon glyphicon-search btn-sm" data-toggle="tooltip" data-placement="top" title="Buscar">Buscar</button>
  </div>
      </div>
            
        </div>
         
        {!! Form::close()!!}


    <div class="row"  >
    <div class="col-lg-12 margin-tb">
        <div class="pull-left ">
            <h3 > Datos de la cuentas por fecha</h3>
            <br>
        </div>
    </div>
</div>

@if($fechai!='2022-01-01')

<div>
        <a class="btn btn-info btn-lg" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{route('partidashpdf',[$fechai, $fechaf, $cuenta])}}">
              <i class="glyphicon glyphicon-list-alt">pdf</i></a>
        <a class="btn btn-info btn-lg" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{route('excelLibroCuenta',[$fechai, $fechaf, $cuenta])}}">
              <i class="glyphicon glyphicon-list-alt">Excel</i></a>
      </div>
@endif

<table class="table table-striped" style="text-align:center" >
    <tr>
      <th style="text-align:center">N°</th>
      <th style="text-align:center">fecha</th>
      <th style="text-align:center">Partida</th>
      <th style="text-align:center">Descripcion</th>
      <th style="text-align:center">Saldo Inicial</th>
      <th style="text-align:center">Debe</th>
      <th style="text-align:center">Haber</th>
      <th style="text-align:center">Saldo</th>
      <th style="text-align:center">Estado</th>
    </tr>
    <?php $no=1;
    $debes = 0;
    $debet = 0;
    $debetp = 0;
    $habers = 0;
    $habert = 0;
    $habertp = 0;
    $aux = 0;
    $cor = "0";
    $saldost = 0;
    $saldofinap = 0;
    $saldofstfinap = 0;
    $saldost1 = 0;
    $saldofst = 0;
    $control = 1;
     ?>

     <?php

     if (is_null($partidas2)) {
         $saldost = 0;
         $saldost1 = 0;
     } else {
         foreach ($partidas2 as $key => $value2) {
         $saldost = $saldost + $value2->debe - $value2->haber;
         $saldost1 = $saldost;
         $saldofinap = number_format($saldost1,2,".",",");
         $cuenta = $value2->idcatalogo;
        }
     }
     
    

     ?>

     <tr>
          <td colspan="3">Cuenta {{ $cuenta }}</td>
          <td>Saldo Anterior</td>
          @if($control==1)
            <td align="right">{{ $saldofinap }}</td>
            @endif
            @if($control==0)
            <td></td>
            @endif
          <td></td>
          <td></td>
          <td align="right">{{ $saldofinap }}</td>
          <td></td>
        </tr>
    
    @foreach ($partidas as $key => $value)

    <?php
    $saldofst = $saldost;
    $saldofst = $saldofst + $value->debe - $value->haber;
    $saldost = $saldofst;
    $debep = $value->debe;
    $debet = $debet + $value->debe;
    $haberp = $value->haber;
    $habert = $habert + $value->haber;
    $debep = number_format($debep,2,".",",");
    $debetp = number_format($debet,2,".",",");
    $haberp = number_format($haberp,2,".",",");
    $habertp = number_format($habert,2,".",",");
    $saldofstfinap = number_format($saldofst,2,".",",");
     ?>

    <tr>
        <td>{{ $no++ }}</td>
        <td align="left" width="110px">{{ $value->fecha }}</td>
        <td align="left">{{ $value->idcatalogo }}</td>
        <td align="left">{{ $value->descripcion }}</td>
        <td></td>
        <td align="right">{{ $debep }}</td>
        <td align="right">{{ $haberp }}</td>
        <td align="right">{{ $saldofstfinap }}</td>
        @if($value->estatus2!=null)
        <td>Cerrada</td>
        @endif
        @if($value->estatus2==null)
        <td align="left">Abierta</td>
        @endif
    </tr>
    
    <?php
            $control = 0;
          ?>
      
    @endforeach
    <tr>
          <td colspan="4">Total</td>
          <td align="right">{{ $saldofinap }}</td>
          <td align="right">{{ $debetp }}</td>
          <td align="right">{{ $habertp }}</td>
          <td align="right">{{ $saldofstfinap }}</td>
          <td></td>
    </tr>
  </table>    



 <div class="text-center">
    <a class="btn btn-primary" href="{{ url('/gestion') }}">Regresar</a>
  </div>

@endsection
