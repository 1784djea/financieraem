@extends('layouts.app2')
@section('content')
  
    	<h3 style="text-align:center"> Actualizacion de Partidas Contables</h3>
    	<br>
      <?php 
      $cont = 0;
      $corr = 0;
      $sumad = 0;
      $sumah = 0;
      $descv = [];
      $idcatalogov = [];
      $debev = [];
      $haberv = [];
      ?>
    @foreach ($partidac as $key => $value)
    <?php 
    $corr = $value->correlativo;
    $idcatalogo = $value->idcatalogo;
    $tipo2 = $value->tipo2;
    $descripcion = $value->descripcion;
    $concepto = $value->concepto;
    $debe = $value->debe;
    $haber = $value->haber;

    $debev[$cont] = $debe;
    $haberv[$cont] = $haber;

    $sumad= $sumad + $debe;

    ?>
    @endforeach
  {{ Form::model($partidac,['route'=>['partidac.update',$corr],'method'=>'PATCH', 'id'=>'valoresForm']) }}
  <div style="width:79%; display:inline-block; border:solid;">
    @csrf
    @foreach ($partidac as $index => $valor)
        <label for="tipo">Partida: </label>
        <input type="text" name="idcatalogo[{{ $index }}]" value="{{ $valor->idcatalogo }}" class="valor-input" required>
        <label for="tipo">Descripcion: </label>
        <input type="text" name="descripcion[{{ $index }}]" value="{{ $valor->descripcion }}" class="valor-input" required>
        <label for="valor_{{ $index }}">Debe: </label>
        <input type="number" id="valor_{{ $index }}" name="debe[{{ $index }}]" value="{{ $valor->debe }}" class="valor-input">
        <br>
    @endforeach
    <strong>Suma Total: <span id="sumaTotal">0</span></strong>
    </div>
    <div style="width:20%; display:inline-block; border:solid;">
    @foreach ($partidac as $index => $valor)
        <label for="valor_{{ $index }}">Haber: </label>
        <input type="number" id="valor_{{ $index }}" name="haber[{{ $index }}]" value="{{ $valor->haber }}" class="valor2-input">
        <br>
    @endforeach
        <strong>Suma Total: <span id="sumaTotal2">0</span></strong>
    </div>
    
    
    <br><br>
    <button type="submit">Actualizar</button>
 {{ Form::close() }}



@push('scripts')

<script>
$(document).ready(function() {
    function calcularSuma() {
        let suma = 0;
        let suma2 = 0;
        $('.valor-input').each(function() {
            const valor = parseFloat($(this).val()) || 0; // Obtener el valor o 0 si no es un número
            suma += valor;
        });
        $('.valor2-input').each(function() {
            const valor2 = parseFloat($(this).val()) || 0; // Obtener el valor o 0 si no es un número
            suma2 += valor2;
        });
        $('#sumaTotal').text(suma);
        $('#sumaTotal2').text(suma2);
    }


    // Calcular suma al cambiar los valores
    $('.valor-input').on('input', function() {
        calcularSuma();
    });

    $('.valor2-input').on('input', function() {
        calcularSuma();
    });

    // Calcular suma al cargar la página
    calcularSuma();
});
</script>


@endpush
@endsection