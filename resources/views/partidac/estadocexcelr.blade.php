
    <div class ="col-sm-12">
      <div class="full.right">
      <h2>BALANCE DE CUENTAS</h2>
      <br>
      </div>
    </div>

      <br>
  <table class="table table-striped" style="text-align:center" >
    <tr>
      <th style="text-align:center">Codigo</th>
      <th style="text-align:center">Nombre</th>
      <th style="text-align:center">Saldo inicial</th>
      <th style="text-align:center">Debe</th>
      <th style="text-align:center">Haber</th>
      <th style="text-align:center">Saldo final</th>
    </tr>
    @foreach ($contelementos as $key => $value)
    @if($value->saldoInicial != 0 || $value->debe != 0 || $value->haber != 0 || $value->saldo != 0)
    <tr>
        <td align="left">{{ $value->elemento }}</td>
        <td align="left">{{ $value->rubroDesc }}</td>
        <td align="right">{{ $value->saldoInicial }}</td>
        <td align="right">{{ $value->debe }}</td>
        <td align="right">{{ $value->haber }}</td>
        <td align="right">{{ $value->saldo }}</td>

      </tr>
      @endif
      @foreach ($contrubros as $key => $value2)
      @if($value->id == $value2->padre)
      @if($value2->saldoInicial != 0 || $value2->debe != 0 || $value2->haber != 0 || $value2->saldo != 0)
      <tr>
        <td align="left">{{ $value2->rubro }}</td>
        <td align="left">{{ $value2->rubroDesc }}</td>
        <td align="right">{{ $value2->saldoInicial }}</td>
        <td align="right">{{ $value2->debe }}</td>
        <td align="right">{{ $value2->haber }}</td>
        <td align="right">{{ $value2->saldo }}</td>
      </tr>
      @endif
      @foreach ($contcuentas as $key => $value3)
      @if($value2->id == $value3->padre)
      @if($value3->saldoInicial != 0 || $value3->debe != 0 || $value3->haber != 0 || $value3->saldo != 0)
      <tr>
        <td align="left">{{ $value3->cuenta }}</td>
        <td align="left">{{ $value3->rubroDesc }}</td>
        <td align="right">{{ $value3->saldoInicial }}</td>
        <td align="right">{{ $value3->debe }}</td>
        <td align="right">{{ $value3->haber }}</td>
        <td align="right">{{ $value3->saldo }}</td>
      </tr>
      @endif
      @foreach ($contsubcuentas as $key => $value4)
      @if($value3->id == $value4->padre)
      @if($value4->saldoInicial != 0 || $value4->debe != 0 || $value4->haber != 0 || $value4->saldo != 0)
      <tr>
        <td align="left">{{ $value4->subcuenta }}</td>
        <td align="left">{{ $value4->rubroDesc }}</td>
        <td align="right">{{ $value4->saldoInicial }}</td>
        <td align="right">{{ $value4->debe }}</td>
        <td align="right">{{ $value4->haber }}</td>
        <td align="right">{{ $value4->saldo }}</td>
      </tr>
      @endif
      @foreach ($contcuentadetalles as $key => $value5)
      @if($value4->id == $value5->padre)
      @if($value5->saldoInicial != 0 || $value5->debe != 0 || $value5->haber != 0 || $value5->saldo != 0)
      <tr>
        <td align="left">{{ $value5->cuentaDetalle }}</td>
        <td align="left">{{ $value5->rubroDesc }}</td>
        <td align="right">{{ $value5->saldoInicial }}</td>
        <td align="right">{{ $value5->debe }}</td>
        <td align="right">{{ $value5->haber }}</td>
        <td align="right">{{ $value5->saldo }}</td>
      </tr>
      @endif
      @endif
    @endforeach
    @endif
    @endforeach
    @endif
    @endforeach
    @endif
    @endforeach
    @endforeach
  </table>
