@extends ('layout')
@section('container')
<div class="row"  >
    <div class="col-lg-12 margin-tb">
        <div class="pull-left ">
            <h3 > Datos de la Partida</h3>
            <br>
        </div>
    </div>
</div>

    


    <table class="table table-striped" style="text-align:center" >
    <tr>
      <th with="80px">No</th>
      <th style="text-align:center">Id</th>
      <th style="text-align:center">Cuenta detalle</th>
      <th style="text-align:center">Rubro</th>
      <th style="text-align:center">Tipo</th>
      <th style="text-align:center">Correlativo</th>
      <th style="text-align:center">Descripcion</th>
      <th style="text-align:center">Fecha</th>
      <th style="text-align:center">Saldo Inicial</th>
      <th style="text-align:center">Debe</th>
      <th style="text-align:center">Haber</th>
      <th style="text-align:center">Saldo final</th>
    </tr>
    <?php $no=1; ?>
    @foreach ($partidac as $key => $value)
    @foreach ($contcuentasd as $key => $value2)
    @if($value->idcatalogo == $value2->subcuenta)
    <tr>
        <td>{{$no++}}</td>
        <td>{{$value->id}}</td>
        <td>{{$value->idcatalogo }}<br></td>
        <td>{{$value2->rubroDesc }}<br></td>
        <td>{{$value->tipo2 }}<br></td>
        <td>{{$value->correlativo }}<br></td>
        <td>{{$value->descripcion }}<br></td>
        <td>{{$value->fecha }}<br></td>
        <td>{{$value->saldoInicial }}<br></td>
        <td>{{$value->debe }}<br></td>
        <td>{{$value->haber }}<br></td>
        <td>{{$value->saldo }}<br></td>
      </tr>
      @endif
    @endforeach
    @endforeach
  </table>

            <br/>
            <a class="btn btn-primary" href="{{ route('partidac.index') }}"> <i class="glyphicon glyphicon-arrow-left"> Regresar</i></a>
    </div>

@endsection
