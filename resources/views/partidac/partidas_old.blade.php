@extends ('layout')
@section('container')
  <div class="row">
    <div class ="col-sm-12">
      <div class="full.right">
      <h2>LIBRO DIARIO</h2>
      <br>
      </div>
    </div>
  </div>
  @if ($message = Session::get('success'))
      <div class="alert alert-success">
          <p>{{ $message }}</p>
      </div>
  @endif
   @if ($message = Session::get('danger'))
      <div class="alert alert-danger">
          <p>{{ $message }}</p>
      </div>
  @endif
      <br>
  <table class="table table-striped" style="text-align:center" >
    <tr>
      <th style="text-align:center">N°</th>
      <th style="text-align:center">Partida</th>
      <th style="text-align:center">Debe</th>
      <th style="text-align:center">Haber</th>
      <th style="text-align:center">Accion</th>
    </tr>
    <?php $no=1;
    $debes = 0;
    $debet = 0;
    $habers = 0;
    $habert = 0;
    $aux = 0;
    $cor = "0";
     ?>

    @foreach ($partidac2 as $key => $value2)
    <?php
    $debet = 0;
    $habert = 0;
     ?>

    @foreach ($partidac as $key => $value)

        @if($value2->correlativo==$value->correlativo)
        <?php 
            $debes = $value->debe;
            $debet = $debet + $debes;
            $debetp = $debet;
            $habers = $value->haber;
            $habert = $habers + $habert;
            $habertp = $habert;
            $status = $value->estatus2;
            
            $debetp = number_format($debetp,2,".",",");
            $habertp = number_format($habertp,2,".",",");
     ?>
        @endif
       @endforeach

    <tr>
        <td>{{ $no++ }}</td>
        <td>{{ $value2->correlativo }}</td>
        <td>{{ $debetp }}</td>
        <td>{{ $habertp }}</td>
        <td>
          <a class="btn btn-info btn-lg" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{route('detallePartidas',[$value2->correlativo, $status])}}">
              <i class="glyphicon glyphicon-list-alt">Detalle</i></a>
          <a href="{{route('reportePartidac4',[$value2->correlativo, $status])}}" class="btn btn-success btn-lg">
            <i class="glyphicon glyphicon-plus"> PDF</i>
        </a>
        </td>
      </tr>
      
    @endforeach
  </table>
 <div class="text-center">
    <a class="btn btn-primary" href="{{ url('/gestion') }}">Regresar</a>
  </div>
@endsection
