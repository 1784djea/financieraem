<!DOCTYPE html>
<html>
<!-- Latest compiled and minified CSS -->
<link href="{{ public_path()}}/css/reportespdf.css" rel="stylesheet">


<head>

    <title>Partida.pdf</title>

</head>

<body>

<div class="container-fluid">
    <div class="header">
            <h2>EM & ASOCIADOS, S.A DE C.V.</h2>
            <h3 > PARTIDA DE {{$vartipo}}</h3>
            <h5>Partida N° {{$num}}</h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h5>Fecha: &nbsp;&nbsp;&nbsp;{{$fechaCierre->fecha}}</h5>
            <br>
            <br>
    </div>

<table class="table table-striped" style="text-align:center">
    <tr>
        <th>Concepto: {{$conceptoCierre->concepto}}</th>
    </tr>
</table>
<br>
<br>



<table class="table table-striped tamletra" style="text-align:center" >
    <thead>
    <tr>
      <th width="10px" style="text-align:center">Cuenta</th>
      <th style="text-align:center">Nombre</th>
      <th style="text-align:center">Descripcion</th>
      <th width="10px" style="text-align:center">Debe</th>
      <th width="10px" style="text-align:center">Haber</th>
    </tr>
</thead>
<?php 
$tdebe = 0;
$thaber = 0;
 ?>
    @foreach ($partidac2 as $key => $value)
    @foreach ($contcuentasd as $key => $value2)
    @if($value->idcatalogo == $value2->subcuenta)
    <tbody>
    <tr>
        <td align="left">{{$value->idcatalogo }}</td>
        <td align="left">{{$value2->rubroDesc }}</td>
        <td align="left">{{$value->descripcion }}</td>
        <?php 

            if ($value->debe==0) {
                $debevp = '';
            } else {
                $debev = $value->debe;
                $debevp = $value->debe;
                $debevp = number_format($debevp,2,".",",");
            }
            
            if ($value->haber==0) {
                $habervp = '';
            } else {
                $haberv = $value->haber;
                $habervp = $value->haber;
                $habervp = number_format($habervp,2,".",",");
            }
        ?>
        <td align="right">{{$debevp }}</td>
        <td align="right">{{$habervp }}</td>
      </tr>
      <?php 
            $tdebe = $tdebe + $value->debe;
            $tdebep = $tdebe;
            $thaber = $thaber + $value->haber;
            $thaberp = $thaber;

            $tdebep = number_format($tdebep,2,".",",");
            $thaberp = number_format($thaberp,2,".",",");
        ?>
  
      @endif
    @endforeach
    @endforeach
    </tbody>
      <tfoot>
    <tr>
      <td id="total" colspan="3">Total :</td>
      <td align="right">{{$tdebep}}</td>
      <td align="right">{{$thaberp}}</td>
    </tr>
   </tfoot>
  </table>
<br>
<br>
<br>
<br>
<br>
  <div class="header">
            <h5>_______________________________</h5>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h5>_______________________________</h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <h5>Elaborado por</h5>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<h5>Revisado por</h5>
            <br>
            <br>
    </div>

    </div>

        <script type="text/php">
            if (isset($pdf)) {
                $text = "Página {PAGE_NUM} / {PAGE_COUNT}";
                $size = 10;
                $font = $fontMetrics->getFont("Verdana");
                $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
                $x = 250;
                $y = 10;;
                $pdf->page_text($x, $y, $text, $font, $size);
            }
        </script>

    </body>

</html>