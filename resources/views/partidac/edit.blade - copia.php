@extends('layouts.app2')
@section('content')
  <div class="row">
    	<h3 style="text-align:center"> Actualizacion de Partidas Contables</h3>
    	<br>
      <?php 
      $cont = 0;
      $sumad = 0;
      $sumah = 0;
      $descv = [];
      $idcatalogov = [];
      $debev = [];
      $haberv = [];
      ?>
    @foreach ($partidac as $key => $value)
    <?php 
    $idcatalogo = $value->idcatalogo;
    $tipo2 = $value->tipo2;
    $descripcion = $value->descripcion;
    $concepto = $value->concepto;
    $debe = $value->debe;
    $haber = $value->haber;

    $debev[$cont] = $debe;
    $haberv[$cont] = $haber;

    $sumad= $sumad + $debe;

    ?>

<div class="row">
  <form id="suma-form">

        <div class="col-md-2">
          <div class="form-group">
            <label for="idcatalogo">Id Catalogo</label>                  
            <input type="text" name="idcatalogo" class="form-control" placeholder="{{ $idcatalogo }}" value="{{ $idcatalogo }}" required>
          </div>
        </div>

        <div class="col-md-1">
          <div class="form-group">
            <label for="tipo2">Tipo</label>                  
            <input type="text" name="tipo2" class="form-control" placeholder="{{ $tipo2 }}" value="{{ $tipo2 }}" required>
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label for="descripcion">Descripcion</label>                  
            <input type="text" name="descripcion" class="form-control" placeholder="{{ $descripcion }}" value="{{ $descripcion }}" required>
          </div>
        </div>

        <div class="col-md-3">
          <div class="form-group">
            <label for="concepto">Concepto</label>                  
            <input type="text" name="concepto" class="form-control" placeholder="{{ $concepto }}" value="{{ $concepto }}" required>
          </div>
        </div>

        

        <div class="col-md-1">
          <div class="form-group">
            <label for="debe">Debe</label>                  
            <input type="text" name="$debe" id="bt_input" class="debe form-control" placeholder="{{ $debev[$cont] }}" value="{{ $debev[$cont] }}" required>
          </div>
        </div>

        <div class="col-md-1">
          <div class="form-group">
            <label for="haber">Haber</label>                  
            <input type="text" name="$haber" id="bt_input" class="form-control" placeholder="{{ $haberv[$cont] }}" value="{{ $haberv[$cont] }}" required>
          </div>
        </div>

         </form>
        </div>



@endforeach

<p>Resultado: <span id="resultado">0</span></p><br>


            <button type="submit">Sumar</button>

        <?php 

          $cont++;
        
         ?>


  </div>


@push('scripts')

<script>
  
  $(document).ready(function(){
    $('#suma-form').submit(function(event) {

        let suma = 0;
        $('.debe').each(function() {
            suma += parseFloat($(this).val()) || 0; // Sumar valores, manejar NaN
        });

        $('#resultado').text(suma);

  }
  function limpiar(){
    $("#pcantidad").val("");
    $("#ptipo2").val("");
    $("#descripcion").val("");
  }
  //Muestra botón guardar
  function evaluar(){
    if(totals == 0){
      $("#guardar").show();
    }else{
      $("#guardar").hide();
    }
  }
  function eliminar(index){
    totals = totals + debea[index];
    totals = totals - habera[index];
    totals = Math.round(totals*100)/100;
    Debetotals = Debetotals - debea[index];
    Debetotals = Math.round(Debetotals*100)/100;
    Habertotals = Habertotals - habera[index];
    Habertotals = Math.round(Habertotals*100)/100;
    $("#totals").html("$" + totals);
    $("#Debetotals").html("$" + Debetotals);
    $("#Habertotals").html("$" + Habertotals);
    $("#fila" + index).remove();
    evaluar();
  }
  $(document).ready(function() {
var data = {}; 
$("#cliente option").each(function(i,el) {  
   data[$(el).data("value")] = $(el).val();
});
// `data` : object of `data-value` : `value`
console.log(data, $("#cliente option").val());
    $('#submit').click(function()
    {
        var value = $('#selected').val();
        alert($('#cliente [value="' + value + '"]').data('value'));
    });
});
</script>


@endpush
@endsection