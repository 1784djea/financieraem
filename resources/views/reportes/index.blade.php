@extends ('layout')
@section('header')
<header style="background-image: url('startbootstrap-clean-blog-gh-pages/assets/img/titulos.jpg'); opacity: 0.8;"><h2 style="color: white; font-family: sans-serif; font-size: 58px; text-align: center;">Gestion de Reportes</h2>
  </header>
@endsection
@section('container')

 
      <br>
 <table class="table table-striped" style="text-align:center" >
    <tr>
      <th style="text-align:center">Tipo</th>
      <th style="text-align:center">Cantidad</th>
      <th style="text-align:center">Reportes</th>
    </tr>
    <?php 
    $contp=0;
    $contc=0;
     ?>
    @foreach ($prestamos as $key => $value)

        @if($value->notas3=='Pendiente')
        <?php 
            $contp++;
         ?>
        @else
        <?php 
            $contc++;
         ?>
        @endif
       @endforeach

    <tr>
        <td>Prestamos Pendientes</td>
        <td>{{$contp}}</td>
        <td><a href="{{route('reportePrestamop')}}" class="btn btn-success btn-lg">
            <i class="glyphicon glyphicon-plus"> PDF</i>
        </a>
    <a href="{{route('reportePrestamoe')}}" class="btn btn-success btn-lg">
            <i class="glyphicon glyphicon-plus"> Excel</i>
        </a></td>
    </tr>
    <tr>
        <td>Prestamos Cancelados</td>
        <td>{{$contc}}</td>
        <td></td>
    </tr>
      
  </table>
 <div class="text-center">
    <a class="btn btn-primary" href="{{ url('/gestion') }}">Regresar</a>
  </div>

  <!--Script para mostrar formulario y Alerta confirmar Guardar con ajax-->
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script type="text/javascript">
      $('.agregar').hide();
       function mostrarFormulario(){
        $('.agregar').show();
       }
$('.agregar').submit(function(e){
     e.preventDefault();Swal.fire({
  title: '¿Está seguro de guardar esta partida?',
  showDenyButton: true,
  //showCancelButton: true,
  confirmButtonText: `Guardar`,
  denyButtonText: `Cancelar`,
})
     .then((result) => {
    if (result.isConfirmed) {
     this.submit();
    }
})
});
</script>
<!--Script para Alerta con ajax-->

 <script type="text/javascript">
$('.formulario-eliminar').submit(function(e){
     e.preventDefault();
       Swal.fire({
    title: '¿Está seguro de eliminar permanentemente esta partida?',
    /*text: "You won't be able to revert this!",*/
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Eliminar',
    cancelButtonText: 'Cancelar'

  }).then((result) => {
    if (result.isConfirmed) {
     this.submit();
    }
})
});

    </script>
@endsection
