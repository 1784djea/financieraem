

<div class="container-fluid">
    <div class="header">
                                
                                <h5>Reporte de Prestamos</h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>

<div class="row"  >
    <div class="col-lg-12 margin-tb">
        <div class="pull-left ">
            <h3 > Detalle de prestamos</h3>
            <br>
        </div>
    </div>
</div>

<table class="table table-striped tamletra" style="text-align:center" >
    <tr>
        <?php 
    $con = 1;
    $prestamo = 0;
     ?>
      <th style="text-align:center">No</th>
      <th style="text-align:center">Fecha Inicio</th>
      <th style="text-align:center">Nombre</th>
      <th style="text-align:center">MONTO DEL PRESTAMO</th>
      <th style="text-align:center">N° de cuotas</th>
      <th style="text-align:center">Valor de Cuota</th>
      <th style="text-align:center">Cuotas pagadas</th>
      <th style="text-align:center">Pagado</th>
      <th style="text-align:center">Capital</th>
      <th style="text-align:center">Intereses</th>
      <th style="text-align:center">SALDO ACTUAL</th>
    </tr>
      @foreach ($prestamos as $key => $value)
      @if($value->notas3 == 'Pendiente')
      <?php 
    $conp = 0;
    $capital = 0;
    $intereses = 0;
    $pagado = 0;
    $prestamo = $value->idprest;
    $date = $value->fechaInicio;
    $dia = substr($date, 8,2);
    $mes = substr($date, 5,2);
    $anio = substr($date, 0,4);
    $fecha2 = $dia.'-'.$mes.'-'.$anio;

    $monto = $value->cantidad;
    $tiempo = $value->tiempo * 12;
    $cuota = $value->pago;
    $pend = $value->pendpago;

     ?>
      @foreach ($pagos as $key => $value2)
      @if($value->id == $value2->idprest2)
      <?php 
        $conp = $conp + 1;
        $capital = $capital + $value2->capitalpagado;
        $intereses = $intereses + $value2->intereses;
     ?>
      
    @endif
    @endforeach
    <?php 
        $pagado = $capital + $intereses;

        $montop = number_format($monto,2,".",",");
        $tiempop = number_format($tiempo,0,".",",");
        $cuotap = number_format($cuota,2,".",",");
        $pagadop = number_format($pagado,2,".",",");
        $capitalp = number_format($capital,2,".",",");
        $interesesp = number_format($intereses,2,".",",");
        $pendp = number_format($pend,2,".",",");

     ?>
    @foreach ($prestatarios as $key => $value3)
      @if($prestamo == $value3->id)

      
    <tr>
        <td align="left">{{ $con }}</td>
        <td align="left">{{ $fecha2 }}</td>
        <td align="left">{{ $value3->pnombre }} {{ $value3->snombre }} {{ $value3->papellido }} {{ $value3->sapellido }}</td>
        <td align="right">${{ $montop }}</td>
        <td align="right">{{ $tiempop }}</td>
        <td align="right">${{ $cuotap }}</td>
        <td align="right">{{ $conp }}</td>
        <td align="right">${{ $pagadop }}</td>
        <td align="right">${{ $capitalp }}</td>
        <td align="right">${{ $interesesp }}</td>
        <td align="right">${{ $pendp }}</td>
      </tr>
      @endif
    @endforeach
      <?php 
    $con++;
     ?>
     @endif
    @endforeach
  </table>

