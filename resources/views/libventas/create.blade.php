@extends('layouts.app2')
@section('content')

      {{ Form::open(['route'=>'libventas.store', 'method'=>'POST']) }}

      <?php 
      
       ?>

       <?php 
                  if (Empty($libventasc)) {
                    $fecha = '2022-01-01';
                  }else{
                  $fecha = $libventasc->fechaccf;
                }
                   ?>

         <div class="row">
    <div class="col-sm-3">
      <br>
      {!! form::label('Fecha:') !!}
      <div>
            <input type="date" name="fechaccf" class="form-control" placeholder="Fecha" min = "{{ $fecha }}"  onkeydown="return false" required>
    </div>
  </div>

  <div class="col-sm-2">
            <br>
      {!! form::label('Correlativo:') !!}
                  <?php 
                  if (Empty($libventasc)) {
                    $correl = 1;
                  }else{
                  $correl = $libventasc->correlativo + 1;
                }
                   ?>
                  
            <input type="text" name="correlativo" class="form-control" placeholder="{{ $correl }}" value="{{ $correl }}" required>
        </div>

  <div class="col-sm-3">
      <br>
      {!! form::label('Monto Gravado:') !!}
            <i> {{ Form::number('montoGravado',NULL, ['class'=>'form-control','id'=>'montoGravado','placeholder'=>'Monto Gravado']) }}  </i> 
  </div>

  <div class="col-sm-3">
      <br>
      {!! form::label('Ventas No Sujeta:') !!}
            <i> {{ Form::number('montoNoSujeto',NULL, ['class'=>'form-control','id'=>'montoNoSujeto','placeholder'=>'Monto No Sujeto']) }}  </i> 
  </div>

  <div class="row">
        <div class="col-md-12" id="guardar">
          <div class="form-group">
            <button class="btn btn-primary" type="submit">
              Guardar
            </button>
          </div>
        </div>
        </div>

      {{ form::close() }}

@endsection
