   <div class="row">
    <div class="col-sm-3">
      <br>
      {!! form::label('Fecha:') !!}
      <div>
            <i> {{ Form::date('fechaccf',NULL, ['class'=>'form-control','id'=>'fechaccf','placeholder'=>'Fecha']) }}  </i>
    </div>
  </div>

  <div class="col-sm-2">
            <br>
      {!! form::label('Correlativo:') !!}
                  <?php 
                  if (Empty($libventasc)) {
                    $correl = 1;
                  }else{
                  $correl = $libventasc->correlativo + 1;
                }
                   ?>
                  
            {{$correl}}
            <input type="text" name="correlativo" class="form-control" placeholder="{{ $correl }}" value="{{ $correl }}" required>
        </div>

  <div class="col-sm-3">
      <br>
      {!! form::label('Monto Gravado:') !!}
            <i> {{ Form::number('montoGravado',NULL, ['class'=>'form-control','id'=>'montoGravado','placeholder'=>'Monto Gravado']) }}  </i> 
  </div>

  <div class="col-sm-3">
      <br>
      {!! form::label('Ventas No Sujeta:') !!}
            <i> {{ Form::number('montoNoSujeto',NULL, ['class'=>'form-control','id'=>'montoNoSujeto','placeholder'=>'Monto No Sujeto']) }}  </i> 
  </div>

     <div>
      <br>
      <br>
       {{ Form::button(isset($model)? 'Update' : 'Guardar' , ['class'=>'btn btn-success btn-sm','type'=>'submit']) }}
        <a class="btn btn-danger btn-sm" href="{{ route('libventas.index') }}">Cancelar</a>
     </div>
      
   
      </div>
    
     
      <br>
 
