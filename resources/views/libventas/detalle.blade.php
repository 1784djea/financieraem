@extends('layouts.app2')
@section('content')
<div class="row"  >
    <div class="col-lg-12 margin-tb">
        <div class="pull-left ">
            <h3 > Detalle de la venta</h3>
            <br>
        </div>
    </div>
</div>

<table class="table table-striped" style="text-align:center" >
    <tr>
      <th with="80px">No</th>
      <th style="text-align:center">Fecha</th>
      <th style="text-align:center">Del N°</th>
      <th style="text-align:center">Al N°</th>
      <th style="text-align:center">Ventas Exentas</th>
      <th style="text-align:center">Ventas No Sujetas</th>
      <th style="text-align:center">Ventas Locales</th>
      <th style="text-align:center">Exportacion</th>
      <th style="text-align:center">Total Ventas</th>
    </tr>
    <?php $no=1;
    $newDate = 0;
    $nummin = 0;
    $nummax = 0;
    $suma = 0;
    $orden = [];
    $fechas = [];
    $control = 0;
    $tnetas = 0;
    $tnetas2 = 0;
    $texentas = 0;
    $tdebito = 0;
    //var_dump($libventas3);
     ?>
    @foreach ($libventas3 as $key => $value3)
    @if($value3->fechaccf!='')
    @foreach ($libventas as $key => $value)
    @if($value->fechaccf==$value3->fechaccf)
    

        
        <?php

        $originalDate = $value->fechaccf;
        $newDate = date("d/m/Y", strtotime($originalDate));

        $suma = $suma + $value->montoGravado;

        $orden[$control] = $value->correlativo;

        $control ++;
        ?>
        @endif
        @endforeach
        
        <?php 
        $tnetas = $tnetas + $suma;

        $nummin = reset($orden);
        $nummax = end($orden);
        $orden = [];

         ?>
    <tr>
        <td>{{$no++}}</td>
        <td>{{ $newDate }}</td>
        <td>{{ $nummin }}</td>
        <td>{{ $nummax }}</td>
        <td></td>
        <td></td>
        <td>{{ $suma }}</td>
        <td></td>
        <td>{{ $suma }}</td>
      </tr>
    @endif
    @endforeach
    <tr>
        <td colspan="4">Total:</td>
        <td></td>
        <td></td>
        <td>{{ $tnetas }}</td>
        <td></td>
        <td>{{ $tnetas }}</td>
      </tr>
  </table>

  <br>
  <br>
<?php 
$tdebito = ($tnetas*0.13);
$tnetas2 = ($tnetas - $tdebito);
 ?>
  <table class="table-striped" style="text-align:center" >
    <tr>
      <th width="135px">Resumen</th>
      <th width="80px"></th>
    </tr>
    <tr>
        <td style="text-align:left">Exentas</td>
        <td></td>
        <td>-</td>
    </tr>
    <tr>
        <td style="text-align:left">Netas Gravadas 13%</td>
        <td></td>
        <td style="text-align:right">$ {{ $tnetas2 }}</td>
    </tr>
    <tr>
        <td style="text-align:left">Debito Fiscal</td>
        <td></td>
        <td style="text-align:right">$ {{ $tdebito }}</td>
    </tr>
    <tr>
        <td style="text-align:left">Exportaciones 0%</td>
        <td></td>
        <td>-</td>
    </tr>
    <tr>
        <td style="text-align:left">Total</td>
        <td></td>
        <td style="text-align:right">$ {{ $tnetas }}</td>
    </tr>
  </table>

            <br/>
            <a class="btn btn-primary" href="{{ route('libventas.index') }}"> <i class="glyphicon glyphicon-arrow-left"> Regresar</i></a>
    </div>

@endsection
