<select id="prestatarioSelect" name="idprest" class="form-control" data-live-search="true">
    <option value="">Seleccionar Prestatario</option>
</select>

<br>
<select id="prestamoSelect" name="idprest2" class="form-control" data-live-search="true">
    <option value="">Seleccionar Prestamo</option>
</select>

<script type="text/javascript">
$(document).ready(function () {
    // Populate categories dropdown
    $.get('/get-prestatarios', function (data) {
        $.each(data, function (key, value) {
            $('#prestatarioSelect').append('<option value="' + value.id + '">' + value.pnombre + ' ' + value.snombre + ' ' + value.papellido + ' ' + value.sapellido + '</option>');
        });
    });

    // Handle category selection change
    $('#prestatarioSelect').change(function () {
        var selectedPrestatario = $(this).val();
        $('#prestamoSelect').empty();

        if (selectedPrestatario !== '') {
            // Populate products dropdown based on selected category
            $.get('/get-prestamos/' + selectedPrestatario, function (data) {
                $.each(data, function (key, value) {
                    $('#prestamoSelect').append('<option value="' + value.id + '">' + value.id + ' ' + '-' + value.codigoPrestamo + '</option>');
                });
            });
        }
    });
});

</script>