@extends('layouts.app2')

@section('content')

<style>
    .alternativa {
      display: none;
    }
  </style>


      
      {{ Form::open(['route'=>'libcompras.store', 'method'=>'POST']) }}
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        
        <h3>Nuevo ingreso
        </h3>
        

        <div class="row">
          <div class="panel panel-body panel-primary">
            <div class="col-md-2">
          <div class="form-group">
            <label for="tipoDoc">Tipo de documento</label>
            <select name="tipoDoc" id="tipoDoc" class="form-control" required>
              <option value="1">Compra Local</option>
              <option value="2">Sujeto Excluido</option>
              <option value="3">Importacion</option>
              <option value="4">Nota de Credito</option>
              <option value="5">Documento Contable de Liquidacion</option>
              <option value="6">Retencion de IVA (No inscritos al iva)</option>
              <option value="7">Retencion de iva (Comp. de retencion com</option>
            </select> 
          </div>
        </div>
            <div class="col-md-4">
          <div class="form-group">
            <label for="ccf">Documento número</label>
            <input type="text" name="ccf" id="ccf" class="form-control">
          </div>
        </div><!-- fin col-md-3 -->

        <div class="col-md-2">
          <div class="form-group">
            <label for="año">Periodo</label>
            <select name="año" id="año" class="form-control" required>
              <option value="">Seleccionar año</option>
              <option value="2022-10">2022 Octubre</option>
              <option value="2022-11">2022 Noviembre</option>
              <option value="2022-12">2022 Diciembre</option>
              <option value="2023-01">2023 Enero</option>
              <option value="2023-02">2023 Febrero</option>
              <option value="2023-03">2023 Marzo</option>
              <option value="2023-04">2023 Abril</option>
              <option value="2023-05">2023 Mayo</option>
              <option value="2023-06">2023 Junio</option>
              <option value="2023-07">2023 Julio</option>
              <option value="2023-08">2023 Agosto</option>
              <option value="2023-09">2023 Septiembre</option>
              <option value="2023-10">2023 Octubre</option>
              <option value="2023-11">2023 Noviembre</option>
              <option value="2023-12">2023 Diciembre</option>
              <option value="2024-01">2024 Enero</option>
            </select> 
          </div>
        </div>


        <div class="col-md-2">
          <div class="form-group">
            <div id="fechaf">
              Selecciona el fecha a ingresar
            </div>
          </div>
        </div>

        <div class="col-md-1">
          <div class="form-group">
            <label for="correlativo">Correlativo</label>
                  <?php 
                  if (Empty($libcompra)) {
                    $correl = 1;
                  }else{
                  $correl = $libcompra->correlativo + 1;
                }
                   ?>
                  
            <input type="text" name="correlativo" class="form-control" placeholder="{{ $correl }}" value="{{ $correl }}" required>
          </div>
        </div><!-- fin col-md-3 -->

        

            <div class="col-md-12"></div>

        
 <div class="col-md-3">
              <div class="form-group">
                <label for="idContribuyente">Registro</label>
              <select name="idContribuyente" id="idContribuyente" class="form-control selectpicker" data-live-search="true">
                  @foreach($contribuyentes as $art)
                  <option value="{{ $art->nombreContrib }}">
                    {{ $art->nombreContrib }}
                  </option>
                  @endforeach
              </select>
              </div>
            </div>
            
            <div class="col-md-3">
          <div class="form-group">
            <label><strong>Calculo de impuestos :</strong></label><br>
                                <label><input type="checkbox" name="calculo2" id="calculo2"> Exento</label>
                                <label><input type="checkbox" name="calculo4" id="calculo4"> R. iva</label>
                                <label><input type="checkbox" name="calculo5" id="calculo5"> Renta</label>
          </div>
        </div>

        <div class="col-md-2">
              <div class="form-group">
                <label for="montoGravado">Monto Gravado</label>
                <input type="number" name="montoGravado" id="montoGravado" class="form-control" placeholder="Cantidad">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <label for="montoGravado2">Monto No Sujeto</label>
                <input type="number" name="montoGravado2" id="montoGravado2" class="form-control" placeholder="Cantidad">
              </div>
            </div>

            <div class="col-md-2">
              <div class="form-group">
                <button type="button" id="bt_add" class="btn btn-primary">
                  Agregar
                </button>
              </div>
            </div>

            
            <div class="col-md-12">
              <table id="detalles" class="table table-striped table-bordered table-hover table-condensed" style="margin-top: 10px">
                <thead style="background-color: #A9D0F5">
                  <th>Opciones</th>
                  <th>Numero</th>
                  <th>Registro</th>
                  <th>Nombre</th>
                  <th>Fecha</th>
                  <th>Tipo</th>
                  <th>Compras no sujetas</th>
                  <th>Compras Gravadas</th>
                  <th>IVA</th>
                  <th>Total de Compras</th>
                  <th>Retencion 1%</th>
                  <th>Renta</th>
                  <th>Sujeto Excluido</th>
                </thead>
                <tfoot>
                  <th>Total</th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th><h4 id="montots2">0.00</h4></th>
                  <th><h4 id="snetots">0.00</th>
                  <th><h4 id="sivats">0.00</th>
                  <th><h4 id="montots">0.00</h4></th>
                  <th><h4 id="srivats">0.00</h4></th>
                  <th><h4 id="srentats">0.00</h4></th>
                  <th><h4 id="montots3">0.00</h4></th>
                </tfoot>
                <tbody>
                  
                </tbody>
              </table>
            </div>

          </div><!-- panel-body -->

        </div><!-- fin row detalle -->
        
        <div class="row">
        <div class="col-md-12" id="guardar">
          <div class="form-group">
            <button class="btn btn-primary" type="submit">
              Guardar
            </button>
          </div>
        </div>
        </div><!-- fin row buttons -->
      {{ form::close() }}


  <div class="alternativa" id="alternativa-2022-10">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2022-07-01" max = "2022-10-31" onkeydown="return false">
    </div>
  </div>

   <div class="alternativa" id="alternativa-2022-11">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2022-08-01" max = "2022-11-30" onkeydown="return false">
    </div>
  </div>

   <div class="alternativa" id="alternativa-2022-12">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2022-09-01" max = "2022-12-31" onkeydown="return false">
    </div>
  </div>

   <div class="alternativa" id="alternativa-2023-01">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2022-10-01" max = "2023-01-31" onkeydown="return false">
    </div>
  </div>

  <div class="alternativa" id="alternativa-2023-02">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2022-11-01" max = "2023-02-28" onkeydown="return false">
    </div>
  </div>

  <div class="alternativa" id="alternativa-2023-03">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2022-12-01" max = "2023-03-31" onkeydown="return false">
    </div>
  </div>

  <div class="alternativa" id="alternativa-2023-04">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2023-01-01" max = "2023-04-30" onkeydown="return false">
    </div>
  </div>

  <div class="alternativa" id="alternativa-2023-05">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2023-02-01" max = "2023-05-31" onkeydown="return false">
    </div>
  </div>

  <div class="alternativa" id="alternativa-2023-06">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2023-03-01" max = "2023-06-30" onkeydown="return false">
    </div>
  </div>

  <div class="alternativa" id="alternativa-2023-07">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2023-04-01" max = "2023-07-31" onkeydown="return false">
    </div>
  </div>

  <div class="alternativa" id="alternativa-2023-08">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2023-05-01" max = "2023-08-31" onkeydown="return false">
    </div>
  </div>

  <div class="alternativa" id="alternativa-2023-09">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2023-06-01" max = "2023-09-30" onkeydown="return false">
    </div>
  </div>

  <div class="alternativa" id="alternativa-2023-10">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2023-07-01" max = "2023-10-31" onkeydown="return false">
    </div>
  </div>

  <div class="alternativa" id="alternativa-2023-11">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2023-08-01" max = "2023-11-30" onkeydown="return false">
    </div>
  </div>

  <div class="alternativa" id="alternativa-2023-12">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2023-09-01" max = "2023-12-31" onkeydown="return false">
    </div>
  </div>

  <div class="alternativa" id="alternativa-2024-01">
    <div class="form-group">
            <label for="fechaccf">Fecha CCF</label>
            <input type="date" name="fechaccf" id="fechaccf" class="form-control" min = "2023-10-01" max = "2024-01-31" onkeydown="return false">
    </div>
  </div>

@push('scripts')
<script>
  
  $(document).ready(function(){
    $("#bt_add").click(function(){
      agregar();
    });
  });
  var cont = 0;
  var aux = 0;
  var totals = 0;
  var sneto = [];
  var snetot = 0;
  var snetots = 0;
  var siva = [];
  var sivat = 0;
  var sivats = 0;
  var sriva = [];
  var srivat = 0;
  var srivats = 0;
  var srenta = [];
  var srentat = 0;
  var srentats = 0;
  var montot = [];
  var montot2 = [];
  var montot3 = [];
  var montott = 0;
  var montots = 0;
  var montots2 = 0;
  var montots3 = 0;
  var montoGravado3 = 0;
  var montotss = 0;
  var tipo2 = [];
  var tipo2t = "0";
  var tipoDoc2 = "0";
  var Habertotals = 0;
  var subtotal = [];
  var subtotal2 = [];
  var subtotal3 = [];
  var sneto2 = 0;
  var siva2 = 0;
  var sriva2 = 0;
  var srenta2 = 0;
  //Cuando cargue el documento
  //Ocultar el botón Guardar
  $("#guardar").hide();
  function agregar(){
    //Obtener los valores de los inputs
    tipoDoc = $("#tipoDoc option:selected").val();
    ccf = $("#ccf").val();
    fechaccf = $("#fechaccf").val();
    montoGravado = $("#montoGravado").val();
    montoGravado2 = $("#montoGravado2").val();
    idContribuyente = $("#idContribuyente option:selected").val();
    calculo2 = document.getElementById("calculo2").checked;
    calculo4 = document.getElementById("calculo4").checked;
    calculo5 = document.getElementById("calculo5").checked;
    //Validar los campos
      if(tipoDoc != "" || montoGravado > 0 || montoGravado2 > 0){

        if(tipoDoc == "1"){
          tipoDoc2= "Compra Local";
        
      //subtotal array inicie en el indice cero
        if(calculo2){

          sneto[cont] = (montoGravado);
          sneto2 = Math.round(sneto[cont]*100)/100;
          snetot = snetot + sneto2;
          siva[cont] = 0;
          siva2 = Math.round(siva[cont]*100)/100;
          sivat = sivat + siva2;
          montoGravado2 = montoGravado2 * 1;
          montoGravado3 = 0;
      montot3[cont] = 0;
          montoGravado = (montoGravado * 1)  + montoGravado2;
          montot[cont] = montoGravado;
          montot2[cont] = montoGravado2;
          tipo2t = "Exento"; 
          tipo2[cont] = tipo2t;
          srenta2 = 0;
          srenta[cont] = srenta2;
          sriva2 = 0;
          sriva[cont] = sriva2;

        } else if(calculo4){

          if (montoGravado > 99) {

            sneto[cont] = (montoGravado * 1);
            sneto2 = Math.round(sneto[cont]*100)/100;
            snetot = snetot + sneto2;
            sriva[cont] = (montoGravado * 0.01);
            sriva2 = Math.round(sriva[cont]*100)/100;
            srivat = srivat + sriva2;
            montoGravado = montoGravado * 1;
            montoGravado2 = montoGravado2 * 1;
            montoGravado3 = 0;
      montot3[cont] = 0;
            montoGravado = montoGravado + sriva2  + montoGravado2;
            montot[cont] = montoGravado ;
            montot2[cont] = montoGravado2;
            tipo2t = "R. iva"; 
            tipo2[cont] = tipo2t;
            srenta2 = 0;
            srenta[cont] = srenta2;
            siva2 = 0;
            siva[cont] = siva2;

          } else {

            sneto[cont] = (montoGravado);
            sneto2 = Math.round(sneto[cont]*100)/100;
            snetot = snetot + sneto2;
            sriva[cont] = 0;
            sriva2 = Math.round(sriva[cont]*100)/100;
            srivat = srivat + sriva2;
            montoGravado2 = montoGravado2 * 1;
            montoGravado3 = 0;
      montot3[cont] = 0;
            montoGravado = montoGravado * 1  + montoGravado2;
            montot[cont] = montoGravado;
            montot2[cont] = montoGravado2;
            tipo2t = "R. iva"; 
            tipo2[cont] = tipo2t;
            srenta2 = 0;
            srenta[cont] = srenta2;
            siva2 = 0;
            siva[cont] = siva2;

          }

        } else if(calculo5){

          sneto[cont] = (montoGravado * 1);
          sneto2 = Math.round(sneto[cont]*100)/100;
          snetot = snetot + sneto2;
          sriva2 = 0;
          sriva[cont] = sriva2;
          siva[cont] = 0;
          srenta[cont] = (montoGravado * 0.1);
          srenta2 = Math.round(srenta[cont]*100)/100;
          srentat = srentat + srenta2;
          montoGravado = montoGravado * 1;
          montoGravado2 = montoGravado2 * 1;
          montoGravado3 = 0;
      montot3[cont] = 0;
          montoGravado = montoGravado + srenta2  + montoGravado2;
          montot[cont] = montoGravado ;
          montot2[cont] = montoGravado2;
          tipo2t = "Renta"; 
          tipo2[cont] = tipo2t;

        } else {
      
      siva[cont] = (montoGravado * 0.13);
      sneto[cont] = (montoGravado * 1);
      siva2 = Math.round(siva[cont]*100)/100;
      sivat = sivat + siva2;
      sneto2 = Math.round(sneto[cont]*100)/100;
      snetot = snetot + sneto2;
      montoGravado = montoGravado * 1;
      montot[cont] = montoGravado;
      montoGravado2 = montoGravado2 * 1;
      montoGravado3 = 0;
      montot3[cont] = 0;
      montot2[cont] = montoGravado2;
      montoGravado = montoGravado + siva2  + montoGravado2;
      tipo2t = "En blanco"; 
      tipo2[cont] = tipo2t;
      srenta2 = 0;
      srenta[cont] = srenta2;
      sriva2 = 0;
      sriva[cont] = sriva2;

      }
      }
      if(tipoDoc == "2"){
          tipoDoc2= "Sujeto Exento";

      siva[cont] = 0;
      sneto[cont] = 0;
      siva2 = Math.round(siva[cont]*100)/100;
      sivat = sivat + siva2;
      sneto2 = 0;
      snetot = snetot + sneto2;
      montoGravado = (montoGravado * 1);
      montot[cont] = Math.round(montoGravado*100)/100;
      montoGravado2 = 0;
      montot2[cont] = 0;
      montoGravado3 = (montoGravado * 1);
      montot3[cont] = Math.round(montoGravado3*100)/100;
      //montoGravado3 = montoGravado3 + siva2 + montoGravado + montoGravado2;
      tipo2t = "Sujeto Excl."; 
      tipo2[cont] = tipo2t;
      srenta2 = 0;
      srenta[cont] = srenta2;
      sriva2 = 0;
      sriva[cont] = sriva2;

      }

      snetots = snetots + sneto2;
      snetots = Math.round(snetots*100)/100;
      sivats = sivats + siva2;
      sivats = Math.round(sivats*100)/100;
      srivats = srivats + sriva2;
      srivats = Math.round(srivats*100)/100;
      srentats = srentats + srenta2;
      srentats = Math.round(srentats*100)/100;
      montots = montots + montoGravado;
      montots = Math.round(montots*100)/100; 
      montots2 = montots2 + montoGravado2;
      montots2 = Math.round(montots2*100)/100; 
      montots3 = montots3 + montoGravado3;
      montots3 = Math.round(montots3*100)/100; 

      var fila = '<tr class="selected" id="fila'+cont+'"><td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+')">X</button></td><td><input type="hidden" name="ccf[]" value="'+ccf+'">'+ccf+'</td><td><input type="hidden" name="tipoDoc[]" value="'+tipoDoc2+'">'+tipoDoc2+'</td><td><input type="hidden" name="idContribuyente[]" value="'+idContribuyente+'">'+idContribuyente+'</td><td><input type="hidden" name="fechaccf[]" value="'+fechaccf+'">'+fechaccf+'</td><td><input type="hidden" name="tipo2[]" value="'+tipo2t+'">'+tipo2t+'</td><td><input type="hidden" name="montot2[]" value="'+montoGravado2+'">'+montoGravado2+'</td><td><input type="hidden" name="sneto2[]" value="'+sneto2+'">'+sneto2+'</td><td><input type="hidden" name="siva[]" value="'+siva2+'">'+siva2+'</td><td><input type="hidden" name="montot[]" value="'+montoGravado+'">'+montoGravado+'</td><td><input type="hidden" name="sriva[]" value="'+sriva2+'">'+sriva2+'</td><td><input type="hidden" name="srenta[]" value="'+srenta2+'">'+srenta2+'</td><td><input type="hidden" name="montot3[]" value="'+montoGravado3+'">'+montoGravado3+'</td></tr>';
      cont++;
      limpiar();
      $("#snetots").html("$" + snetots);
      $("#sivats").html("$" + sivats);
      $("#srivats").html("$" + srivats);
      $("#srentats").html("$" + srentats);
      $("#montots").html("$" + montots);
      $("#montots2").html("$" + montots2);
      $("#montots3").html("$" + montots3);
      evaluar();
      $("#detalles").append(fila);
    }else{
      alert("Error al ingresar el detalle del ingreso, revise los datos del credito fiscal");
    }
   
    
  }
  function limpiar(){
    $("#ccf").val("");
    $("#fechaccf").val("");
    $("#montoGravado").val("");
    $("#montoGravado2").val("");
  }
  //Muestra botón guardar
  function evaluar(){
    if(montot != 0){
      $("#guardar").show();
    }else{
      $("#guardar").hide();
    }
  }
  function eliminar(index){
    montots = montots - montot[index] - montot2[index] - siva[index];
    montots = Math.round(montots*100)/100; 
    montots2 = montots2 - montot2[index];
    montots2 = Math.round(montots2*100)/100; 
    montots3 = montots3 - montot3[index];
    montots3 = Math.round(montots3*100)/100;
    sivats = sivats - siva[index];
    sivats = Math.round(sivats*100)/100;
    srivats = srivats - sriva[index];
    srivats = Math.round(srivats*100)/100;
    srentats = srentats - srenta[index];
    srentats = Math.round(srentats*100)/100;
    snetots = snetots - sneto[index];
    snetots = Math.round(snetots*100)/100;
    $("#sivats").html("$" + sivats);
    $("#srivats").html("$" + srivats);
    $("#srentats").html("$" + srentats);
    $("#snetots").html("$" + snetots);
    $("#montots").html("$" + montots);
    $("#montots2").html("$" + montots2);
    $("#montots3").html("$" + montots3);
    $("#fila" + index).remove();
    evaluar();
  }
  $(document).ready(function() {
var data = {}; 
$("#cliente option").each(function(i,el) {  
   data[$(el).data("value")] = $(el).val();
});
// `data` : object of `data-value` : `value`
console.log(data, $("#cliente option").val());
    $('#submit').click(function()
    {
        var value = $('#selected').val();
        alert($('#cliente [value="' + value + '"]').data('value'));
    });
});

      document.getElementById('año').addEventListener('change', function(e) {
      console.log(this, this.value);
      if(this.value != '') {
        document.getElementById('fechaf').innerHTML = document.getElementById(`alternativa-${this.value}`).innerHTML;
      } else {
        document.getElementById('fechaf').innerText = 'Selecciona un fecha a ingresa'
      }
    });

</script>
@endpush
@endsection

