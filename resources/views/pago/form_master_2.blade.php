   <br>

   <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css'>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>

   <div class="row">

    <div class="col-sm-5">
      {!! form::label('Prestatario','Prestatario') !!}
    </div>
    <div class="col-sm-7">
      @foreach ($prestatarios as $key => $value2)
          @if($pago->idprest == $value2->id)
        {{$value2->pnombre}} {{$value2->snombre}} {{$value2->papellido}} {{$value2->sapellido}}
        @endif
        @endforeach
  </div>
    

  <div class="col-sm-5">
      {!! form::label('Prestamo','Prestamo') !!}
    </div>
    <div class="col-sm-7">
      @foreach ($prestamos as $key => $value3)
          @if($pago->idprest2 == $value3->id)
        {{$value3->codigoPrestamo}}
        @endif
        @endforeach
  </div>

  <div class="col-sm-5">
      {!! form::label('Referencia del pago:') !!}
    </div>
     <div class="col-sm-5">
      <div class="form-group {{ $errors->has('referencia') ? 'has-error' : "" }}">
       <i>{{ Form::text('referencia',NULL, ['class'=>'form-control','id'=>'referencia','placeholder'=>'Codigo de referencia']) }} </i>
    </div>
  </div>

  <div class="col-sm-5">
      {!! form::label('Fecha de pago:') !!}
    </div>
     <div class="col-sm-5">
      <div class="form-group {{ $errors->has('fechadepago') ? 'has-error' : "" }}">
       <i>{{ Form::date('fechadepago',NULL, ['class'=>'form-control','id'=>'fechadepago','placeholder'=>'Fecha de pago']) }} </i>
    </div>
  </div>

  <div class="col-sm-5">
      {!! form::label('Dias de intereses a calcular:') !!}
    </div>
     <div class="col-sm-5">
      {{ $pago->diasIntereses}}
  </div>

  <div class="col-sm-5">
      {!! form::label('Cantidad:') !!}
    </div>
     <div class="col-sm-5">
      {{ $pago->cantidad}}
  </div>


  <div class="col-sm-5">
      {!! form::label('Comentarios:') !!}
    </div>
     <div class="col-sm-5">
      <div class="form-group {{ $errors->has('comentarios') ? 'has-error' : "" }}">
       <i>{{ Form::text('comentarios',NULL, ['class'=>'form-control','id'=>'comentarios','placeholder'=>'Comentarios']) }} </i>
    </div>
  </div>

  
       <div class="form-group text-center" >
      {{ Form::button(isset($model)? 'Update' : 'Guardar' , ['class'=>'btn btn-success btn-sm','type'=>'submit']) }}
      <a class="btn btn-danger btn-sm" href="{{ route('pago.index') }}">Cancelar</a>
    </div>
      </div>
 
   <br>

    <!--Script para Colocar guion automatico en numero de DUI-->
<script type="text/javascript">
$(document).ready(function () {
    // Populate categories dropdown
    $.get('/get-prestatarios', function (data) {
        $.each(data, function (key, value) {
            $('#idprest').append('<option value="' + value.id + '">' + value.pnombre + ' ' + value.snombre + ' ' + value.papellido + ' ' + value.sapellido + '</option>');
        });
    });

    // Handle category selection change
    $('#idprest').change(function () {
        var selectedPrestatario = $(this).val();
        $('#idprest2').empty();

        if (selectedPrestatario !== '') {
            // Populate products dropdown based on selected category
            $.get('/get-prestamos/' + selectedPrestatario, function (data) {
                $.each(data, function (key, value) {
                    $('#idprest2').append('<option value="' + value.id + '">' + value.id + ' ' + '-' + value.codigoPrestamo + '</option>');
                });
            });
        }
    });
});

</script>