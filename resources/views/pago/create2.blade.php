@extends('layout')
@section('container')
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <h3 style="text-align:center"> DATOS DE LA SUBCUENTA</h3>
      <br>
      {{ Form::model($contcuentas,['route'=>['contcuenta.store2'], 'method'=>'POST']) }}
        @include('contcuenta.form_master2')
      {{ form::close() }}
    </div>
  </div>
@endsection
