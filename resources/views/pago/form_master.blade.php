   <br>

   <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css'>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>

   <div class="row">

    <div class="col-sm-5">
      {!! form::label('Prestatario','Prestatario') !!}
    </div>
    <div class="col-sm-5">
      <div class="form-group {{ $errors->has('idprest') ? 'has-error' : "" }}">
      <select id="idprest" name="idprest" class="form-control" data-live-search="true">
    <option value="">Seleccionar Prestatario</option>
</select>         
 </div>
  </div>

  <div class="col-sm-5">
      {!! form::label('Prestamo','Prestamo') !!}
    </div>
    <div class="col-sm-5">
      <div class="form-group {{ $errors->has('idprest2') ? 'has-error' : "" }}">
      <select id="idprest2" name="idprest2" class="form-control" data-live-search="true">
    <option value="">Seleccionar Prestamo</option>
</select>      
 </div>
  </div>

  <div class="col-sm-5">
      {!! form::label('Referencia del pago:') !!}
    </div>
     <div class="col-sm-5">
      <div class="form-group {{ $errors->has('referencia') ? 'has-error' : "" }}">
       <i>{{ Form::text('referencia',NULL, ['class'=>'form-control','id'=>'referencia','placeholder'=>'Codigo de referencia', 'required' => '']) }} </i>
    </div>
  </div>

  <div class="col-sm-5">
      {!! form::label('Fecha de pago:') !!}
    </div>
     <div class="col-sm-5">
      <div class="form-group {{ $errors->has('fechadepago') ? 'has-error' : "" }}">
       <i>{{ Form::date('fechadepago',NULL, ['class'=>'form-control','id'=>'fechadepago','placeholder'=>'Fecha de pago', 'required' => '']) }} </i>
    </div>
  </div>

  <div class="col-sm-5">
      {!! form::label('Dias de intereses a calcular:') !!}
    </div>
     <div class="col-sm-5">
      <div class="form-group {{ $errors->has('diasIntereses') ? 'has-error' : "" }}">
       <i>{{ Form::text('diasIntereses',NULL, ['class'=>'form-control','id'=>'diasIntereses','placeholder'=>'Dias intereses', 'required' => '']) }} </i>
    </div>
  </div>

  <div class="col-sm-5">
      {!! form::label('Cantidad:') !!}
    </div>
     <div class="col-sm-5">
      <div class="form-group {{ $errors->has('cantidad') ? 'has-error' : "" }}">
       <i>{{ Form::text('cantidad',NULL, ['class'=>'form-control','id'=>'cantidad','placeholder'=>'Cantidad', 'required' => '']) }} </i>
    </div>
  </div>

  <div class="col-sm-5">
      {!! form::label('Comentarios:') !!}
    </div>
     <div class="col-sm-5">
      <div class="form-group {{ $errors->has('comentarios') ? 'has-error' : "" }}">
       <i>{{ Form::text('comentarios',NULL, ['class'=>'form-control','id'=>'comentarios','placeholder'=>'Comentarios']) }} </i>
    </div>
  </div>

  
       <div class="form-group text-center" >
      {{ Form::button(isset($model)? 'Update' : 'Guardar' , ['class'=>'btn btn-success btn-sm','type'=>'submit']) }}
      <a class="btn btn-danger btn-sm" href="{{ route('pago.index') }}">Cancelar</a>
    </div>
      </div>
 
   <br>

    <!--Script para Colocar guion automatico en numero de DUI-->
<script type="text/javascript">
$(document).ready(function () {
    // Populate categories dropdown
    $.get('/get-prestatarios', function (data) {
        $.each(data, function (key, value) {
            $('#idprest').append('<option value="' + value.id + '">' + value.pnombre + ' ' + value.snombre + ' ' + value.papellido + ' ' + value.sapellido + '</option>');
        });
    });

    // Handle category selection change
    $('#idprest').change(function () {
        var selectedPrestatario = $(this).val();
        $('#idprest2').empty();

        if (selectedPrestatario !== '') {
            // Populate products dropdown based on selected category
            $.get('/get-prestamos/' + selectedPrestatario, function (data) {
                $.each(data, function (key, value) {
                    $('#idprest2').append('<option value="' + value.id + '">' + value.id + ' ' + '-' + value.codigoPrestamo + '</option>');
                });
            });
        }
    });
});

</script>