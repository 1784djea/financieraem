@extends('layout')
@section('container')
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
    	<h3 style="text-align:center"> Actualizacion del pago</h3>
    	<br>
      {{ Form::model($pago,['route'=>['pago.update',$pago->id],'method'=>'PATCH']) }}
      @include('pago.form_master2')
      {{ Form::close() }}
    </div>
  </div>
@endsection