<!DOCTYPE html>
<html>
<!-- Latest compiled and minified CSS -->
<link href="{{ public_path()}}/css/reportes.css" rel="stylesheet">


<head>

    <title>Factura electronica</title>

</head>

<body>
    <table class="table table-striped tamletra" style="text-align:center" >
    <tr>
        <td style="text-align:left;" WIDTH="25%">
            <img src="img/logo.jpg">
        </td>
        <td style="text-align:right;" WIDTH="50%">
            <h4>DOCUMENTO TRIBUTARIO ELECTRÓNICO</h4>
            <h4>{{$tipo}}</h4>
        </td>
        <td style="text-align:right;" WIDTH="25%">Ver.1</td>
        </tr>
        </table>

        <br><br>

    <table class="table table-striped tamletra" style="text-align:center" >
    <tr>
        <td style="text-align:left;">
            Código de Generación:<br>
            Número de Control:<br>
            Sello de Recepción:<br>
         </td>
        <td style="text-align:left;">QR</td>
        <td style="text-align:right;">
            Módelo de Facturación:<br>
            Tipo de Transmisión:<br>
            Fecha y Hora de Generación:<br>
        </td>
    </tr>
</table>
<br><br>
<table class="table table-striped tamletra" style="text-align:center" >
    <tr>
      <th style="text-align:center" WIDTH="50%">Emisor</th>
      <th style="text-align:center" WIDTH="50%">Receptor</th>
    </tr>
    <tr>
        <td style="text-align:left">
            Nombre o razón social: EM & ASOCIADOS S.A. DE C.V<br>
            NIT: 0618-160722-101-0<br>
            NRC:<br>
            Actividad economica:<br>
            Direccion: Tonacatepeque, San Salvador<br>
            Numero de telefono:<br>
            Correo electronico:<br>
            Nombre Comercial:<br>
            Tipo de establecimiento:<br>
        </td>
        <td  style="text-align:left">
            @foreach ($prestatario as $key => $values)
            Nombre o razon social: {{$values->pnombre}} {{$values->snombre}} {{$values->papellido}} {{$values->sapellido}}<br>
            Tipo de doc. identificacion: DUI<br>
            N de Doc. identificacion: {{$values->dui}}<br>
            {{$values->direccion}}<br>
            Correo Electronico: {{$values->email}}<br>
            Nombre Comercial:<br>
            @endforeach
        </td>
    </tr>
</table>
<br><br>


<table class="table table-striped tamletra" style="text-align:center" >
    <tr>
      <th style="padding: 3px">No.</th>
      <th style="text-align:left; padding: 3px">Cantidad</th>
      <th style="text-align:left; padding: 3px">Unidad</th>
      <th style="text-align:left; padding: 3px; width:220px">Descripcion</th>
      <th style="text-align:left; padding: 3px">Precio unitario</th>
      <th style="text-align:left; padding: 3px; width:80px">Otros montos no afectados</th>
      <th style="text-align:left; padding: 3px">Desc item</th>
      <th style="text-align:left; padding: 3px; width:55px">Ventas no sujetas</th>
      <th style="text-align:left; padding: 3px">Ventas exentas</th>
      <th style="text-align:left; padding: 3px">Ventas Gravadas</th>
    </tr>
    <?php $no=1;
    $iva=0;
    $ventas=0;
    $ventast=0; 
    $ventast2=0; 
    ?>
    @foreach ($pagos as $key => $value)
    <?php 
    $ventas=$value->intereses; 
    $ventas=round($ventas, 2); 
    $ventasp=number_format($ventas,2,".",","); 
    $ventast = $ventast + $ventas;
    $ventast = round($ventast, 2);
    $ventastp = number_format($ventast,2,".",",");
    ?>
    <tr>
        <td>{{ $no++ }}</td>
        <td style="text-align:center;">1</td>
        <td style="text-align:left">Otra</td>
        <td style="text-align:left">Pago de intereses credito cod: {{ $value->idprest2 }}</td>
        <td style="text-align:right;">${{ $ventasp }}</td>
        <td style="text-align:right;">$0.00</td>
        <td style="text-align:right;">$0.00</td>
        <td style="text-align:right;">$0.00</td>
        <td style="text-align:right;">${{ $ventasp }}</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    @endforeach
    <?php 
    if ($tipo2 == 1) {
        $iva = $ventast * 0.13; 
        $iva = round($iva, 2); 
        $ivap = number_format($iva,2,".",","); 
    } else {
        $iva = 0;
        $iva = round($iva, 2); 
        $ivap = number_format($iva,2,".",",");
    }
    $ventast2 = $ventast + $iva;
    $ventast2 = round($ventast2, 2); 
    $ventast2p = number_format($ventast2,2,".",",");
    
    ?>
    @if($tipo2 == 0)
    <tr>
        <td colspan="9" style="text-align:right">Sumatoria de ventas:</td>
        <td style="text-align:right;">${{ $ventastp }}</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Monto global de Desc., Rebajas y otros a ventas no sujetas:</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Monto global Desc., Rebajas y otros a ventas exentas:</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Monto global Desc., Rebajas y otros a ventas gravadas:</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right"> </td>
        <td style="text-align:right;"></td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Sub-total:</td>
        <td style="text-align:right;">${{ $ventastp }}</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">IVA Percibido: (+):</td>
        <td style="text-align:right;">${{ $ivap }}</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">IVA Retenido: (-):</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Monto Total de la Operacion:</td>
        <td style="text-align:right;">${{ $ventast2p }}</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Total Otros Montos No Afectos:</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Total a Pagar</td>
        <td style="text-align:right;">${{ $ventast2p }}</td>
    </tr>
    @endif

    @if($tipo2 == 1)
    <tr>
        <td colspan="9" style="text-align:right">Sumatoria de ventas:</td>
        <td style="text-align:right;">${{ $ventastp }}</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Monto global de Desc., Rebajas y otros a ventas no sujetas:</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Monto global Desc., Rebajas y otros a ventas exentas:</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Monto global Desc., Rebajas y otros a ventas gravadas:</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Impuesto al valor agregado 13%</td>
        <td style="text-align:right;">${{ $ivap }}</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Sub-total:</td>
        <td style="text-align:right;">${{ $ventastp }}</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">IVA Percibido: (+):</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">IVA Retenido: (-):</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Total Otros Montos No Afectos:</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Total a Pagar</td>
        <td style="text-align:right;">${{ $ventast2p }}</td>
    </tr>
    @endif

    @if($tipo2 == 3)
    <tr>
        <td colspan="9" style="text-align:right">Sumatoria de ventas:</td>
        <td style="text-align:right;">${{ $ventastp }}</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Monto global de Desc., Rebajas y otros a ventas no sujetas:</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Monto global Desc., Rebajas y otros a ventas exentas:</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Monto global Desc., Rebajas y otros a ventas gravadas:</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Impuesto al valor agregado 13%</td>
        <td style="text-align:right;">${{ $ivap }}</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Sub-total:</td>
        <td style="text-align:right;">${{ $ventastp }}</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">IVA Percibido: (+):</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">IVA Retenido: (-):</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Total Otros Montos No Afectos:</td>
        <td style="text-align:right;">$0.00</td>
    </tr>
    <tr>
        <td colspan="9" style="text-align:right">Total a Pagar</td>
        <td style="text-align:right;">${{ $ventast2p }}</td>
    </tr>
    @endif

</table>

    <div class="bordesolido tamletra" style="width:100%">
        <p>Valor en letras: {{$numl}}</p>
        <p>Observaciones: </p>
    </div>
    <div class="bordesolido tamletra" style="width:100%">
        <p>Responsable por parte del Emisor:</p>
        <p>Responsable por parte del Receptor:</p>
    </div>



    </body>

</html>